<?php

namespace App\Models\Contacts;

use App\Traits\Accountable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jedrzej\Searchable\SearchableTrait;

class Contact extends Model
{
	use HasFactory, SearchableTrait, Accountable;

	protected $guarded = ['id'];

	public $searchable = [
		'id',
		'account_id',
		'gender',
		'first_name',
		'last_name',
		'middle_name',
		'address',
		'city',
		'state',
		'postal_code',
		'country',
		'languages',
		'birth_date',
		'creation_type',
		'creator_id',
		'creator_details',
		'detailed_status',
		'verification_status',
		'verification_type',
		'verification_details',
		'verification_date',
		'import_id',
		'created_at',
		'deleted_at',
	];

}
