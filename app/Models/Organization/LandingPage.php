<?php

namespace App\Models\Organization;

use App\Traits\Accountable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Jedrzej\Searchable\SearchableTrait;

class LandingPage extends Model
{
	use HasFactory, SoftDeletes, SearchableTrait, Accountable;

	protected $guarded = ['id'];

	public $searchable = [
		'id',
		'account_id',
		'name',
		'domain_name',
		'url',
		'status',
		'created_at',
		'deleted_at',
	];

	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_IN_PROGRESS = 2;


	protected static function booted()
	{
		self::deleting(fn($model) => $model->update(['status' => self::STATUS_INACTIVE]));
		self::restoring(fn($model) => $model->update(['status' => self::STATUS_ACTIVE]));
	}

	public static function statusesList()
	{
		return [
			self::STATUS_INACTIVE => ['name' => trans('values.inactive'), 'class' => 'danger'],
			self::STATUS_ACTIVE => ['name' => trans('values.connected'), 'class' => 'success'],
			self::STATUS_IN_PROGRESS => ['name' => trans('values.in_progress'), 'class' => 'info'],
		];
	}

	public function getStatusNameAttribute()
	{
		return self::statusesList()[$this->status]['name'] ?? trans('values.n/a');
	}

	public function getStatusDataAttribute()
	{
		return self::statusesList()[$this->status] ?? ['name' => trans('values.n/a'), 'class' => 'info'];
	}

	public function getScreenshotUrlAttribute()
	{
		return Storage::url('images/landing-pages/' . $this->screenshot);
	}

	public function getCanBeCanceledAttribute()
	{
		return $this->status == self::STATUS_ACTIVE;
	}
}
