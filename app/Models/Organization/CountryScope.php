<?php

namespace App\Models\Organization;

use App\Helpers\Countries;
use App\Models\Accounts\Account;
use App\Traits\Accountable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;
use Jedrzej\Searchable\SearchableTrait;

class CountryScope extends Model
{
	use HasFactory, SoftDeletes, SearchableTrait, Accountable;

	protected $guarded = ['id'];

	public $searchable = [
		'id',
		'account_id',
		'code',
		'status',
		'created_at',
		'deleted_at',
	];

	const STATUS_DISABLED = 0;
	const STATUS_ENABLED = 1;

	protected static function booted()
	{
		self::deleting(fn($model) => $model->update(['status' => self::STATUS_DISABLED]));
		self::restoring(fn($model) => $model->update(['status' => self::STATUS_ENABLED]));
	}

	public static function statusesList()
	{
		return [
			self::STATUS_DISABLED => ['name' => trans('values.disabled'), 'class' => 'danger'],
			self::STATUS_ENABLED => ['name' => trans('values.enabled'), 'class' => 'success'],
		];
	}

	public function getStatusNameAttribute()
	{
		return self::statusesList()[$this->status]['name'] ?? trans('values.n/a');
	}

	public function getStatusDataAttribute()
	{
		return self::statusesList()[$this->status] ?? ['name' => trans('values.n/a'), 'class' => 'info'];
	}

	public function getNameAttribute()
	{
		return Countries::getName($this->code);
	}

	public function getImageAttribute()
	{
		return Countries::getImageUrl($this->code);
	}

	public function account()
	{
		return $this->belongsTo(Account::class)->withTrashed();
	}

	public function getCanBeDisabledAttribute()
	{
		return $this->status == self::STATUS_ENABLED;
	}

	public static function allowedCountriesList($simple = false, $filter = [])
	{
		if ($simple) {
			return array_filter(json_decode(File::get(resource_path('data/countries-list.json')), 1), fn($code) => !in_array($code, $filter));
		}
		return collect(json_decode(File::get(resource_path('data/countries-list.json')), 1))->map(fn($code) => !in_array($code, $filter) ? ['code' => strtoupper($code), 'name' => Countries::getName(strtoupper($code))] : null)->filter()->toArray();
	}
}
