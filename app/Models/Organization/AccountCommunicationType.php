<?php

namespace App\Models\Organization;

use App\Traits\Accountable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jedrzej\Searchable\SearchableTrait;

class AccountCommunicationType extends Model
{
	use HasFactory, SoftDeletes, SearchableTrait, Accountable;

	protected $guarded = ['id'];

	public $searchable = [
		'id',
		'account_id',
		'communication_type_id',
		'status',
		'created_at',
		'deleted_at',
	];

	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_IN_PROGRESS = 2;
	const STATUS_DECLINED = 3;


	protected static function booted()
	{
		self::deleting(fn($model) => $model->update(['status' => self::STATUS_INACTIVE]));
		self::restoring(fn($model) => $model->update(['status' => self::STATUS_ACTIVE]));
	}

	public static function statusesList()
	{
		return [
			self::STATUS_INACTIVE => ['name' => trans('values.inactive'), 'class' => 'danger'],
			self::STATUS_ACTIVE => ['name' => trans('values.connected'), 'class' => 'success'],
			self::STATUS_IN_PROGRESS => ['name' => trans('values.in_progress'), 'class' => 'info'],
			self::STATUS_DECLINED => ['name' => trans('values.declined'), 'class' => 'danger'],
		];
	}

	public function getStatusNameAttribute()
	{
		return self::statusesList()[$this->status]['name'] ?? trans('values.n/a');
	}

	public function getStatusDataAttribute()
	{
		return self::statusesList()[$this->status] ?? ['name' => trans('values.n/a'), 'class' => 'info'];
	}

	public function communicationType()
	{
		return $this->belongsTo(CommunicationType::class)->withTrashed();
	}
}
