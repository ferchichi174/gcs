<?php

namespace App\Models\Organization;
;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;
use Jedrzej\Searchable\SearchableTrait;

class CommunicationType extends Model
{
	use HasFactory, SoftDeletes, SearchableTrait;

	protected $guarded = ['id'];
	public $searchable = [
		'id',
		'name',
		'status',
		'created_at',
		'deleted_at',
	];

	const STATUS_UNAVAILABLE = 0;
	const STATUS_AVAILABLE = 1;
	const STATUS_COMING_SOON = 2;

	protected static function booted()
	{
		self::deleting(fn($model) => $model->update(['status' => self::STATUS_UNAVAILABLE]));
		self::restoring(fn($model) => $model->update(['status' => self::STATUS_AVAILABLE]));
	}

	public static function statusesList()
	{
		return [
			self::STATUS_UNAVAILABLE => ['name' => trans('values.unavailable'), 'class' => 'danger'],
			self::STATUS_AVAILABLE => ['name' => trans('values.available'), 'class' => 'success'],
			self::STATUS_COMING_SOON => ['name' => trans('values.coming_soon'), 'class' => 'success'],
		];
	}

	public function getRequiresApprovalAttribute()
	{
		return self::communicationTypesList()[$this->id]['requires_approval'] ?? false;
	}

	public function getStatusNameAttribute()
	{
		return self::statusesList()[$this->status]['name'] ?? trans('values.n/a');
	}

	public function getStatusDataAttribute()
	{
		return self::statusesList()[$this->status] ?? ['name' => trans('values.n/a'), 'class' => 'info'];
	}

	public function getDescriptionAttribute()
	{
		return self::communicationTypesList()[$this->id]['description'] ?? trans('values.n/a');
	}

	public function getDetailedDescriptionAttribute()
	{
		return self::communicationTypesList()[$this->id]['detailed_description'] ?? trans('values.n/a');
	}

	public function getIconAttribute()
	{
		return self::communicationTypesList()[$this->id]['icon'] ?? '';
	}

	public static function communicationTypesList()
	{
		return json_decode(File::get(resource_path('data/communication-types.json')), 1)['data'];
	}

}
