<?php

namespace App\Models\Accounts;

use App\Models\Organization\AccountCommunicationType;
use App\Models\Organization\CommunicationType;
use App\Models\Organization\CountryScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
	use HasFactory, SoftDeletes;

	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;

	public static function statusesList()
	{
		return [
			self::STATUS_INACTIVE => ['name' => trans('values.inactive'), 'class' => 'warning'],
			self::STATUS_ACTIVE => ['name' => trans('values.active'), 'class' => 'success'],
		];
	}

	public function getStatusNameAttribute()
	{
		return self::statusesList()[$this->status]['name'] ?? trans('values.n/a');
	}

	public function getStatusDataAttribute()
	{
		return self::statusesList()[$this->status] ?? ['name' => trans('values.n/a'), 'class' => 'info'];
	}

	public function countryScopes()
	{
		return $this->hasMany(CountryScope::class)->withTrashed();
	}

	public function accountCommunicationTypes()
	{
		return $this->hasMany(AccountCommunicationType::class)->withTrashed();
	}
}
