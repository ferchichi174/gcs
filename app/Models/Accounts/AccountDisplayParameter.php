<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountDisplayParameter extends Model
{
	use HasFactory;

	public $timestamps = false;

	const DATE_FORMAT_YMD_DIGIT_LONG = 1;
	const DATE_FORMAT_YMD_DIGIT_SHORT = 2;
	const DATE_FORMAT_YMD_LETTER_SHORT = 3;
	const DATE_FORMAT_YMD_LETTER_LONG = 4;
	const DATE_FORMAT_DMY_DIGIT_LONG = 1;
	const DATE_FORMAT_DMY_DIGIT_SHORT = 2;
	const DATE_FORMAT_DMY_LETTER_SHORT = 3;
	const DATE_FORMAT_DMY_LETTER_LONG = 4;
	const DATE_FORMAT_MDY_DIGIT_LONG = 1;
	const DATE_FORMAT_MDY_DIGIT_SHORT = 2;
	const DATE_FORMAT_MDY_LETTER_SHORT = 3;
	const DATE_FORMAT_MDY_LETTER_LONG = 4;

	const TIME_FORMAT_24_LONG = 1;
	const TIME_FORMAT_24_SHORT = 1;
	const TIME_FORMAT_12_LONG = 1;
	const TIME_FORMAT_12_SHORT = 1;
}
