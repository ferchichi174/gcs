<?php

namespace App\Traits;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Builder;

trait Accountable
{
	protected static function bootAccountable()
	{
		if (session()->has('account_id') && session('account_id') > 0) {
			static::addGlobalScope('belongs_to_account_id', function (Builder $builder) {
				$builder->where('account_id', session('account_id'));
			});
		}
	}

	public function account()
	{
		return $this->belongsTo(Account::class);
	}
}