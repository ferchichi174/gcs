<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Models\Organization\LandingPage;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class LandingPagesController extends Controller
{
	public function index()
	{
		return view('pages.organization.landing-pages', [
			'landingPagesList' => LandingPage::withTrashed()->get(),
			'landingPageStatusesList' => LandingPage::statusesList(),
		]);
	}

	public function filteredIndex(Request $request)
	{
		$landingPages = LandingPage::withTrashed()->filtered()->get();
		if ($request->has(['order', 'columns'])) {
			$landingPages = $landingPages->sortBy(collect($request->order)->map(function ($order) use ($request) {
				return [$request->columns[$order['column']]['name'], $order['dir']];
			})->toArray());
		}
		return DataTables::collection($landingPages)
			->setTotalRecords(LandingPage::withTrashed()->count())
			->setFilteredRecords($landingPages->count())
			->addColumn('status_data', function ($item) {
				return $item->statusData;
			})
			->addColumn('screenshot_url', function ($item) {
				return $item->screenshotUrl;
			})
			->editColumn('created_at', function ($item) {
				return $item->created_at->format(session('date_format', 'Y-m-d'));
			})
			->editColumn('can_be_canceled', function ($item) {
				return $item->canBeCanceled;
			})
			->editColumn('deleted_at', function ($item) {
				return $item->deleted_at?->format(session('date_format', 'Y-m-d')) ?? trans('values.n/a');
			})
			->only(['id', 'name', 'url', 'domain_name', 'screenshot_url', 'status', 'status_data', 'created_at', 'deleted_at', 'can_be_canceled'])
			->filter(function () {
			})
			->order(function () {
			})
			->toJson();
	}

	public function create()
	{
		//
	}

	public function store(Request $request)
	{
		//
	}

	public function show(LandingPage $landingPage)
	{
		//
	}

	public function edit(LandingPage $landingPage)
	{
		//
	}

	public function update(Request $request, LandingPage $landingPage)
	{
		//
	}

	public function destroy(LandingPage $landingPage)
	{

		if ($landingPage->delete()) {
			return $this->successfulResponse([
				'html' => trans_choice('actions.canceled', 1, ['attribute' => trans_choice('names.landing_page', 1)])
			]);
		}

		return $this->failedResponse();
	}
}