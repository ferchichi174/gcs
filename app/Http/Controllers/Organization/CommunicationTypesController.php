<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Models\Organization\CommunicationType;
use Illuminate\Http\Request;

class CommunicationTypesController extends Controller
{
	public function index()
	{
		//
	}

	public function create()
	{
		//
	}

	public function store(Request $request)
	{
		//
	}

	public function show(CommunicationType $communicationType)
	{
		//
	}

	public function edit(CommunicationType $communicationType)
	{
		//
	}

	public function update(Request $request, CommunicationType $communicationType)
	{
		//
	}

	public function destroy(CommunicationType $communicationType)
	{
		//
	}
}