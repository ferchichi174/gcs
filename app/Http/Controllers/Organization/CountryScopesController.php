<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Models\Organization\CountryScope;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class CountryScopesController extends Controller
{
	public function index()
	{
		return view('pages.organization.country-scopes', [
			'allowedCountriesList' => CountryScope::allowedCountriesList(),
			'countryScopeStatusesList' => CountryScope::statusesList(),
		]);
	}

	public function filteredIndex(Request $request)
	{
		$countryScopes = CountryScope::withTrashed()->filtered()->get();
		if ($request->has(['order', 'columns'])) {
			$countryScopes = $countryScopes->sortBy(collect($request->order)->map(function ($order) use ($request) {
				return [$request->columns[$order['column']]['name'], $order['dir']];
			})->toArray());
		}
		return DataTables::collection($countryScopes)
			->setTotalRecords(CountryScope::withTrashed()->count())
			->setFilteredRecords($countryScopes->count())
			->addColumn('name', function ($item) {
				return $item->name;
			})
			->addColumn('image', function ($item) {
				return $item->image;
			})
			->addColumn('status_data', function ($item) {
				return $item->statusData;
			})
			->addColumn('can_be_disabled', function ($item) {
				return $item->canBeDisabled;
			})
			->editColumn('created_at', function ($item) {
				return $item->created_at->format(session('date_format', 'Y-m-d'));
			})
			->editColumn('deleted_at', function ($item) {
				return $item->deleted_at?->format(session('date_format', 'Y-m-d')) ?? trans('values.n/a');
			})
			->only(['id', 'code', 'name', 'image', 'status', 'status_data', 'created_at', 'deleted_at', 'can_be_disabled'])
			->filter(function () {
			})
			->order(function () {
			})
			->toJson();
	}

	public function create()
	{
		//
	}

	public function store(Request $request)
	{
		$request->merge(['account_id' => session('account_id')]);
		$validator = \Validator::make($request->input(), [
			'code' => [
				'required',
				Rule::in(CountryScope::allowedCountriesList(true)),
				function ($attribute, $value, $fail) {
					if (CountryScope::withTrashed()->whereCode($value)->exists()) {
						$fail(trans_choice('messages.attribute_already_active', 1, ['attribute' => trans_choice('attributes.country', 1)]));
					}
				},
			],
		]);

		if ($validator->fails()) {
			return response()->json([
				'status' => 415,
				'title' => trans('messages.operation_failed'),
				'html' => implode('<br>', \Arr::flatten($validator->errors()->getMessages())),
				'icon' => 'error',
			]);
		}

		CountryScope::create(['account_id' => session('account_id'), 'code' => $request->input('code'), 'status' => CountryScope::STATUS_ENABLED]);

		return $this->successfulResponse([
			'html' => trans_choice('actions.created', 1, ['attribute' => trans_choice('names.country_scope', 1)])
		]);
	}

	public function show(CountryScope $countryScope)
	{
		//
	}

	public function edit(CountryScope $countryScope)
	{
		//
	}

	public function update(Request $request, CountryScope $countryScope)
	{
		//
	}

	public function destroy(CountryScope $countryScope)
	{
		//
	}

	public function enable(int $countryScopeId)
	{
		if (CountryScope::withTrashed()->find($countryScopeId)->restore()) {
			return $this->successfulResponse([
				'html' => trans_choice('actions.enabled', 1, ['attribute' => trans_choice('names.country_scope', 1)])
			]);
		}

		return $this->failedResponse();
	}

	public function disable(CountryScope $countryScope)
	{
		if ($countryScope->delete()) {
			return $this->successfulResponse([
				'html' => trans_choice('actions.disabled', 1, ['attribute' => trans_choice('names.country_scope', 1)])
			]);
		}

		return $this->failedResponse();
	}
}