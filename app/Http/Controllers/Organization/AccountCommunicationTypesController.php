<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Account;
use App\Models\Organization\AccountCommunicationType;
use App\Models\Organization\CommunicationType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AccountCommunicationTypesController extends Controller
{
	public function index()
	{
		return view('pages.organization.communication-types', [
			'communicationTypesList' => CommunicationType::get(),
			'accountCommunicationTypesList' => Account::find(session('account_id'))->accountCommunicationTypes,
			'accountCommunicationTypeStatusesList' => AccountCommunicationType::statusesList(),
		]);
	}

	public function create()
	{
		//
	}

	public function store(Request $request)
	{
		$request->merge(['account_id' => session('account_id')]);
		$validator = \Validator::make($request->input(), [
			'communication_type_id' => [
				'required',
				Rule::exists('communication_types', 'id')->where('status', CommunicationType::STATUS_AVAILABLE),
				function ($attribute, $value, $fail) {
					if (AccountCommunicationType::withTrashed()->whereCommunicationTypeId($value)->where('status', '!=', AccountCommunicationType::STATUS_INACTIVE)->exists()) {
						$fail(trans_choice('messages.attribute_already_active', 1, ['attribute' => trans_choice('names.communication_type', 1)]));
					}
				},
			],
		]);

		if ($validator->fails()) {
			return response()->json([
				'status' => 415,
				'title' => trans('messages.operation_failed'),
				'html' => implode('<br>', \Arr::flatten($validator->errors()->getMessages())),
				'icon' => 'error',
			]);
		}
		$needsApproval = (CommunicationType::communicationTypesList()[$request->input('communication_type_id')]['requires_approval'] ?? false);
		AccountCommunicationType::create(['account_id' => session('account_id'), 'communication_type_id' => $request->input('communication_type_id'), 'status' => $needsApproval ? AccountCommunicationType::STATUS_IN_PROGRESS : AccountCommunicationType::STATUS_ACTIVE]);

		return $this->successfulResponse([
			'html' => $needsApproval ? trans('messages.wait_for_approval') : trans_choice('actions.created', 1, ['attribute' => trans_choice('names.country_scope', 1)])
		]);
	}

	public function show(AccountCommunicationType $accountCommunicationType)
	{
		//
	}

	public function edit(AccountCommunicationType $accountCommunicationType)
	{
		//
	}

	public function update(Request $request, AccountCommunicationType $accountCommunicationType)
	{
		//
	}

	public function destroy(AccountCommunicationType $accountCommunicationType)
	{

		if ($accountCommunicationType->delete()) {
			return $this->successfulResponse([
				'html' => trans_choice('actions.disabled', 1, ['attribute' => trans_choice('names.communication_type', 1)])
			]);
		}

		return $this->failedResponse();
	}
}