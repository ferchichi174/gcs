<?php

namespace App\Http\Controllers\Contacts;

use App\Http\Controllers\Controller;
use App\Models\Contacts\Contact;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ContactsController extends Controller
{
	public function index()
	{
		return view('pages.contacts.contacts-index', []);
	}

	public function filteredIndex(Request $request)
	{
		$contacts = Contact::filtered()->get();
		if ($request->has(['order', 'columns'])) {
			$contacts = $contacts->sortBy(collect($request->order)->map(function ($order) use ($request) {
				return [$request->columns[$order['column']]['name'], $order['dir']];
			})->toArray());
		}
		return DataTables::collection($contacts)
			->setTotalRecords(Contact::count())
			->setFilteredRecords($contacts->count())
			->editColumn('created_at', function ($item) {
				return $item->created_at->format(session('date_format', 'Y-m-d'));
			})
			->editColumn('deleted_at', function ($item) {
				return $item->deleted_at?->format(session('date_format', 'Y-m-d')) ?? trans('values.n/a');
			})
//			->only(['id', 'code', 'name', 'image', 'status', 'status_data', 'created_at', 'deleted_at', 'can_be_disabled'])
			->filter(function () {
			})
			->order(function () {
			})
			->toJson();
	}


	public function create()
	{
		//
	}

	public function store(Request $request)
	{
		//
	}

	public function show(Contact $contact)
	{
		//
	}

	public function edit(Contact $contact)
	{
		//
	}

	public function update(Request $request, Contact $contact)
	{
		//
	}

	public function destroy(Contact $contact)
	{
		//
	}
}