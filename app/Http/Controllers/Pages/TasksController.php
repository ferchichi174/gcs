<?php
namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\tasks;
use App\Models\User;
use App\Models\Contacts\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $contact = Contact::orderby("id", "desc")->get();
        $user = User::orderby("id", "desc")->get();
        $task = tasks::orderby("id", "desc")->get();


        $data = collect(json_decode(file_get_contents(resource_path('data/communication-types.json'))));

        // get the default inner page
        return view('pages.pages.tasks',['task' => $task,'user'=>$user,'contacts'=>$contact,'data'=>$data["data"]]);
    }
    public function add(Request $request)
    {


        $tasks = new tasks();
        $tasks->title = $request->title ;
        $tasks->task = $request->task ;
        $tasks->status = 0 ;
        $tasks->details = $request->details ;
        $tasks->creator = $request->creator ;
        $tasks->recipient = $request->recipient ;
        $tasks->scheduling = $request->date ;
        $tasks->task = 0 ;
        $tasks->communication_id = 1 ;
        $tasks->contact_id = 1 ;
        $tasks->user_id = Auth::id();
        $tasks->save();
        return redirect()->route('tasks.index');

    }
}
