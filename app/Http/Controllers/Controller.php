<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function successfulResponse($options = [])
	{
		return response()->json(array_merge([
			'success' => true,
			'title' => trans('messages.operation_successful'),
			'icon' => 'success',
		], $options));
	}

	public function failedResponse($options = [])
	{
		return response()->json(array_merge([
			'success' => false,
			'title' => trans('messages.operation_failed'),
			'html' => trans('messages.reload_page_try_again'),
			'icon' => 'info',
		], $options));
	}
}
