let organizationCommunicationTypesIndexComponent = function () {

	// Update card visibility
	let updateCards = function () {
		document.querySelectorAll('.communication-type-card').forEach(function (item) {
			if ((item.getAttribute('data-status-eliminated') === 'true') || (item.getAttribute('data-text-eliminated') === 'true')) {
				item.classList.add('d-none');
			} else {
				item.classList.remove('d-none');
			}
		});
	};

	// Search Datatable
	let handleSearch = function () {
		let communicationTypesList = document.querySelectorAll('.communication-type-card');
		document.querySelector('input[data-datatable-filter="search"]').addEventListener('keyup', function (e) {
			communicationTypesList.forEach(function (item) {
				item.setAttribute('data-text-eliminated', false);
				if (e.target.value !== '') {
					if (item.querySelector('.communication-type-name').textContent.toLowerCase().search(e.target.value.toLowerCase()) < 0 && item.querySelector('.communication-type-description').textContent.toLowerCase().search(e.target.value.toLowerCase()) < 0) {
						item.setAttribute('data-text-eliminated', true);
					}
				}
			});
			updateCards();
		});
	};

	// Filter Datatable
	let handleFilter = () => {
		let communicationTypesList = document.querySelectorAll('.communication-type-card');
		document.querySelector('select[name="datatable-type-status"]').addEventListener('change', function (e) {
			communicationTypesList.forEach(function (item) {
				item.setAttribute('data-status-eliminated', false);
				if (e.target.value !== 'all') {
					if (item.querySelector('.communication-type-name').getAttribute('data-status').toLowerCase().search(e.target.value.toLowerCase()) < 0) {
						item.setAttribute('data-status-eliminated', true);
					}
				}
			});
			updateCards();
		});
	};

	// Delete scope
	let handleButtonActions = () => {
		let detailedDescriptionModal = new bootstrap.Modal(document.querySelector('#communication-type-description-modal'), {});
		document.querySelectorAll('.activate-communication-type').forEach(function (item) {
			item.addEventListener('click', function (e) {
				let typeId = e.target.dataset.id;
				if (typeId > 0) {
					Swal.fire({
						title: Lang.get('messages.confirm_action'),
						text: Lang.choice('actions.validate_enable', 1, {attribute: Lang.choice('names.communication_type', 1)}),
						icon: "question",
						showCancelButton: true,
					}).then(function (result) {
						if (result.value) {
							pageBlock.block();
							axios.post(route('organization.communication-types.store'), {communication_type_id: typeId})
								.then(function (response) {
									Swal.fire({
										title: response.data.title,
										html: response.data.html,
										icon: response.data.icon,
										willClose: function () {
											if (response.data.success) {
												location.reload();
											}
										}
									});
								})
								.catch(() => errorSwal())
								.then(function () {
									pageBlock.release();
								});
						}
					});
				} else {
					errorSwal();
				}
			});
		});

		document.querySelectorAll('.view-detailed-description').forEach(function (item) {
			item.addEventListener('click', function (e) {
				detailedDescriptionModal._element.querySelector('.communication-type-description').innerHTML = e.target.closest('.communication-type-card').querySelector('.communication-type-detailed-description').innerHTML;
				detailedDescriptionModal._element.querySelector('.communication-type-name').textContent = e.target.closest('.communication-type-card').querySelector('.communication-type-name').textContent;
				detailedDescriptionModal.show();
			});
		});
		document.querySelectorAll('.disconnect-communication-type').forEach(function (item) {
			item.addEventListener('click', function (e) {
				let typeId = e.target.dataset.id;
				if (typeId > 0) {
					Swal.fire({
						title: Lang.get('messages.confirm_action'),
						text: Lang.choice('actions.validate_disconnect', 1, {attribute: Lang.choice('names.communication_type', 1)}),
						icon: "question",
						showCancelButton: true,
					}).then(function (result) {
						if (result.value) {
							pageBlock.block();
							axios.delete(route('organization.communication-types.destroy', typeId))
								.then(function (response) {
									Swal.fire({
										title: response.data.title,
										html: response.data.html,
										icon: response.data.icon,
										willClose: function () {
											if (response.data.success) {
												location.reload();
											}
										}
									});
								})
								.catch(() => errorSwal())
								.then(function () {
									pageBlock.release();
								});
						}
					});
				} else {
					errorSwal();
				}
			});
			item.addEventListener('mouseenter', function (e) {
				e.target.classList.replace('btn-light-primary', 'btn-light-danger');
				e.target.querySelector('.hover-state').classList.remove('d-none');
				e.target.querySelector('.normal-state').classList.add('d-none');
			});
			item.addEventListener('mouseleave', function (e) {
				e.target.classList.replace('btn-light-danger', 'btn-light-primary');
				e.target.querySelector('.hover-state').classList.add('d-none');
				e.target.querySelector('.normal-state').classList.remove('d-none');
			});
		});
	};

	return {
		// public functions
		init: function () {
			handleButtonActions();
			handleSearch();
			handleFilter();
			//handleStatusChangingActions();
		}
	};
}
();
organizationCommunicationTypesIndexComponent.init();