let landingpagesIndexComponent = function () {
	let landingPagesListContainer, landingPageDetailsModal;
	let initDatatable = function () {
		window.landingpagesDatatable = DTable(modelsListTable, {
			processing: true,
			serverSide: false,
			ajax: {
				url: route('organization.landing-pages.filtered-index'),
				type: 'POST',
				complete: function (response) {
					pageBlock.release();
					if (response.responseJSON.data && response.responseJSON.data.length > 0) {
					} else {
						Swal.fire({
							title: Lang.get('messages.no_data'),
							text: Lang.get('messages.empty_table'),
							icon: 'info'
						});
					}
				}
			},
			columns: [
				{data: 'id', name: 'id'},
				{data: 'name', name: 'name'},
				{data: 'domain_name', name: 'domain_name'},
				{data: 'url', name: 'url'},
				{data: 'created_at', name: 'created_at'},
				{data: 'status_data', name: 'status'},
				{data: 'id', name: 'actions', sortable: false, searchable: false},
			],
			columnDefs: [
				{
					targets: -2,
					render: function (status_data) {
						return `<span class="badge badge-light-${status_data.class} fw-bolder px-4 py-3">${status_data.name}</span>`;
					}
				},
				{
					targets: -1,
					className: 'text-end',
					render: function (id, type, item) {
						return `
							<button type="button" class="btn btn-light-primary btn-sm btn-icon btn-icon-primary btn-active-primary menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark" title="${Lang.get('messages.quick_actions')}">
								<span class="svg-icon svg-icon-3 svg-icon-primary">
									<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
									    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									     <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
									        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
									        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
									        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
									    </g>
									</svg>
								</span>
							</button>
							<!--begin::Menu-->
							<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4"
							     data-kt-menu="true">
								<!--begin::Menu item-->
								<div class="menu-item px-3">
									<a href="${item.url}" class="menu-link px-3 view-landing-page">
										${Lang.get('actions.view_attribute', {attribute: Lang.choice('names.landing_page', 1)})}
									</a>
								</div>
								<!--end::Menu item-->
								<!--begin::Menu item-->
								<div class="menu-item px-3">
									<a href="javascript:" class="menu-link px-3 show-landing-page-details" data-id="${item.id}">
										${Lang.get('actions.show_attribute_details', {attribute: Lang.choice('names.landing_page', 1)})}
									</a>
								</div>
								<!--end::Menu item-->
								<!--begin::Menu item-->
								<div class="menu-item px-3">
									<a href="javascript:" class="menu-link px-3 ${item.can_be_canceled ? 'cancel-landing-page' : 'disabled disabled-pointer-events'}" data-id="${item.id}">
										${Lang.get('actions.cancel_attribute', {attribute: Lang.choice('names.landing_page', 1)})}
									</a>
								</div>
								<!--end::Menu item-->
							</div>
							<!--end::Menu-->
						`;
					},
				}
			],
			drawCallback: function () {
				drawCards(this.api().data().toArray());
			}
		});
	};

	let drawCards = function (data) {
		landingPagesListContainer.innerHTML = '';
		if (data.length > 0) {
			data.forEach(function (item) {
				landingPagesListContainer.insertAdjacentHTML('beforeend', `
							<div class="col-12 col-md-6 col-xl-3 mb-6">
								<!--begin::Hot sales post-->
								<div class="card card-xl-stretch me-md-6">
									<!--begin::Card Body-->
									<div class="card-body">
										<div class="d-flex justify-content-between mb-4">
											<div class="d-flex my-auto">
												<a href="${item.url}" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">${item.name}</a>
											</div>
											<div class="d-flex">
												<!--begin::Action-->
												<div data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark" title="${Lang.get('messages.quick_actions')}">
													<button type="button" class="btn btn-light-primary btn-sm btn-icon btn-icon-primary btn-active-primary menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
														<span class="svg-icon svg-icon-3 svg-icon-primary">
															<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
															    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															     <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
															        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
															        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
															        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
															    </g>
															</svg>
														</span>
													</button>
													<!--begin::Menu-->
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4"
													     data-kt-menu="true">
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="${item.url}" class="menu-link px-3 view-landing-page">
																${Lang.get('actions.view_attribute', {attribute: Lang.choice('names.landing_page', 1)})}
															</a>
														</div>
														<!--end::Menu item-->
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="javascript:" class="menu-link px-3 show-landing-page-details" data-id="${item.id}">
																${Lang.get('actions.show_attribute_details', {attribute: Lang.choice('names.landing_page', 1)})}
															</a>
														</div>
														<!--end::Menu item-->
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="javascript:" class="menu-link px-3 ${item.can_be_canceled ? 'cancel-landing-page' : 'disabled disabled-pointer-events'}" data-id="${item.id}">
																${Lang.get('actions.cancel_attribute', {attribute: Lang.choice('names.landing_page', 1)})}
															</a>
														</div>
														<!--end::Menu item-->
													</div>
													<!--end::Menu-->
												</div>
												<!--end::Action-->
											</div>
										</div>
										<!--begin::Overlay-->
										<a class="d-block overlay" data-fslightbox="lightbox-basic" href="${item.screenshot_url}">
											<!--begin::Image-->
											<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-125px" style="background-image:url('${item.screenshot_url}')"></div>
											<!--end::Image-->
											<!--begin::Action-->
											<div class="overlay-layer card-rounded bg-dark bg-opacity-25">
												<i class="bi bi-eye-fill fs-2x text-white"></i>
											</div>
											<!--end::Action-->
										</a>
										<!--end::Overlay-->
										<!--begin::Body-->
										<div class="mt-5 row justify-content-between">
										<div class="col-auto">
											<!--begin::Text-->
											<div class="fs-6 fw-bolder ">
												<!--begin::Label-->
												<div class="min-w-125px py-3 px-4 text-center">
													<div class="fs-6 text-gray-800 fw-bolder">${item.created_at}</div>
													<div class="fw-bold text-gray-400 mt-2">${Lang.choice('attributes.starting_date', 1)}</div>
												</div>
												<!--end::Label-->
											</div>
											<!--end::Text-->
										</div>
										<div class="col-auto">
											<!--begin::Text-->
											<div class="fs-6 fw-bolder ">
												<!--begin::Label-->
												<div class="min-w-125px py-3 px-4 text-center">
													<div class="fs-6 text-gray-800 fw-bolder"><span class="badge badge-light-${item.status_data.class} fw-bolder">${item.status_data.name}</span></div>
													<div class="fw-bold text-gray-400 mt-2">${Lang.choice('attributes.status', 1)}</div>
												</div>
												<!--end::Label-->
											</div>
											<!--end::Text-->
										</div>
										<!--end::Body-->
									</div>
									<!--end::Card Body-->
								</div>
								<!--end::Hot sales post-->
							</div>
						`);
			});
			refreshFsLightbox();
			KTApp.initBootstrapTooltips();
			KTApp.initBootstrapPopovers();
			KTMenu.createInstances();
		}
	};

	// Filter Datatable
	let handleFilterDatatable = () => {
		document.querySelector('input[data-datatable-filter="search"]').addEventListener('keyup', function (e) {
			landingpagesDatatable.search(e.target.value).draw();
			drawCards(landingpagesDatatable.rows({'search': 'applied'}).data().toArray());
			console.log(landingpagesDatatable.rows({'search': 'applied'}).data().toArray());
		});
		document.querySelector('select[name="datatable-landing-page-status"]').addEventListener('change', function (e) {
			landingpagesDatatable.search(e.target.value === 'all' ? '' : e.target.value).draw();
			drawCards(landingpagesDatatable.rows({'search': 'applied'}).data().toArray());
		});
	};

	// Delete scope
	let handleActions = () => {
		landingPagesListContainer.addEventListener('click', function (e) {
			if (e.target.classList.contains('show-landing-page-details')) {
				let data = landingpagesDatatable.row({id: e.target.dataset.id}).data();
				landingPageDetailsModal._element.querySelector('.landing-page-name').textContent = data.name;
				landingPageDetailsModal._element.querySelector('.landing-page-domain_name').textContent = data.domain_name;
				landingPageDetailsModal._element.querySelector('.landing-page-url').textContent = data.url;
				landingPageDetailsModal._element.querySelector('.landing-page-created_at').textContent = data.created_at;
				landingPageDetailsModal._element.querySelector('.landing-page-status').textContent = data.status_data.name;
				landingPageDetailsModal.show();
			} else if (e.target.classList.contains('cancel-landing-page')) {
				let landingPageId = e.target.dataset.id;
				if (landingPageId > 0) {
					Swal.fire({
						title: Lang.get('messages.confirm_action'),
						text: Lang.choice('actions.validate_cancel', 1, {attribute: Lang.choice('names.landing_page', 1)}),
						icon: "question",
						showCancelButton: true,
					}).then(function (result) {
						if (result.value) {
							pageBlock.block();
							axios.delete(route('organization.landing-pages.destroy', landingPageId))
								.then(function (response) {
									Swal.fire({
										title: response.data.title,
										html: response.data.html,
										icon: response.data.icon,
										willClose: function () {
											if (response.data.success) {
												landingpagesDatatable.ajax.reload();
											}
										}
									});
								})
								.catch(() => errorSwal())
								.then(function () {
									pageBlock.release();
								});
						}
					});
				} else {
					errorSwal();
				}
			}
		});
	};

	return {
		// public functions
		init: function () {
			window.modelsListTable = document.querySelector('#landing-pages-table');
			landingPagesListContainer = document.querySelector('#landing-pages-list');
			landingPageDetailsModal = new bootstrap.Modal(document.querySelector('#landing-page-details-modal'), {});
			initDatatable();
			handleFilterDatatable();
			handleActions();
		}
	};
}
();
landingpagesIndexComponent.init();