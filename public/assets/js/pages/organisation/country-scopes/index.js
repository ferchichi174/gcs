let organizationCountryScopesIndexComponent = function () {
	let newOrganizationCountryScopeForm;
	let initDatatable = function () {
		window.organizationCountryScopesDatatable = DTable(modelsListTable, {
			// dom: `<'row'<'col-sm-12'tr>><'row datatable-controls'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			processing: false,
			serverSide: false,
			ajax: {
				url: route('organization.country-scopes.filtered-index'),
				type: 'POST',
				complete: function (response) {
					pageBlock.release();
					if (response.responseJSON.data && response.responseJSON.data.length > 0) {
						let countriesList = newOrganizationCountryScopeForm.querySelector('select[name="code"]');
						countriesList.querySelectorAll('option').forEach(item => (item.text !== '' ? item.disabled = false : ''));
						response.responseJSON.data.forEach(function (item) {
							countriesList.querySelector(`option[value="${item.code}"]`).disabled = true;
						});
					} else {
						Swal.fire({
							title: Lang.get('messages.no_data'),
							text: Lang.get('messages.empty_table'),
							icon: 'info'
						});
					}
				}
			},
			columns: [
				{data: 'id', name: 'id'},
				{data: 'name', name: 'name'},
				{data: 'status_data', name: 'status'},
				{data: 'id', name: 'actions', sortable: false, searchable: false},
			],
			columnDefs: [
				{
					targets: 1,
					render: function (name, type, item) {
						return `<div class="d-flex align-items-center">
							<div class="me-5 position-relative">
								<div class="symbol symbol-35px symbol-circle">
									<img alt="Pic" src="${item.image}">
								</div>
							</div>
							<div class="d-flex flex-column justify-content-center">
								<a href="" class="fs-6 text-gray-800 text-hover-primary">${name}</a>
								<div class="fw-bold text-gray-400">${Lang.choice('attributes.code', 1) + ': ' + item.code}</div>
							</div>
						</div>`;
					}
				},
				{
					targets: -2,
					render: function (status_data) {
						return `<span class="badge badge-light-${status_data.class} fw-bolder px-4 py-3">${status_data.name}</span>`;
					}
				},
				{
					targets: -1,
					className: 'text-end',
					render: function (id, type, item) {
						return `<button class="btn btn-light btn-sm ${item.can_be_disabled ? 'disable-organization-country-scope' : 'enable-organization-country-scope'}">${item.can_be_disabled ? Lang.get('actions.disable') : Lang.get('actions.enable')}</button>`;
					},
				}
			],
		});
	};

	// Search Datatable
	let handleSearchDatatable = function () {
		document.querySelector('input[data-datatable-filter="search"]').addEventListener('keyup', function (e) {
			organizationCountryScopesDatatable.search(e.target.value).draw();
		});
	};

	// Filter Datatable
	let handleFilterDatatable = () => {
		document.querySelector('select[name="datatable-scope-status"]').addEventListener('change', function (e) {
			console.log(organizationCountryScopesDatatable);
			organizationCountryScopesDatatable.search(e.target.value === 'all' ? '' : e.target.value).draw();
		});
	};

	// Delete scope
	let handleStatusChangingActions = () => {
		organizationCountryScopesDatatable.table().container().addEventListener('click', function (e) {
			if (e.target.classList.contains('disable-organization-country-scope')) {
				let scopeId = organizationCountryScopesDatatable.row(e.target.closest('tr')).data().id;
				Swal.fire({
					title: Lang.get("messages.confirm_action"),
					text: Lang.choice("actions.validate_disable", 1, {attribute: Lang.choice("names.country_scope", 1)}),
					icon: "warning",
					showCancelButton: true,
				}).then(function (result) {
					if (result.value) {
						pageBlock.block();
						axios.get(route('organization.country-scopes.disable', scopeId))
							.then(function (response) {
								Swal.fire({
									title: response.data.title,
									html: response.data.html,
									icon: response.data.icon,
									willClose: function () {
										if (response.data.success) {
											organizationCountryScopesDatatable.ajax.reload();
										}
									}
								});
							})
							.catch(() => errorSwal())
							.then(function () {
								pageBlock.release();
							});
					}
				});
			} else if (e.target.classList.contains('enable-organization-country-scope')) {
				let scopeId = organizationCountryScopesDatatable.row(e.target.closest('tr')).data().id;
				Swal.fire({
					title: Lang.get("messages.confirm_action"),
					text: Lang.choice("actions.validate_enable", 1, {attribute: Lang.choice("names.country_scope", 1)}),
					icon: "warning",
					showCancelButton: true,
				}).then(function (result) {
					if (result.value) {
						pageBlock.block();
						axios.get(route('organization.country-scopes.enable', scopeId))
							.then(function (response) {
								Swal.fire({
									title: response.data.title,
									html: response.data.html,
									icon: response.data.icon,
									willClose: function () {
										if (response.data.success) {
											organizationCountryScopesDatatable.ajax.reload();
										}
									}
								});
							})
							.catch(() => errorSwal())
							.then(function () {
								pageBlock.release();
							});
					}
				});
			}
		});
	};

	// Add new scope
	let handleCreateAction = () => {
		let validator = FormValidation.formValidation(
			newOrganizationCountryScopeForm,
			{
				fields: {
					code: {
						validators: {
							notEmpty: {
								message: Lang.get('validation.required', {attribute: Lang.choice('attributes.country', 1)})
							}
						}
					},
				},

				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap5({
						rowSelector: '.fv-row',
						eleInvalidClass: '',
						eleValidClass: ''
					})
				}
			}
		);

		newOrganizationCountryScopeForm.addEventListener('submit', function (e) {
			e.preventDefault();
			if (validator) {
				validator.validate().then(function (status) {
					if (status === 'Valid') {
						Swal.fire({
							title: Lang.get("messages.confirm_action"),
							text: Lang.choice("actions.validate_add", 1, {attribute: Lang.choice("names.country_scope", 1)}),
							icon: "info",
							showCancelButton: true,
						}).then(function (result) {
							if (result.value) {
								pageBlock.block();
								axios.post(route('organization.country-scopes.store'), {code: newOrganizationCountryScopeForm.querySelector('[name="code"]').value})
									.then(function (response) {
										Swal.fire({
											title: response.data.title,
											html: response.data.html,
											icon: response.data.icon,
											willClose: function () {
												if (response.data.success) {
													bootstrap.Modal.getInstance(newOrganizationCountryScopeForm.closest('.modal')).hide();
													organizationCountryScopesDatatable.ajax.reload();
												}
											}
										});
									})
									.catch(() => errorSwal())
									.then(function () {
										pageBlock.release();
									});
							}
						});
					}
				});
			}
		});
	};

	return {
		// public functions
		init: function () {
			window.modelsListTable = document.querySelector('#organization-country-scopes-table');
			newOrganizationCountryScopeForm = document.querySelector('#new-organization-country-scope-form');
			initDatatable();
			handleSearchDatatable();
			handleFilterDatatable();
			handleStatusChangingActions();
			handleCreateAction();
		}
	};
}
();
organizationCountryScopesIndexComponent.init();