let contactsIndexComponent = function () {
	let newContactForm;
	let initDatatable = function () {
		window.contactsDatatable = DTable(modelsListTable, {
			processing: true,
			serverSide: false,
			ajax: {
				url: route('contacts.filtered-index'),
				type: 'POST',
				complete: function (response) {
					pageBlock.release();
					if (response.responseJSON.data && response.responseJSON.data.length > 0) {
					} else {
						Swal.fire({
							title: Lang.get('messages.no_data'),
							text: Lang.get('messages.empty_table'),
							icon: 'info'
						});
					}
				}
			},
			columns: [
				{data: 'id', name: 'id'},
				{data: 'id', name: 'id'},
				{data: 'last_name', name: 'last_name'},
				{data: 'first_name', name: 'first_name'},
				{data: 'middle_name', name: 'middle_name'},
				{data: 'gender', name: 'gender'},
				{data: 'country', name: 'country'},
				{data: 'language', name: 'language'},
				{data: 'birth_date', name: 'birth_date'},
				{data: 'creation_date', name: 'creation_date'},
				{data: 'creation_type', name: 'creation_type'},
				{data: 'creator', name: 'creator'},
				{data: 'creator_details', name: 'creator_details'},
				{data: 'detailed_status', name: 'detailed_status'},
				{data: 'phone', name: 'phone'},
				{data: 'phone_type', name: 'phone_type'},
				{data: 'email', name: 'email'},
				{data: 'email_type', name: 'email_type'},
				{data: 'fax', name: 'fax'},
				{data: 'verification_status', name: 'verification_status'},
				{data: 'verification_type', name: 'verification_type'},
				{data: 'verification_details', name: 'verification_details'},
				{data: 'verification_date', name: 'verification_date'},
				{data: 'phone_verification_flag', name: 'phone_verification_flag'},
				{data: 'email_verification_flag', name: 'email_verification_flag'},
				{data: 'fax_verification_flag', name: 'fax_verification_flag'},
				{data: 'do_not_deal', name: 'do_not_deal'},
				{data: 'do_not_deal_date', name: 'do_not_deal_date'},
				{data: 'do_not_deal_declarant', name: 'do_not_deal_declarant'},
				{data: 'do_not_deal_reason', name: 'do_not_deal_reason'},
				{data: 'do_not_call', name: 'do_not_call'},
				{data: 'do_not_call_date', name: 'do_not_call_date'},
				{data: 'do_not_call_declarant', name: 'do_not_call_declarant'},
				{data: 'do_not_call_reason', name: 'do_not_call_reason'},
				{data: 'do_not_chat', name: 'do_not_chat'},
				{data: 'do_not_chat_date', name: 'do_not_chat_date'},
				{data: 'do_not_chat_declarant', name: 'do_not_chat_declarant'},
				{data: 'do_not_chat_reason', name: 'do_not_chat_reason'},
				{data: 'do_not_text', name: 'do_not_text'},
				{data: 'do_not_text_date', name: 'do_not_text_date'},
				{data: 'do_not_text_declarant', name: 'do_not_text_declarant'},
				{data: 'do_not_text_reason', name: 'do_not_text_reason'},
				{data: 'do_not_email', name: 'do_not_email'},
				{data: 'do_not_email_date', name: 'do_not_email_date'},
				{data: 'do_not_email_declarant', name: 'do_not_email_declarant'},
				{data: 'do_not_email_reason', name: 'do_not_email_reason'},
				{data: 'personal_group', name: 'personal_group'},
				{data: 'id', name: 'actions', sortable: false, searchable: false},
			],
			columnDefs: [
				{
					targets: -1,
					className: 'text-end',
					render: function (id, type, item) {
						return `<button class="btn btn-light btn-sm ${item.can_be_disabled ? 'disable-organization-country-scope' : 'enable-organization-country-scope'}">${item.can_be_disabled ? Lang.get('actions.disable') : Lang.get('actions.enable')}</button>`;
					},
				}
			],
			responsive: false,
			scrollX: true,
			scrollCollapse: true,
			fixedColumns: {
				left: 4,
				right: 1
			}
		});
	};

	// Search Datatable
	let handleSearchDatatable = function () {
		document.querySelector('input[data-datatable-filter="search"]').addEventListener('keyup', function (e) {
			contactsDatatable.search(e.target.value).draw();
		});
	};

	// Filter Datatable
	let handleFilterDatatable = () => {
		document.querySelector('select[name="datatable-scope-status"]').addEventListener('change', function (e) {
			console.log(contactsDatatable);
			contactsDatatable.search(e.target.value === 'all' ? '' : e.target.value).draw();
		});
	};

	// Delete scope
	let handleStatusChangingActions = () => {
		contactsDatatable.table().container().addEventListener('click', function (e) {
			if (e.target.classList.contains('disable-organization-country-scope')) {
				let scopeId = contactsDatatable.row(e.target.closest('tr')).data().id;
				Swal.fire({
					title: Lang.get("messages.confirm_action"),
					text: Lang.choice("actions.validate_disable", 1, {attribute: Lang.choice("names.country_scope", 1)}),
					icon: "warning",
					showCancelButton: true,
				}).then(function (result) {
					if (result.value) {
						pageBlock.block();
						axios.get(route('contacts.disable', scopeId))
							.then(function (response) {
								Swal.fire({
									title: response.data.title,
									html: response.data.html,
									icon: response.data.icon,
									willClose: function () {
										if (response.data.success) {
											contactsDatatable.ajax.reload();
										}
									}
								});
							})
							.catch(() => errorSwal())
							.then(function () {
								pageBlock.release();
							});
					}
				});
			} else if (e.target.classList.contains('enable-organization-country-scope')) {
				let scopeId = contactsDatatable.row(e.target.closest('tr')).data().id;
				Swal.fire({
					title: Lang.get("messages.confirm_action"),
					text: Lang.choice("actions.validate_enable", 1, {attribute: Lang.choice("names.country_scope", 1)}),
					icon: "warning",
					showCancelButton: true,
				}).then(function (result) {
					if (result.value) {
						pageBlock.block();
						axios.get(route('contacts.enable', scopeId))
							.then(function (response) {
								Swal.fire({
									title: response.data.title,
									html: response.data.html,
									icon: response.data.icon,
									willClose: function () {
										if (response.data.success) {
											contactsDatatable.ajax.reload();
										}
									}
								});
							})
							.catch(() => errorSwal())
							.then(function () {
								pageBlock.release();
							});
					}
				});
			}
		});
	};

	// Add new scope
	let handleCreateAction = () => {
		let validator = FormValidation.formValidation(
			newContactForm,
			{
				fields: {
					code: {
						validators: {
							notEmpty: {
								message: Lang.get('validation.required', {attribute: Lang.choice('attributes.country', 1)})
							}
						}
					},
				},

				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap5({
						rowSelector: '.fv-row',
						eleInvalidClass: '',
						eleValidClass: ''
					})
				}
			}
		);

		newContactForm.addEventListener('submit', function (e) {
			e.preventDefault();
			if (validator) {
				validator.validate().then(function (status) {
					if (status === 'Valid') {
						Swal.fire({
							title: Lang.get("messages.confirm_action"),
							text: Lang.choice("actions.validate_add", 1, {attribute: Lang.choice("names.country_scope", 1)}),
							icon: "info",
							showCancelButton: true,
						}).then(function (result) {
							if (result.value) {
								pageBlock.block();
								axios.post(route('contacts.store'), {code: newContactForm.querySelector('[name="code"]').value})
									.then(function (response) {
										Swal.fire({
											title: response.data.title,
											html: response.data.html,
											icon: response.data.icon,
											willClose: function () {
												if (response.data.success) {
													bootstrap.Modal.getInstance(newContactForm.closest('.modal')).hide();
													contactsDatatable.ajax.reload();
												}
											}
										});
									})
									.catch(() => errorSwal())
									.then(function () {
										pageBlock.release();
									});
							}
						});
					}
				});
			}
		});
	};

	return {
		// public functions
		init: function () {
			window.modelsListTable = document.querySelector('#contacts-table');
			newContactForm = document.querySelector('#new-contact-form');
			initDatatable();
			// handleSearchDatatable();
			// handleFilterDatatable();
			// handleStatusChangingActions();
			// handleCreateAction();
		}
	};
}
();
contactsIndexComponent.init();