<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactPoliciesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contact_policies', function (Blueprint $table) {
			$table->id();
			$table->foreignId('contact_id')->constrained('contacts');
			$table->unsignedTinyInteger('action');
			$table->dateTime('action_date');
			$table->unsignedInteger('declarer_id');
			$table->unsignedInteger('reason_id')->nullable();
			$table->unsignedTinyInteger('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contact_policies');
	}
}
