<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandingPagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landing_pages', function (Blueprint $table) {
			$table->id();
			$table->foreignId('account_id')->constrained('accounts');
			$table->string('name', 100);
			$table->string('domain_name', 100)->nullable();
			$table->string('url', 128);
			$table->string('screenshot', 255)->nullable();
			$table->unsignedTinyInteger('status');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('landing_pages');
	}
}
