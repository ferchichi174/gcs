<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function (Blueprint $table) {
			$table->id();
			$table->foreignId('account_id')->constrained('accounts');
			$table->unsignedTinyInteger('gender');
			$table->string('first_name', 100);
			$table->string('last_name', 100);
			$table->string('middle_name', 100)->nullable();
			$table->string('address', 128)->nullable();
			$table->string('city', 100)->nullable();
			$table->string('state', 100)->nullable();
			$table->string('postal_code', 32)->nullable();
			$table->char('country', 2);
			$table->string('languages', 32);
			$table->date('birth_date')->nullable();
			$table->unsignedTinyInteger('creation_type')->nullable();
			$table->unsignedInteger('creator_id')->nullable();
			$table->string('creator_details', 255)->nullable();
			$table->unsignedTinyInteger('detailed_status');
			$table->unsignedTinyInteger('verification_status');
			$table->unsignedTinyInteger('verification_type')->nullable();
			$table->string('verification_details', 255)->nullable();
			$table->dateTime('verification_date')->nullable();
			$table->unsignedInteger('import_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contacts');
	}
}
