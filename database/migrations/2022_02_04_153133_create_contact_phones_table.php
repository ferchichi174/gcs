<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactPhonesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contact_phones', function (Blueprint $table) {
			$table->id();
			$table->foreignId('contact_id')->constrained('contacts');
			$table->unsignedTinyInteger('class');
			$table->string('number', 32)->nullable();
			$table->unsignedTinyInteger('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('contact_phones');
	}
}
