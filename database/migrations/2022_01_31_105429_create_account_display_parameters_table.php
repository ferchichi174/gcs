<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountDisplayParametersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('account_display_parameters', function (Blueprint $table) {
			$table->id();
			$table->foreignId('account_id')->constrained('accounts');
			$table->unsignedTinyInteger('username');
			$table->unsignedTinyInteger('date_format');
			$table->unsignedTinyInteger('time_format');
			$table->string('time_zone', 64);
			$table->char('language', 2);
			$table->unsignedTinyInteger('country_format');
			$table->unsignedTinyInteger('currency_format');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('account_display_parameters');
	}
}
