<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountCommunicationTypesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('account_communication_types', function (Blueprint $table) {
			$table->id();
			$table->foreignId('account_id')->constrained('accounts');
			$table->foreignId('communication_type_id')->constrained('communication_types');
			$table->unsignedTinyInteger('status');
			$table->timestamps();
			$table->softDeletes();
			$table->index('account_id');
			$table->index('communication_type_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('account_communication_types');
	}
}
