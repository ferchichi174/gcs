<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title',100);
            $table->unsignedTinyInteger('task');
            $table->unsignedTinyInteger('status');
            $table->string('details');
            $table->integer('creator');
            $table->integer('recipient');
            $table->dateTime('scheduling');
            $table->integer('communication_id');
            $table->foreignId('contact_id')->constrained('contacts');
            $table->foreignId('user_id')->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
