<?php

namespace Database\Seeders;

use App\Models\Accounts\Account;
use App\Models\Organization\CommunicationType;
use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Account::create(['name' => 'Test Account', 'status' => Account::STATUS_ACTIVE]);
		$now = now();
		CommunicationType::insert(collect(CommunicationType::communicationTypesList())->map(fn($communicationType, $id) => [
			'id' => $id,
			'name' => $communicationType['title'],
			'status' => $communicationType['status'] == 'active' ? CommunicationType::STATUS_AVAILABLE : ($communicationType['status'] == 'coming_soon' ? CommunicationType::STATUS_COMING_SOON : CommunicationType::STATUS_UNAVAILABLE),
			'created_at' => $now,
			'updated_at' => $now,
		])->toArray());
	}
}
