<?php

return [
	'active' => 'Active',
	'inactive' => 'Inactive',
	'enabled' => 'Enabled',
	'disabled' => 'Disabled',
	'n/a' => 'N/a',
	'all' => 'All',
	'available' => 'Available',
	'unavailable' => 'Unavailable',
	'coming_soon' => 'Coming Soon',
	'connected' => 'Connected',
	'declined' => 'Declined',
	'in_progress' => 'In progress',
];