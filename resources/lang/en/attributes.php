<?php

return [
	'id' => 'ID|IDs',
	'name' => 'Name|Names',
	'status' => 'Status|Statuses',
	'detail' => 'Detail|Details',
	'action' => 'Action|Actions',
	'country' => 'Country|Countries',
	'code' => 'Code|Codes',
	'date' => 'Date|Dates',
	'domain_name' => 'Domain name|Domain names',
	'url' => 'URL|URLs',
	'starting_date' => 'Starting date|Starting dates',
	'last_name' => 'Last Name',
	'first_name' => 'First Name',
	'middle_name' => 'Middle Name',
	'gender' => 'Gender|Genders',
	'language' => 'Language|Languages',
	'attribute_status' => ':attribute status|:attribute statuses',
	'verification' => 'Verification',
	'attribute_type' => ':attribute type|:attribute types',
	'attribute_date' => ':attribute date|:attribute dates',
	'fax' => 'Fax|Faxes',
	'email' => 'Email|Emails',
	'phone' => 'Phone|Phones',
	'creator' => 'Creator|Creators',
	'creation_date' => 'Creation date|Creation dates',
	'birth_date' => 'Birth date|Birth dates',
	'attribute_detail' => ':attribute detail|:attribute details',
	'attribute_verification_flag' => ':attribute verification flag|:attribute verification flags',
	'declarant' => 'Declarant|Declarants',
	'reason' => 'Reason|Reasons',
	'personal_group' => 'Personal Group|Personal Groups',
];