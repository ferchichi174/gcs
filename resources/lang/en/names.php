<?php

return [
	'country_scope' => 'Country Scope|Country Scopes',
	'communication' => 'Communication|Communications',
	'communication_type' => 'Communication type|Communication types',
	'integration' => 'Integration|Integrations',
	'report_display' => 'Report display|Reports display',
	'landing_page' => 'Landing page|Landing pages',
	'team' => 'Team|Teams',
	'role' => 'Role|Roles',
	'extension' => 'Extension|Extensions',
	'user' => 'User|Users',
	'contact' => 'Contact|Contacts',
	'contact_group' => 'Contact group|Contact groups',
];