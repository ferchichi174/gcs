@extends('base.base')

@section('content')
	<!--begin::Main-->
	<!--begin::Root-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="page d-flex flex-row flex-column-fluid">
		{{ theme()->getView('layout/aside/_base') }}
		<!--begin::Wrapper-->
			<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
			{{ theme()->getView('layout/header/_base') }}
			
			{{ theme()->getView('layout/_toolbar') }}
			<!--begin::Content-->
				<div class="content d-flex flex-column flex-column-fluid pt-3" id="kt_content">
					<!--begin::Container-->
					<div id="kt_content_container" class="{{ theme()->printHtmlClasses('content-container', false) }}">
						@yield('content-container')
					</div>
					<!--end::Container-->
				
				</div>
				<!--end::Content-->
				{{ theme()->getView('layout/_footer') }}
			</div>
			<!--end::Wrapper-->
		</div>
		<!--end::Page-->
	</div>
	<!--end::Root-->
	
	<!--begin::Drawers-->
	{{ theme()->getView('partials/topbar/_activity-drawer') }}
	<!--end::Drawers-->
	
	{{ theme()->getView('layout/_scrolltop') }}
	<!--end::Main-->

@endsection
