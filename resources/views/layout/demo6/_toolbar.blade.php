@php
	$pageTitleDisplay = (theme()->getOption('layout', 'page-title/display') && theme()->getOption('layout', 'header/left') !== 'page-title');
@endphp

<!--begin::Toolbar-->
<div class="toolbar py-2" id="kt_toolbar">
	<!--begin::Container-->
	<div id="kt_toolbar_container" class="{{ theme()->printHtmlClasses('toolbar-container', false) }} d-flex align-items-center">
	@if ($pageTitleDisplay)
		<!--begin::Page title-->
			<div class="flex-grow-1 flex-shrink-0 me-5">
				{{ theme()->getView('layout/_page-title') }}
			</div>
			<!--end::Page title-->
	@endif
	
	<!--begin::Action group-->
		<div class="d-flex {{ $pageTitleDisplay ? 'align-items-center' : 'flex-stack flex-grow-1' }} flex-wrap">
			@yield('toolbar')
		</div>
		<!--end::Action group-->
	</div>
	<!--end::Container-->
</div>
<!--end::Toolbar-->
