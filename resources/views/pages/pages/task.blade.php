<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="../../">
		<title>GoCC</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				{{ theme()->getView('pages/aside') }}
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					{{ theme()->getView('pages/header') }}
					<!--end::Header-->
					<!--begin::Toolbar-->
					<div class="toolbar py-2" id="kt_toolbar">
						<!--begin::Container-->
						<div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
							<!--begin::Page title-->
							<div class="flex-grow-1 flex-shrink-0 me-5">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">View Subscriptions</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-3"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">Tasks</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">View Task</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
							</div>
							<!--end::Page title-->
							<!--begin::Action group-->
							<div class="d-flex align-items-center flex-wrap">
								<!--begin::Wrapper-->
								<div class="d-flex align-items-center">
									<!--begin::Actions-->
									<div class="d-flex align-items-center">
										<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light btn-active-color-primary">
											<!--begin::Svg Icon | path: icons/duotune/files/fil005.svg-->
											<span class="svg-icon svg-icon-2x">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13H13V10C13 9.4 12.6 9 12 9C11.4 9 11 9.4 11 10V13H8C7.4 13 7 13.4 7 14C7 14.6 7.4 15 8 15H11V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18V15H16C16.6 15 17 14.6 17 14C17 13.4 16.6 13 16 13Z" fill="black" />
													<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
										</button>
									</div>
									<!--end::Actions-->
								</div>
								<!--end::Wrapper-->
							</div>
							<!--end::Action group-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Toolbar-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-fluidssssss">
							<!--begin::Layout-->
							<div class="d-flex flex-column flex-lg-row">
								<!--begin::Content-->
								<div class="flex-lg-row-fluid me-lg-15 order-2 order-lg-1 mb-10 mb-lg-0">
									<!--begin::Card-->
									<div class="card card-flush pt-3 mb-5 mb-xl-10">
										<!--begin::Card header-->
										<div class="card-header">
											<!--begin::Card title-->
											<div class="card-title">
												<h2 class="fw-bolder">Product Details</h2>
											</div>
											<!--begin::Card title-->
											<!--begin::Card toolbar-->
											<div class="card-toolbar">
												<a href="../../demo6/dist/apps/subscriptions/add.html" class="btn btn-light-primary">Update Product</a>
											</div>
											<!--end::Card toolbar-->
										</div>
										<!--end::Card header-->
										<!--begin::Card body-->
										<div class="card-body pt-3">
											<!--begin::Section-->
											<div class="mb-10">
												<!--begin::Title-->
												<h5 class="mb-4">Billing Address:</h5>
												<!--end::Title-->
												<!--begin::Details-->
												<div class="d-flex flex-wrap py-5">
													<!--begin::Row-->
													<div class="flex-equal me-5">
														<!--begin::Details-->
														<table class="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
															<!--begin::Row-->
															<tr>
																<td class="text-gray-400 min-w-175px w-175px">Bill to:</td>
																<td class="text-gray-800 min-w-200px">
																	<a href="../../demo6/dist/pages/apps/customers/view.html" class="text-gray-800 text-hover-primary">e.smith@kpmg.com.au</a>
																</td>
															</tr>
															<!--end::Row-->
															<!--begin::Row-->
															<tr>
																<td class="text-gray-400">Customer Name:</td>
																<td class="text-gray-800">Emma Smith</td>
															</tr>
															<!--end::Row-->
															<!--begin::Row-->
															<tr>
																<td class="text-gray-400">Address:</td>
																<td class="text-gray-800">Floor 10, 101 Avenue of the Light Square, New York, NY, 10050.</td>
															</tr>
															<!--end::Row-->
															<!--begin::Row-->
															<tr>
																<td class="text-gray-400">Phone:</td>
																<td class="text-gray-800">(555) 555-1234</td>
															</tr>
															<!--end::Row-->
														</table>
														<!--end::Details-->
													</div>
													<!--end::Row-->
													<!--begin::Row-->
													<div class="flex-equal">
														<!--begin::Details-->
														<table class="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
															<!--begin::Row-->
															<tr>
																<td class="text-gray-400 min-w-175px w-175px">Subscribed Product:</td>
																<td class="text-gray-800 min-w-200px">
																	<a href="#" class="text-gray-800 text-hover-primary">Basic Bundle</a>
																</td>
															</tr>
															<!--end::Row-->
															<!--begin::Row-->
															<tr>
																<td class="text-gray-400">Subscription Fees:</td>
																<td class="text-gray-800">$149.99 / Year</td>
															</tr>
															<!--end::Row-->
															<!--begin::Row-->
															<tr>
																<td class="text-gray-400">Billing method:</td>
																<td class="text-gray-800">Annually</td>
															</tr>
															<!--end::Row-->
															<!--begin::Row-->
															<tr>
																<td class="text-gray-400">Currency:</td>
																<td class="text-gray-800">USD - US Dollar</td>
															</tr>
															<!--end::Row-->
														</table>
														<!--end::Details-->
													</div>
													<!--end::Row-->
												</div>
												<!--end::Row-->
											</div>
											<!--end::Section-->
											<!--begin::Section-->
											<div class="mb-0">
												<!--begin::Title-->
												<h5 class="mb-4">Subscribed Products:</h5>
												<!--end::Title-->
												<!--begin::Product table-->
												<div class="table-responsive">
													<!--begin::Table-->
													<table class="table align-middle table-row-dashed fs-6 gy-4 mb-0">
														<!--begin::Table head-->
														<thead>
															<!--begin::Table row-->
															<tr class="border-bottom border-gray-200 text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
																<th class="min-w-150px">Product</th>
																<th class="min-w-125px">Subscription ID</th>
																<th class="min-w-125px">Qty</th>
																<th class="min-w-125px">Total</th>
																<th class="text-end min-w-70px">Actions</th>
															</tr>
															<!--end::Table row-->
														</thead>
														<!--end::Table head-->
														<!--begin::Table body-->
														<tbody class="fw-bold text-gray-800">
															<tr>
																<td>
																	<label class="w-150px">Basic Bundle</label>
																	<div class="fw-normal text-gray-600">Basic yearly bundle</div>
																</td>
																<td>
																	<span class="badge badge-light-danger">sub_4567_8765</span>
																</td>
																<td>1</td>
																<td>$149.99 / Year</td>
																<td class="text-end">
																	<!--begin::Action-->
																	<a href="#" class="btn btn-icon btn-active-light-primary w-30px h-30px" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
																		<!--begin::Svg Icon | path: icons/duotune/general/gen019.svg-->
																		<span class="svg-icon svg-icon-3">
																			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																				<path d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z" fill="black" />
																				<path opacity="0.3" d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z" fill="black" />
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</a>
																	<!--begin::Menu-->
																	<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-6 w-200px py-4" data-kt-menu="true">
																		<!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="#" class="menu-link px-3">Pause Subscription</a>
																		</div>
																		<!--end::Menu item-->
																		<!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="#" class="menu-link px-3" data-kt-subscriptions-view-action="delete">Edit Subscription</a>
																		</div>
																		<!--end::Menu item-->
																		<!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="#" class="menu-link text-danger px-3" data-kt-subscriptions-view-action="edit">Cancel Subscription</a>
																		</div>
																		<!--end::Menu item-->
																	</div>
																	<!--end::Menu-->
																	<!--end::Action-->
																</td>
															</tr>
															<tr>
																<td>
																	<label class="w-150px">Pro Bundle</label>
																	<div class="fw-normal text-gray-400">Basic yearly bundle</div>
																</td>
																<td>
																	<span class="badge badge-light-danger">sub_4567_3433</span>
																</td>
																<td>5</td>
																<td>$949.99 / Year</td>
																<td class="text-end">
																	<!--begin::Action-->
																	<a href="#" class="btn btn-icon btn-active-light-primary w-30px h-30px" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
																		<!--begin::Svg Icon | path: icons/duotune/general/gen019.svg-->
																		<span class="svg-icon svg-icon-3">
																			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																				<path d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z" fill="black" />
																				<path opacity="0.3" d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z" fill="black" />
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</a>
																	<!--begin::Menu-->
																	<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-6 w-200px py-4" data-kt-menu="true">
																		<!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="#" class="menu-link px-3">Pause Subscription</a>
																		</div>
																		<!--end::Menu item-->
																		<!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="#" class="menu-link px-3" data-kt-subscriptions-view-action="delete">Edit Subscription</a>
																		</div>
																		<!--end::Menu item-->
																		<!--begin::Menu item-->
																		<div class="menu-item px-3">
																			<a href="#" class="menu-link text-danger px-3" data-kt-subscriptions-view-action="edit">Cancel Subscription</a>
																		</div>
																		<!--end::Menu item-->
																	</div>
																	<!--end::Menu-->
																	<!--end::Action-->
																</td>
															</tr>
														</tbody>
														<!--end::Table body-->
													</table>
													<!--end::Table-->
												</div>
												<!--end::Product table-->
											</div>
											<!--end::Section-->
										</div>
										<!--end::Card body-->
									</div>
									<!--end::Card-->
									<!--begin::Card-->
									<div class="card card-flush pt-3 mb-5 mb-xl-10">
										<!--begin::Card header-->
										<div class="card-header">
											<!--begin::Card title-->
											<div class="card-title">
												<h2>Recent Events</h2>
											</div>
											<!--end::Card title-->
											<!--begin::Card toolbar-->
											<div class="card-toolbar">
												<a href="#" class="btn btn-light-primary">View All Events</a>
											</div>
											<!--end::Card toolbar-->
										</div>
										<!--end::Card header-->
										<!--begin::Card body-->
										<div class="card-body pt-0">
											<!--begin::Table wrapper-->
											<div class="table-responsive">
												<!--begin::Table-->
												<table class="table align-middle table-row-dashed fs-6 text-gray-600 fw-bold gy-5" id="kt_table_customers_events">
													<!--begin::Table body-->
													<tbody>
														<!--begin::Table row-->
														<tr>
															<!--begin::Event=-->
															<td class="min-w-400px">Invoice
															<a href="#" class="fw-bolder text-gray-800 text-hover-primary me-1">6240-4047</a>status has changed from
															<span class="badge badge-light-info me-1">In Progress</span>to
															<span class="badge badge-light-primary">In Transit</span></td>
															<!--end::Event=-->
															<!--begin::Timestamp=-->
															<td class="pe-0 text-gray-600 text-end min-w-200px">10 Nov 2021, 11:05 am</td>
															<!--end::Timestamp=-->
														</tr>
														<!--end::Table row-->
														<!--begin::Table row-->
														<tr>
															<!--begin::Event=-->
															<td class="min-w-400px">
															<a href="#" class="fw-bolder text-gray-800 text-hover-primary me-1">Max Smith</a>has made payment to
															<a href="#" class="fw-bolder text-gray-800 text-hover-primary">1036-5276</a></td>
															<!--end::Event=-->
															<!--begin::Timestamp=-->
															<td class="pe-0 text-gray-600 text-end min-w-200px">10 Nov 2021, 9:23 pm</td>
															<!--end::Timestamp=-->
														</tr>
														<!--end::Table row-->
														<!--begin::Table row-->
														<tr>
															<!--begin::Event=-->
															<td class="min-w-400px">
															<a href="#" class="fw-bolder text-gray-800 text-hover-primary me-1">Melody Macy</a>has made payment to
															<a href="#" class="fw-bolder text-gray-800 text-hover-primary">1111-7185</a></td>
															<!--end::Event=-->
															<!--begin::Timestamp=-->
															<td class="pe-0 text-gray-600 text-end min-w-200px">15 Apr 2021, 11:05 am</td>
															<!--end::Timestamp=-->
														</tr>
														<!--end::Table row-->
														<!--begin::Table row-->
														<tr>
															<!--begin::Event=-->
															<td class="min-w-400px">
															<a href="#" class="fw-bolder text-gray-800 text-hover-primary me-1">Max Smith</a>has made payment to
															<a href="#" class="fw-bolder text-gray-800 text-hover-primary">1036-5276</a></td>
															<!--end::Event=-->
															<!--begin::Timestamp=-->
															<td class="pe-0 text-gray-600 text-end min-w-200px">15 Apr 2021, 9:23 pm</td>
															<!--end::Timestamp=-->
														</tr>
														<!--end::Table row-->
														<!--begin::Table row-->
														<tr>
															<!--begin::Event=-->
															<td class="min-w-400px">Invoice
															<a href="#" class="fw-bolder text-gray-800 text-hover-primary me-1">6240-4047</a>status has changed from
															<span class="badge badge-light-info me-1">In Progress</span>to
															<span class="badge badge-light-primary">In Transit</span></td>
															<!--end::Event=-->
															<!--begin::Timestamp=-->
															<td class="pe-0 text-gray-600 text-end min-w-200px">10 Nov 2021, 8:43 pm</td>
															<!--end::Timestamp=-->
														</tr>
														<!--end::Table row-->
													</tbody>
													<!--end::Table body-->
												</table>
												<!--end::Table-->
											</div>
											<!--end::Table wrapper-->
										</div>
										<!--end::Card body-->
									</div>
									<!--end::Card-->
									<!--begin::Card-->
									<div class="card card-flush pt-3 mb-5 mb-xl-10">
										<!--begin::Card header-->
										<div class="card-header">
											<!--begin::Card title-->
											<div class="card-title">
												<h2>Invoices</h2>
											</div>
											<!--end::Card title-->
											<!--begin::Toolbar-->
											<div class="card-toolbar">
												<!--begin::Tab nav-->
												<ul class="nav nav-stretch fs-5 fw-bold nav-line-tabs nav-line-tabs-2x border-transparent" role="tablist">
													<li class="nav-item" role="presentation">
														<a id="kt_referrals_year_tab" class="nav-link text-active-primary active" data-bs-toggle="tab" role="tab" href="#kt_customer_details_invoices_1">This Year</a>
													</li>
													<li class="nav-item" role="presentation">
														<a id="kt_referrals_2019_tab" class="nav-link text-active-primary ms-3" data-bs-toggle="tab" role="tab" href="#kt_customer_details_invoices_2">2020</a>
													</li>
													<li class="nav-item" role="presentation">
														<a id="kt_referrals_2018_tab" class="nav-link text-active-primary ms-3" data-bs-toggle="tab" role="tab" href="#kt_customer_details_invoices_3">2019</a>
													</li>
													<li class="nav-item" role="presentation">
														<a id="kt_referrals_2017_tab" class="nav-link text-active-primary ms-3" data-bs-toggle="tab" role="tab" href="#kt_customer_details_invoices_4">2018</a>
													</li>
												</ul>
												<!--end::Tab nav-->
											</div>
											<!--end::Toolbar-->
										</div>
										<!--end::Card header-->
										<!--begin::Card body-->
										<div class="card-body pt-2">
											<!--begin::Tab Content-->
											<div id="kt_referred_users_tab_content" class="tab-content">
												<!--begin::Tab panel-->
												<div id="kt_customer_details_invoices_1" class="tab-pane fade show active" role="tabpanel">
													<!--begin::Table wrapper-->
													<div class="table-responsive">
														<!--begin::Table-->
														<table id="kt_customer_details_invoices_table_1" class="table align-middle table-row-dashed fs-6 fw-bolder gs-0 gy-4 p-0 m-0">
															<!--begin::Thead-->
															<thead class="border-bottom border-gray-200 fs-7 text-uppercase fw-bolder">
																<tr class="text-start text-gray-400">
																	<th class="min-w-100px">Order ID</th>
																	<th class="min-w-100px">Amount</th>
																	<th class="min-w-100px">Status</th>
																	<th class="min-w-125px">Date</th>
																	<th class="w-100px">Invoice</th>
																</tr>
															</thead>
															<!--end::Thead-->
															<!--begin::Tbody-->
															<tbody class="fs-6 fw-bold text-gray-600">
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">102445788</a>
																	</td>
																	<td class="text-success">$38.00</td>
																	<td>
																		<span class="badge badge-light-warning">Pending</span>
																	</td>
																	<td>Nov 01, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">423445721</a>
																	</td>
																	<td class="text-danger">$-2.60</td>
																	<td>
																		<span class="badge badge-light-info">In progress</span>
																	</td>
																	<td>Oct 24, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">312445984</a>
																	</td>
																	<td class="text-success">$76.00</td>
																	<td>
																		<span class="badge badge-light-success">Approved</span>
																	</td>
																	<td>Oct 08, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">312445984</a>
																	</td>
																	<td class="text-success">$5.00</td>
																	<td>
																		<span class="badge badge-light-warning">Pending</span>
																	</td>
																	<td>Sep 15, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">523445943</a>
																	</td>
																	<td class="text-danger">$-1.30</td>
																	<td>
																		<span class="badge badge-light-danger">Rejected</span>
																	</td>
																	<td>May 30, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
															</tbody>
															<!--end::Tbody-->
														</table>
														<!--end::Table-->
													</div>
													<!--end::Table wrapper-->
												</div>
												<!--end::Tab panel-->
												<!--begin::Tab panel-->
												<div id="kt_customer_details_invoices_2" class="tab-pane fade" role="tabpanel">
													<!--begin::Table wrapper-->
													<div class="table-responsive">
														<!--begin::Table-->
														<table id="kt_customer_details_invoices_table_2" class="table align-middle table-row-dashed fs-6 fw-bolder gs-0 gy-4 p-0 m-0">
															<!--begin::Thead-->
															<thead class="border-bottom border-gray-200 fs-7 text-uppercase fw-bolder">
																<tr class="text-start text-gray-400">
																	<th class="min-w-100px">Order ID</th>
																	<th class="min-w-100px">Amount</th>
																	<th class="min-w-100px">Status</th>
																	<th class="min-w-125px">Date</th>
																	<th class="w-100px">Invoice</th>
																</tr>
															</thead>
															<!--end::Thead-->
															<!--begin::Tbody-->
															<tbody class="fs-6 fw-bold text-gray-600">
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">523445943</a>
																	</td>
																	<td class="text-danger">$-1.30</td>
																	<td>
																		<span class="badge badge-light-warning">Pending</span>
																	</td>
																	<td>May 30, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">231445943</a>
																	</td>
																	<td class="text-success">$204.00</td>
																	<td>
																		<span class="badge badge-light-info">In progress</span>
																	</td>
																	<td>Apr 22, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">426445943</a>
																	</td>
																	<td class="text-success">$31.00</td>
																	<td>
																		<span class="badge badge-light-danger">Rejected</span>
																	</td>
																	<td>Feb 09, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">984445943</a>
																	</td>
																	<td class="text-success">$52.00</td>
																	<td>
																		<span class="badge badge-light-success">Approved</span>
																	</td>
																	<td>Nov 01, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">324442313</a>
																	</td>
																	<td class="text-danger">$-0.80</td>
																	<td>
																		<span class="badge badge-light-info">In progress</span>
																	</td>
																	<td>Jan 04, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
															</tbody>
															<!--end::Tbody-->
														</table>
														<!--end::Table-->
													</div>
													<!--end::Table wrapper-->
												</div>
												<!--end::Tab panel-->
												<!--begin::Tab panel-->
												<div id="kt_customer_details_invoices_3" class="tab-pane fade" role="tabpanel">
													<!--begin::Table wrapper-->
													<div class="table-responsive">
														<!--begin::Table-->
														<table id="kt_customer_details_invoices_table_3" class="table align-middle table-row-dashed fs-6 fw-bolder gs-0 gy-4 p-0 m-0">
															<!--begin::Thead-->
															<thead class="border-bottom border-gray-200 fs-7 text-uppercase fw-bolder">
																<tr class="text-start text-gray-400">
																	<th class="min-w-100px">Order ID</th>
																	<th class="min-w-100px">Amount</th>
																	<th class="min-w-100px">Status</th>
																	<th class="min-w-125px">Date</th>
																	<th class="w-100px">Invoice</th>
																</tr>
															</thead>
															<!--end::Thead-->
															<!--begin::Tbody-->
															<tbody class="fs-6 fw-bold text-gray-600">
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">426445943</a>
																	</td>
																	<td class="text-success">$31.00</td>
																	<td>
																		<span class="badge badge-light-success">Approved</span>
																	</td>
																	<td>Feb 09, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">984445943</a>
																	</td>
																	<td class="text-success">$52.00</td>
																	<td>
																		<span class="badge badge-light-danger">Rejected</span>
																	</td>
																	<td>Nov 01, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">324442313</a>
																	</td>
																	<td class="text-danger">$-0.80</td>
																	<td>
																		<span class="badge badge-light-info">In progress</span>
																	</td>
																	<td>Jan 04, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">312445984</a>
																	</td>
																	<td class="text-success">$5.00</td>
																	<td>
																		<span class="badge badge-light-danger">Rejected</span>
																	</td>
																	<td>Sep 15, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">102445788</a>
																	</td>
																	<td class="text-success">$38.00</td>
																	<td>
																		<span class="badge badge-light-warning">Pending</span>
																	</td>
																	<td>Nov 01, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
															</tbody>
															<!--end::Tbody-->
														</table>
														<!--end::Table-->
													</div>
													<!--end::Table wrapper-->
												</div>
												<!--end::Tab panel-->
												<!--begin::Tab panel-->
												<div id="kt_customer_details_invoices_4" class="tab-pane fade" role="tabpanel">
													<!--begin::Table wrapper-->
													<div class="table-responsive">
														<!--begin::Table-->
														<table id="kt_customer_details_invoices_table_4" class="table align-middle table-row-dashed fs-6 fw-bolder gs-0 gy-4 p-0 m-0">
															<!--begin::Thead-->
															<thead class="border-bottom border-gray-200 fs-7 text-uppercase fw-bolder">
																<tr class="text-start text-gray-400">
																	<th class="min-w-100px">Order ID</th>
																	<th class="min-w-100px">Amount</th>
																	<th class="min-w-100px">Status</th>
																	<th class="min-w-125px">Date</th>
																	<th class="w-100px">Invoice</th>
																</tr>
															</thead>
															<!--end::Thead-->
															<!--begin::Tbody-->
															<tbody class="fs-6 fw-bold text-gray-600">
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">102445788</a>
																	</td>
																	<td class="text-success">$38.00</td>
																	<td>
																		<span class="badge badge-light-success">Approved</span>
																	</td>
																	<td>Nov 01, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">423445721</a>
																	</td>
																	<td class="text-danger">$-2.60</td>
																	<td>
																		<span class="badge badge-light-danger">Rejected</span>
																	</td>
																	<td>Oct 24, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">102445788</a>
																	</td>
																	<td class="text-success">$38.00</td>
																	<td>
																		<span class="badge badge-light-info">In progress</span>
																	</td>
																	<td>Nov 01, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">423445721</a>
																	</td>
																	<td class="text-danger">$-2.60</td>
																	<td>
																		<span class="badge badge-light-info">In progress</span>
																	</td>
																	<td>Oct 24, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
																<tr>
																	<td>
																		<a href="#" class="text-gray-600 text-hover-primary">426445943</a>
																	</td>
																	<td class="text-success">$31.00</td>
																	<td>
																		<span class="badge badge-light-success">Approved</span>
																	</td>
																	<td>Feb 09, 2020</td>
																	<td class="">
																		<button class="btn btn-sm btn-light btn-active-light-primary">Download</button>
																	</td>
																</tr>
															</tbody>
															<!--end::Tbody-->
														</table>
														<!--end::Table-->
													</div>
													<!--end::Table wrapper-->
												</div>
												<!--end::Tab panel-->
											</div>
											<!--end::Tab Content-->
										</div>
										<!--end::Card body-->
									</div>
									<!--end::Card-->
								</div>
								<!--end::Content-->
								<!--begin::Sidebar-->
								<div class="flex-column flex-lg-row-auto w-lg-250px w-xl-300px mb-10 order-1 order-lg-2">
									<!--begin::Card-->
									<div class="card card-flush mb-0" data-kt-sticky="true" data-kt-sticky-name="subscription-summary" data-kt-sticky-offset="{default: false, lg: '200px'}" data-kt-sticky-width="{lg: '250px', xl: '300px'}" data-kt-sticky-left="auto" data-kt-sticky-top="150px" data-kt-sticky-animation="false" data-kt-sticky-zindex="95">
										<!--begin::Card header-->
										<div class="card-header">
											<!--begin::Card title-->
											<div class="card-title">
												<h2>Summary</h2>
											</div>
											<!--end::Card title-->
											<!--begin::Card toolbar-->
											<div class="card-toolbar">
												<!--begin::More options-->
												<a href="#" class="btn btn-sm btn-light btn-icon" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
													<!--begin::Svg Icon | path: icons/duotune/general/gen052.svg-->
													<span class="svg-icon svg-icon-3">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect x="10" y="10" width="4" height="4" rx="2" fill="black" />
															<rect x="17" y="10" width="4" height="4" rx="2" fill="black" />
															<rect x="3" y="10" width="4" height="4" rx="2" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon-->
												</a>
												<!--begin::Menu-->
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-6 w-200px py-4" data-kt-menu="true">
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3">Pause Subscription</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3" data-kt-subscriptions-view-action="delete">Edit Subscription</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link text-danger px-3" data-kt-subscriptions-view-action="edit">Cancel Subscription</a>
													</div>
													<!--end::Menu item-->
												</div>
												<!--end::Menu-->
												<!--end::More options-->
											</div>
											<!--end::Card toolbar-->
										</div>
										<!--end::Card header-->
										<!--begin::Card body-->
										<div class="card-body pt-0 fs-6">
											<!--begin::Section-->
											<div class="mb-7">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Avatar-->
													<div class="symbol symbol-60px symbol-circle me-3">
														<img alt="Pic" src="assets/media/avatars/150-4.jpg" />
													</div>
													<!--end::Avatar-->
													<!--begin::Info-->
													<div class="d-flex flex-column">
														<!--begin::Name-->
														<a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-2">Sean Bean</a>
														<!--end::Name-->
														<!--begin::Email-->
														<a href="#" class="fw-bold text-gray-600 text-hover-primary">sean@dellito.com</a>
														<!--end::Email-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Details-->
											</div>
											<!--end::Section-->
											<!--begin::Seperator-->
											<div class="separator separator-dashed mb-7"></div>
											<!--end::Seperator-->
											<!--begin::Section-->
											<div class="mb-7">
												<!--begin::Title-->
												<h5 class="mb-4">Product details</h5>
												<!--end::Title-->
												<!--begin::Details-->
												<div class="mb-0">
													<!--begin::Plan-->
													<span class="badge badge-light-info me-2">Basic Bundle</span>
													<!--end::Plan-->
													<!--begin::Price-->
													<span class="fw-bold text-gray-600">$149.99 / Year</span>
													<!--end::Price-->
												</div>
												<!--end::Details-->
											</div>
											<!--end::Section-->
											<!--begin::Seperator-->
											<div class="separator separator-dashed mb-7"></div>
											<!--end::Seperator-->
											<!--begin::Section-->
											<div class="mb-10">
												<!--begin::Title-->
												<h5 class="mb-4">Payment Details</h5>
												<!--end::Title-->
												<!--begin::Details-->
												<div class="mb-0">
													<!--begin::Card info-->
													<div class="fw-bold text-gray-600 d-flex align-items-center">Mastercard
													<img src="assets/media/svg/card-logos/mastercard.svg" class="w-35px ms-2" alt="" /></div>
													<!--end::Card info-->
													<!--begin::Card expiry-->
													<div class="fw-bold text-gray-600">Expires Dec 2024</div>
													<!--end::Card expiry-->
												</div>
												<!--end::Details-->
											</div>
											<!--end::Section-->
											<!--begin::Seperator-->
											<div class="separator separator-dashed mb-7"></div>
											<!--end::Seperator-->
											<!--begin::Section-->
											<div class="mb-10">
												<!--begin::Title-->
												<h5 class="mb-4">Subscription Details</h5>
												<!--end::Title-->
												<!--begin::Details-->
												<table class="table fs-6 fw-bold gs-0 gy-2 gx-2">
													<!--begin::Row-->
													<tr class="">
														<td class="text-gray-400">Subscription ID:</td>
														<td class="text-gray-800">sub_4567_8765</td>
													</tr>
													<!--end::Row-->
													<!--begin::Row-->
													<tr class="">
														<td class="text-gray-400">Started:</td>
														<td class="text-gray-800">15 Apr 2021</td>
													</tr>
													<!--end::Row-->
													<!--begin::Row-->
													<tr class="">
														<td class="text-gray-400">Status:</td>
														<td>
															<span class="badge badge-light-success">Active</span>
														</td>
													</tr>
													<!--end::Row-->
													<!--begin::Row-->
													<tr class="">
														<td class="text-gray-400">Next Invoice:</td>
														<td class="text-gray-800">15 Apr 2022</td>
													</tr>
													<!--end::Row-->
												</table>
												<!--end::Details-->
											</div>
											<!--end::Section-->
											<!--begin::Actions-->
											<div class="mb-0">
												<a href="../../demo6/dist/apps/subscriptions/add.html" class="btn btn-primary" id="kt_subscriptions_create_button">Edit Subscription</a>
											</div>
											<!--end::Actions-->
										</div>
										<!--end::Card body-->
									</div>
									<!--end::Card-->
								</div>
								<!--end::Sidebar-->
							</div>
							<!--end::Layout-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted fw-bold me-1">2021©</span>
								<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Menu-->
							<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
								<li class="menu-item">
									<a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
								</li>
								<li class="menu-item">
									<a href="https://keenthemes.com/support" target="_blank" class="menu-link px-2">Support</a>
								</li>
								<li class="menu-item">
									<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
								</li>
							</ul>
							<!--end::Menu-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		{{ theme()->getView('pages/header_right') }}
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<!--end::Main-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/widgets.js"></script>
		<script src="assets/js/custom/apps/chat/chat.js"></script>
		<script src="assets/js/custom/modals/create-app.js"></script>
		<script src="assets/js/custom/modals/upgrade-plan.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>
