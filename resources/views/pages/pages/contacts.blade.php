<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
	<base href="../../">
	<title>GoCC</title>
	<meta charset="utf-8" />
	<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
	<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
	<meta property="og:url" content="https://keenthemes.com/metronic" />
	<meta property="og:site_name" content="Keenthemes | Metronic" />
	<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<!--end::Fonts-->
	<!--begin::Page Vendor Stylesheets(used by this page)-->
	<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Page Vendor Stylesheets-->
	<!--begin::Global Stylesheets Bundle(used by all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed">
	<!--begin::Main-->
	<!--begin::Root-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="page d-flex flex-row flex-column-fluid">
			<!--begin::Aside-->
			{{ theme()->getView('pages/aside') }}
			<!--end::Aside-->
			<!--begin::Wrapper-->
			<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
				<!--begin::Header-->
				{{ theme()->getView('pages/header') }}
				<!--end::Header-->
				<!--begin::Toolbar-->
				<div class="toolbar py-3" id="kt_toolbar">
					<!--begin::Container-->
					<div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
						<!--begin::Page title-->
						<div class="flex-grow-1 flex-shrink-0 me-5">
							<!--begin::Page title-->
							<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
								<!--begin::Title-->
								<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Contacts List</h1>
								<!--end::Title-->
								<!--begin::Separator-->
								<span class="h-20px border-gray-200 border-start mx-3"></span>
								<!--end::Separator-->
								<!--begin::Breadcrumb-->
								<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
									<!--begin::Item-->
									<li class="breadcrumb-item text-muted">
										<a href="" class="text-muted text-hover-primary">Home</a>
									</li>
									<!--end::Item-->
									<!--begin::Item-->
									<li class="breadcrumb-item">
										<span class="bullet bg-gray-200 w-5px h-2px"></span>
									</li>
									<!--end::Item-->
									<!--begin::Item-->
									<li class="breadcrumb-item text-muted">Contacts</li>
									<!--end::Item-->
									<!--begin::Item-->
									<li class="breadcrumb-item text-dark">Listing</li>
									<!--end::Item-->
								</ul>
								<!--end::Breadcrumb-->
							</div>
							<!--end::Page title-->
						</div>
						<!--end::Page title-->
						<!--begin::Action group-->
						<div class="d-flex align-items-center flex-wrap">
							<!--begin::Wrapper-->
							<div class="d-flex align-items-center me-2">
								<a href="public/contacts/add" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"><i class="bi bi-person-plus-fill"></i> Add contact</a>
							</div>
							<div class="d-flex align-items-center me-2">
								<a href="public/contacts/import" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"><i class="bi bi-file-plus-fill"></i> Import contacts</a>
							</div>
							<div class="d-flex align-items-center me-2">
								<a href="#" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder" data-bs-toggle="modal" data-bs-target="#kt_modal_view_users"><i class="bi bi-collection-fill"></i> Groups</a>
							</div>
							<div class="d-flex align-items-center">
								<a href="public/contacts/search" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"><i class="bi bi-search"></i> Advanced Search</a>
							</div>
							<!--end::Wrapper-->
						</div>
						<!--end::Action group-->
					</div>
					<!--end::Container-->
				</div>
				<!--end::Toolbar-->
				<!--begin::Content-->
				<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
					<!--begin::Container-->
					<div id="kt_content_container" class="container-fluid">
						<!--begin::Card-->
						<div class="card">
							<!--begin::Card header-->
							<div class="card-header border-0 pt-6">
								<!--begin::Card title-->
								<div class="card-title">
									<!--begin::Search-->
									<div class="d-flex align-items-center position-relative my-1">
										<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
										<span class="svg-icon svg-icon-1 position-absolute ms-6">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
												<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
												<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
											</svg>
										</span>
										<!--end::Svg Icon-->
										<input type="text" data-kt-customer-table-filter="search" class="form-control form-control-solid w-250px ps-15" placeholder="Search Contacts" />
									</div>
									<!--end::Search-->
								</div>
								<!--begin::Card title-->
								<!--begin::Card toolbar-->
								<div class="card-toolbar">
									<!--begin::Toolbar-->
									<div class="d-flex justify-content-end" data-kt-customer-table-toolbar="base">
										<!--begin::Filter-->
										<button type="button" class="btn btn-light-primary me-3" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
											<!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
											<span class="svg-icon svg-icon-2">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->Filter</button>
										<!--begin::Menu 1-->
										<div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true" id="kt-toolbar-filter">
											<!--begin::Header-->
											<div class="px-7 py-5">
												<div class="fs-4 text-dark fw-bolder">Filter Options</div>
											</div>
											<!--end::Header-->
											<!--begin::Separator-->
											<div class="separator border-gray-200"></div>
											<!--end::Separator-->
											<!--begin::Content-->
											<div class="px-7 py-5">
												<!--begin::Input group-->
												<div class="mb-10">
													<!--begin::Label-->
													<label class="form-label fs-5 fw-bold mb-3">Month:</label>
													<!--end::Label-->
													<!--begin::Input-->
													<select class="form-select form-select-solid fw-bolder" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" data-kt-customer-table-filter="month" data-dropdown-parent="#kt-toolbar-filter">
														<option></option>
														<option value="aug">August</option>
														<option value="sep">September</option>
														<option value="oct">October</option>
														<option value="nov">November</option>
														<option value="dec">December</option>
													</select>
													<!--end::Input-->
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												<div class="mb-10">
													<!--begin::Label-->
													<label class="form-label fs-5 fw-bold mb-3">Payment Type:</label>
													<!--end::Label-->
													<!--begin::Options-->
													<div class="d-flex flex-column flex-wrap fw-bold" data-kt-customer-table-filter="payment_type">
														<!--begin::Option-->
														<label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
															<input class="form-check-input" type="radio" name="payment_type" value="all" checked="checked" />
															<span class="form-check-label text-gray-600">All</span>
														</label>
														<!--end::Option-->
														<!--begin::Option-->
														<label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
															<input class="form-check-input" type="radio" name="payment_type" value="visa" />
															<span class="form-check-label text-gray-600">Visa</span>
														</label>
														<!--end::Option-->
														<!--begin::Option-->
														<label class="form-check form-check-sm form-check-custom form-check-solid mb-3">
															<input class="form-check-input" type="radio" name="payment_type" value="mastercard" />
															<span class="form-check-label text-gray-600">Mastercard</span>
														</label>
														<!--end::Option-->
														<!--begin::Option-->
														<label class="form-check form-check-sm form-check-custom form-check-solid">
															<input class="form-check-input" type="radio" name="payment_type" value="american_express" />
															<span class="form-check-label text-gray-600">American Express</span>
														</label>
														<!--end::Option-->
													</div>
													<!--end::Options-->
												</div>
												<!--end::Input group-->
												<!--begin::Actions-->
												<div class="d-flex justify-content-end">
													<button type="reset" class="btn btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true" data-kt-customer-table-filter="reset">Reset</button>
													<button type="submit" class="btn btn-primary" data-kt-menu-dismiss="true" data-kt-customer-table-filter="filter">Apply</button>
												</div>
												<!--end::Actions-->
											</div>
											<!--end::Content-->
										</div>
										<!--end::Menu 1-->
										<!--end::Filter-->
										<!--begin::Export-->
										<button type="button" class="btn btn-light-primary" data-bs-toggle="modal" data-bs-target="#kt_customers_export_modal">
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr078.svg-->
											<span class="svg-icon svg-icon-2">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.3" x="12.75" y="4.25" width="12" height="2" rx="1" transform="rotate(90 12.75 4.25)" fill="black" />
													<path d="M12.0573 6.11875L13.5203 7.87435C13.9121 8.34457 14.6232 8.37683 15.056 7.94401C15.4457 7.5543 15.4641 6.92836 15.0979 6.51643L12.4974 3.59084C12.0996 3.14332 11.4004 3.14332 11.0026 3.59084L8.40206 6.51643C8.0359 6.92836 8.0543 7.5543 8.44401 7.94401C8.87683 8.37683 9.58785 8.34458 9.9797 7.87435L11.4427 6.11875C11.6026 5.92684 11.8974 5.92684 12.0573 6.11875Z" fill="black" />
													<path d="M18.75 8.25H17.75C17.1977 8.25 16.75 8.69772 16.75 9.25C16.75 9.80228 17.1977 10.25 17.75 10.25C18.3023 10.25 18.75 10.6977 18.75 11.25V18.25C18.75 18.8023 18.3023 19.25 17.75 19.25H5.75C5.19772 19.25 4.75 18.8023 4.75 18.25V11.25C4.75 10.6977 5.19771 10.25 5.75 10.25C6.30229 10.25 6.75 9.80228 6.75 9.25C6.75 8.69772 6.30229 8.25 5.75 8.25H4.75C3.64543 8.25 2.75 9.14543 2.75 10.25V19.25C2.75 20.3546 3.64543 21.25 4.75 21.25H18.75C19.8546 21.25 20.75 20.3546 20.75 19.25V10.25C20.75 9.14543 19.8546 8.25 18.75 8.25Z" fill="#C4C4C4" />
												</svg>
											</span>
											<!--end::Svg Icon-->Export</button>
										<!--end::Export-->
									</div>
									<!--end::Toolbar-->
									<!--begin::Group actions-->
									<div class="d-flex justify-content-end align-items-center d-none" data-kt-customer-table-toolbar="selected">
										<div class="fw-bolder me-5">
											<span class="me-2" data-kt-customer-table-select="selected_count"></span>Selected</div>
										<button type="button" class="btn btn-danger" data-kt-customer-table-select="delete_selected">Delete Selected</button>
									</div>
									<!--end::Group actions-->
								</div>
								<!--end::Card toolbar-->
							</div>
							<!--end::Card header-->
							<!--begin::Card body-->
							<div class="card-body pt-0">
								<!--begin::Table-->
								<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
									<!--begin::Table head-->
									<thead>
										<!--begin::Table row-->
										<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
											<th class="w-10px pe-2">
												<div class="form-check form-check-sm form-check-custom form-check-solid me-3">
													<input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_customers_table .form-check-input" value="1" />
												</div>
											</th>
											<th class="min-w-125px">ID</th>
											<th class="min-w-125px">Last Name</th>
											<th class="min-w-125px">First Name</th>
											<th class="min-w-125px">Middle Name</th>
											<th class="min-w-125px">Gender</th>
											<th class="min-w-125px">Country</th>
											<th class="min-w-125px">Languages</th>
											<th class="min-w-125px">Birth date</th>
											<th class="min-w-125px">Creation Date</th>
											<th class="min-w-125px">Creation Type</th>
											<th class="min-w-125px">Creator</th>
											<th class="min-w-125px">Creator Details</th>
											<th class="min-w-125px">Detailed Status</th>
											<th class="min-w-125px">Phone</th>
											<th class="min-w-125px">Phone Type</th>
											<th class="min-w-125px">Email</th>
											<th class="min-w-125px">Email Type</th>
											<th class="min-w-125px">Fax</th>
											<th class="min-w-125px">Verification Status</th>
											<th class="min-w-125px">Verification type</th>
											<th class="min-w-125px">Verification details</th>
											<th class="min-w-125px">Verification date</th>
											<th class="min-w-125px">Phone Verifying Flag</th>
											<th class="min-w-125px">Email Verifying Flag</th>
											<th class="min-w-125px">Fax Verifying Flag</th>
											<th class="min-w-125px">DO NOT DEAL</th>
											<th class="min-w-125px">DO NOT DEAL Date</th>
											<th class="min-w-125px">DO NOT DEAL Declarant</th>
											<th class="min-w-125px">DO NOT DEAL Reason</th>
											<th class="min-w-125px">DNCall</th>
											<th class="min-w-125px">DNCall Date</th>
											<th class="min-w-125px">DNCall Declarant</th>
											<th class="min-w-125px">DNCall Reason</th>
											<th class="min-w-125px">DNChat</th>
											<th class="min-w-125px">DNChat Date</th>
											<th class="min-w-125px">DNChat Declarant</th>
											<th class="min-w-125px">DNChat Reason</th>
											<th class="min-w-125px">DNText</th>
											<th class="min-w-125px">DNText Date</th>
											<th class="min-w-125px">DNText Declarant</th>
											<th class="min-w-125px">DNText Reason</th>
											<th class="min-w-125px">DNEmail</th>
											<th class="min-w-125px">DNEmail Date</th>
											<th class="min-w-125px">DNEmail Declarant</th>
											<th class="min-w-125px">DNEmail Reason</th>
											<th class="min-w-125px">Personnal Group</th>
											<th class="text-end min-w-70px">Actions</th>
										</tr>
										<!--end::Table row-->
									</thead>
									<!--end::Table head-->
									<!--begin::Table body-->
									<tbody class="fw-bold text-gray-600">
										<tr>
											<!--begin::Checkbox-->
											<td>
												<div class="form-check form-check-sm form-check-custom form-check-solid">
													<input class="form-check-input" type="checkbox" value="1" />
												</div>
											</td>
											<!--end::Checkbox-->
											<td>45</td>
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Emma</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Smith</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1"> </a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Emma Smith</a>
											</td>
											<!--end::Name=-->
											<!--begin::Email=-->
											<td>
												<a href="#" class="text-gray-600 text-hover-primary mb-1">e.smith@kpmg.com.au</a>
											</td>
											<!--end::Email=-->
											<!--begin::Company=-->
											<td>-</td>
											<!--end::Company=-->
											<!--begin::Payment method=-->
											<td data-filter="mastercard">
												<img src="assets/media/svg/card-logos/mastercard.svg" class="w-35px me-3" alt="" />**** 9883</td>
											<!--end::Payment method=-->
											<!--begin::Date=-->
											<td>14 Dec 2020</td>
											<!--end::Date=-->
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>

											<!--begin::Action=-->
											<td class="text-end">
												<a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
													<span class="svg-icon svg-icon-5 m-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon--></a>
												<!--begin::Menu-->
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">View</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">Edit</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add task</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add appointment</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Share</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">CALL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">SMS</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">EMAIL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">FAX</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3" data-kt-customer-table-filter="delete_row">Delete</a>
													</div>
													<!--end::Menu item-->
												</div>
												<!--end::Menu-->
											</td>
											<!--end::Action=-->
										</tr>
										<tr>
											<!--begin::Checkbox-->
											<td>
												<div class="form-check form-check-sm form-check-custom form-check-solid">
													<input class="form-check-input" type="checkbox" value="1" />
												</div>
											</td>
											<!--end::Checkbox-->
											<td>1</td>
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Melody</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Macy</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1"> </a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Melody Macy</a>
											</td>
											<!--end::Name=-->
											<!--begin::Email=-->
											<td>
												<a href="#" class="text-gray-600 text-hover-primary mb-1">melody@altbox.com</a>
											</td>
											<!--end::Email=-->
											<!--begin::Company=-->
											<td>Google</td>
											<!--end::Company=-->
											<!--begin::Payment method=-->
											<td data-filter="visa">
												<img src="assets/media/svg/card-logos/visa.svg" class="w-35px me-3" alt="" />**** 2680</td>
											<!--end::Payment method=-->
											<!--begin::Date=-->
											<td>01 Dec 2020</td>
											<!--end::Date=-->
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<!--begin::Action=-->
											<td class="text-end">
												<a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
													<span class="svg-icon svg-icon-5 m-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon--></a>
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">View</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">Edit</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add task</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add appointment</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Share</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">CALL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">SMS</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">EMAIL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">FAX</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3" data-kt-customer-table-filter="delete_row">Delete</a>
													</div>
													<!--end::Menu item-->
												</div>
											</td>
											<!--end::Action=-->
										</tr>
										<tr>
											<!--begin::Checkbox-->
											<td>
												<div class="form-check form-check-sm form-check-custom form-check-solid">
													<input class="form-check-input" type="checkbox" value="1" />
												</div>
											</td>
											<td>2</td>
											<!--end::Checkbox-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Max</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Smith</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1"></a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Max Smith</a>
											</td>
											<!--end::Name=-->
											<!--begin::Email=-->
											<td>
												<a href="#" class="text-gray-600 text-hover-primary mb-1">max@kt.com</a>
											</td>
											<!--end::Email=-->
											<!--begin::Company=-->
											<td>Bistro Union</td>
											<!--end::Company=-->
											<!--begin::Payment method=-->
											<td data-filter="mastercard">
												<img src="assets/media/svg/card-logos/mastercard.svg" class="w-35px me-3" alt="" />**** 3168</td>
											<!--end::Payment method=-->
											<!--begin::Date=-->
											<td>12 Nov 2020</td>
											<!--end::Date=-->
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<!--begin::Action=-->
											<td class="text-end">
												<a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
													<span class="svg-icon svg-icon-5 m-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon--></a>
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">View</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">Edit</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add task</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add appointment</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Share</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">CALL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">SMS</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">EMAIL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">FAX</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3" data-kt-customer-table-filter="delete_row">Delete</a>
													</div>
													<!--end::Menu item-->
												</div>
											</td>
											<!--end::Action=-->
										</tr>
										<tr>
											<!--begin::Checkbox-->
											<td>
												<div class="form-check form-check-sm form-check-custom form-check-solid">
													<input class="form-check-input" type="checkbox" value="1" />
												</div>
											</td>
											<!--end::Checkbox-->
											<td>3</td>
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Sean</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Bean</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1"> </a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Sean Bean</a>
											</td>
											<!--end::Name=-->
											<!--begin::Email=-->
											<td>
												<a href="#" class="text-gray-600 text-hover-primary mb-1">sean@dellito.com</a>
											</td>
											<!--end::Email=-->
											<!--begin::Company=-->
											<td>Astro Limited</td>
											<!--end::Company=-->
											<!--begin::Payment method=-->
											<td data-filter="american_express">
												<img src="assets/media/svg/card-logos/american-express.svg" class="w-35px me-3" alt="" />**** 5330</td>
											<!--end::Payment method=-->
											<!--begin::Date=-->
											<td>21 Oct 2020</td>
											<!--end::Date=-->
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<!--begin::Action=-->
											<td class="text-end">
												<a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
													<span class="svg-icon svg-icon-5 m-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon--></a>
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">View</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">Edit</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add task</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add appointment</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Share</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">CALL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">SMS</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">EMAIL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">FAX</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3" data-kt-customer-table-filter="delete_row">Delete</a>
													</div>
													<!--end::Menu item-->
												</div>
											</td>
											<!--end::Action=-->
										</tr>
										<tr>
											<!--begin::Checkbox-->
											<td>
												<div class="form-check form-check-sm form-check-custom form-check-solid">
													<input class="form-check-input" type="checkbox" value="1" />
												</div>
											</td>
											<!--end::Checkbox-->
											<td>4</td>
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Brian</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Cox</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1"> </a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Brian Cox</a>
											</td>
											<!--end::Name=-->
											<!--begin::Email=-->
											<td>
												<a href="#" class="text-gray-600 text-hover-primary mb-1">brian@exchange.com</a>
											</td>
											<!--end::Email=-->
											<!--begin::Company=-->
											<td>-</td>
											<!--end::Company=-->
											<!--begin::Payment method=-->
											<td data-filter="visa">
												<img src="assets/media/svg/card-logos/visa.svg" class="w-35px me-3" alt="" />**** 2159</td>
											<!--end::Payment method=-->
											<!--begin::Date=-->
											<td>19 Oct 2020</td>
											<!--end::Date=-->
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<!--begin::Action=-->
											<td class="text-end">
												<a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
													<span class="svg-icon svg-icon-5 m-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon--></a>
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">View</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">Edit</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add task</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add appointment</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Share</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">CALL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">SMS</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">EMAIL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">FAX</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3" data-kt-customer-table-filter="delete_row">Delete</a>
													</div>
													<!--end::Menu item-->
												</div>
											</td>
											<!--end::Action=-->
										</tr>
										<tr>
											<!--begin::Checkbox-->
											<td>
												<div class="form-check form-check-sm form-check-custom form-check-solid">
													<input class="form-check-input" type="checkbox" value="1" />
												</div>
											</td>
											<!--end::Checkbox-->
											<td>5</td>
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Mikaela</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Collins</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1"> </a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Mikaela Collins</a>
											</td>
											<!--end::Name=-->
											<!--begin::Email=-->
											<td>
												<a href="#" class="text-gray-600 text-hover-primary mb-1">mikaela@pexcom.com</a>
											</td>
											<!--end::Email=-->
											<!--begin::Company=-->
											<td>Keenthemes</td>
											<!--end::Company=-->
											<!--begin::Payment method=-->
											<td data-filter="american_express">
												<img src="assets/media/svg/card-logos/american-express.svg" class="w-35px me-3" alt="" />**** 3066</td>
											<!--end::Payment method=-->
											<!--begin::Date=-->
											<td>23 Sep 2020</td>
											<!--end::Date=-->
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<!--begin::Action=-->
											<td class="text-end">
												<a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
													<span class="svg-icon svg-icon-5 m-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon--></a>
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">View</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">Edit</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add task</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add appointment</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Share</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">CALL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">SMS</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">EMAIL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">FAX</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3" data-kt-customer-table-filter="delete_row">Delete</a>
													</div>
													<!--end::Menu item-->
												</div>
											</td>
											<!--end::Action=-->
										</tr>
										<tr>
											<!--begin::Checkbox-->
											<td>
												<div class="form-check form-check-sm form-check-custom form-check-solid">
													<input class="form-check-input" type="checkbox" value="1" />
												</div>
											</td>
											<!--end::Checkbox-->
											<td>8</td>
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Francis</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Mitcham</a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1"> </a>
											</td>
											<!--end::Name=-->
											<!--begin::Name=-->
											<td>
												<a href="public/contacts/details" class="text-gray-800 text-hover-primary mb-1">Francis Mitcham</a>
											</td>
											<!--end::Name=-->
											<!--begin::Email=-->
											<td>
												<a href="#" class="text-gray-600 text-hover-primary mb-1">f.mitcham@kpmg.com.au</a>
											</td>
											<!--end::Email=-->
											<!--begin::Company=-->
											<td>Paypal</td>
											<!--end::Company=-->
											<!--begin::Payment method=-->
											<td data-filter="mastercard">
												<img src="assets/media/svg/card-logos/mastercard.svg" class="w-35px me-3" alt="" />**** 4335</td>
											<!--end::Payment method=-->
											<!--begin::Date=-->
											<td>11 Sep 2020</td>
											<!--end::Date=-->
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<td>--</td>
											<!--begin::Action=-->
											<td class="text-end">
												<a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
													<span class="svg-icon svg-icon-5 m-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon--></a>
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">View</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="public/contacts/details" class="menu-link px-3">Edit</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add task</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Add appointment</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">Share</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">CALL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">SMS</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">EMAIL</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="" class="menu-link px-3">FAX</a>
													</div>
													<!--end::Menu item-->
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<a href="#" class="menu-link px-3" data-kt-customer-table-filter="delete_row">Delete</a>
													</div>
													<!--end::Menu item-->
												</div>
											</td>
											<!--end::Action=-->
										</tr>
									</tbody>
									<!--end::Table body-->
								</table>
								<!--end::Table-->
							</div>
							<!--end::Card body-->
						</div>
						<!--end::Card-->
						<!--begin::Modals-->
						<!--begin::Modal - Customers - Add-->
						<div class="modal fade" id="kt_modal_add_customer" tabindex="-1" aria-hidden="true">
							<!--begin::Modal dialog-->
							<div class="modal-dialog modal-dialog-centered mw-950px">
								<!--begin::Modal content-->
								<div class="modal-content">
									<!--begin::Form-->
									<form class="form" action="#" id="kt_modal_add_customer_form" data-kt-redirect="../../demo6/dist/apps/customers/list.html">
										<!--begin::Modal header-->
										<div class="modal-header" id="kt_modal_add_customer_header">
											<!--begin::Modal title-->
											<h2 class="fw-bolder">Add a Contact</h2>
											<!--end::Modal title-->
											<!--begin::Close-->
											<div id="kt_modal_add_customer_close" class="btn btn-icon btn-sm btn-active-icon-primary">
												<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
												<span class="svg-icon svg-icon-1">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
														<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
											</div>
											<!--end::Close-->
										</div>
										<!--end::Modal header-->
										<!--begin::Modal body-->
										<div class="modal-body py-10 px-lg-17">
											<!--begin::Scroll-->
											<div class="scroll-y me-n7 pe-7" id="kt_modal_add_customer_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_customer_header" data-kt-scroll-wrappers="#kt_modal_add_customer_scroll" data-kt-scroll-offset="300px">
												<!--begin::Input group-->
												<div class="fv-row mb-7 row" data-kt-buttons="true" data-kt-buttons-target="[data-kt-button]">
													<!--begin::Label-->
													<label class="required fs-6 fw-bold mb-2">Gender</label>
													<!--end::Label-->
													<div>
														<label class="btn btn-outline-secondary text-muted text-hover-white text-active-white btn-outline btn-active-success" data-kt-button="true">
															<!--begin::Input-->
															<input class="btn-check" type="radio" name="gender" value="1" data-com.bitwarden.browser.user-edited="yes">
															<!--end::Input-->
															Male
														</label>
														<!--end::Radio-->
														<!--begin::Radio-->
														<label class="btn btn-outline-secondary text-muted text-hover-white text-active-white btn-outline btn-active-success active" data-kt-button="true">
															<!--begin::Input-->
															<input class="btn-check" type="radio" name="gender" checked="checked" value="2" data-com.bitwarden.browser.user-edited="yes">
															<!--end::Input-->
															Female
														</label>
														<!--end::Radio-->
													</div>
												</div>
												<!--begin::Input group-->

												<!--begin::Input group-->
												<div class="fv-row mb-7 row">
													<div class="col-4">
														<!--begin::Label-->
														<label class="required fs-6 fw-bold mb-2">First Name</label>
														<!--end::Label-->
														<!--begin::Input-->
														<input type="text" class="form-control form-control-solid" placeholder="" name="firstname" value="Sean Bean" />
														<!--end::Input-->
													</div>
													<div class="col-4">
														<!--begin::Label-->
														<label class="required fs-6 fw-bold mb-2">Middle Name</label>
														<!--end::Label-->
														<!--begin::Input-->
														<input type="text" class="form-control form-control-solid" placeholder="" name="middlename" value="Sean Bean" />
														<!--end::Input-->
													</div>
													<div class="col-4">
														<!--begin::Label-->
														<label class="required fs-6 fw-bold mb-2">Last Name</label>
														<!--end::Label-->
														<!--begin::Input-->
														<input type="text" class="form-control form-control-solid" placeholder="" name="lastname" value="Sean Bean" />
														<!--end::Input-->
													</div>
												</div>
												<!--end::Input group-->

												<!--begin::Input group-->
												<div class="fv-row mb-7 row">
													<div class="col-6">
														<!--begin::Label-->
														<label class="fs-6 fw-bold mb-2">
															<span class="required">Country</span>
															<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
														</label>
														<!--end::Label-->
														<!--begin::Input-->
														<select name="country" aria-label="Select a Country" data-control="select2" data-placeholder="Select a Country..." data-dropdown-parent="#kt_modal_add_customer" class="form-select form-select-solid fw-bolder">
															<option value="">Select a Country...</option>
															<option value="AF">Afghanistan</option>
															<option value="AX">Aland Islands</option>
															<option value="AL">Albania</option>
															<option value="DZ">Algeria</option>
															<option value="AS">American Samoa</option>
															<option value="AD">Andorra</option>
															<option value="AO">Angola</option>
															<option value="AI">Anguilla</option>
															<option value="AG">Antigua and Barbuda</option>
															<option value="AR">Argentina</option>
															<option value="AM">Armenia</option>
															<option value="AW">Aruba</option>
															<option value="AU">Australia</option>
															<option value="AT">Austria</option>
															<option value="AZ">Azerbaijan</option>
															<option value="BS">Bahamas</option>
															<option value="BH">Bahrain</option>
															<option value="BD">Bangladesh</option>
															<option value="BB">Barbados</option>
															<option value="BY">Belarus</option>
															<option value="BE">Belgium</option>
															<option value="BZ">Belize</option>
															<option value="BJ">Benin</option>
															<option value="BM">Bermuda</option>
															<option value="BT">Bhutan</option>
															<option value="BO">Bolivia, Plurinational State of</option>
															<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
															<option value="BA">Bosnia and Herzegovina</option>
															<option value="BW">Botswana</option>
															<option value="BR">Brazil</option>
															<option value="IO">British Indian Ocean Territory</option>
															<option value="BN">Brunei Darussalam</option>
															<option value="BG">Bulgaria</option>
															<option value="BF">Burkina Faso</option>
															<option value="BI">Burundi</option>
															<option value="KH">Cambodia</option>
															<option value="CM">Cameroon</option>
															<option value="CA">Canada</option>
															<option value="CV">Cape Verde</option>
															<option value="KY">Cayman Islands</option>
															<option value="CF">Central African Republic</option>
															<option value="TD">Chad</option>
															<option value="CL">Chile</option>
															<option value="CN">China</option>
															<option value="CX">Christmas Island</option>
															<option value="CC">Cocos (Keeling) Islands</option>
															<option value="CO">Colombia</option>
															<option value="KM">Comoros</option>
															<option value="CK">Cook Islands</option>
															<option value="CR">Costa Rica</option>
															<option value="CI">Côte d'Ivoire</option>
															<option value="HR">Croatia</option>
															<option value="CU">Cuba</option>
															<option value="CW">Curaçao</option>
															<option value="CZ">Czech Republic</option>
															<option value="DK">Denmark</option>
															<option value="DJ">Djibouti</option>
															<option value="DM">Dominica</option>
															<option value="DO">Dominican Republic</option>
															<option value="EC">Ecuador</option>
															<option value="EG">Egypt</option>
															<option value="SV">El Salvador</option>
															<option value="GQ">Equatorial Guinea</option>
															<option value="ER">Eritrea</option>
															<option value="EE">Estonia</option>
															<option value="ET">Ethiopia</option>
															<option value="FK">Falkland Islands (Malvinas)</option>
															<option value="FJ">Fiji</option>
															<option value="FI">Finland</option>
															<option value="FR">France</option>
															<option value="PF">French Polynesia</option>
															<option value="GA">Gabon</option>
															<option value="GM">Gambia</option>
															<option value="GE">Georgia</option>
															<option value="DE">Germany</option>
															<option value="GH">Ghana</option>
															<option value="GI">Gibraltar</option>
															<option value="GR">Greece</option>
															<option value="GL">Greenland</option>
															<option value="GD">Grenada</option>
															<option value="GU">Guam</option>
															<option value="GT">Guatemala</option>
															<option value="GG">Guernsey</option>
															<option value="GN">Guinea</option>
															<option value="GW">Guinea-Bissau</option>
															<option value="HT">Haiti</option>
															<option value="VA">Holy See (Vatican City State)</option>
															<option value="HN">Honduras</option>
															<option value="HK">Hong Kong</option>
															<option value="HU">Hungary</option>
															<option value="IS">Iceland</option>
															<option value="IN">India</option>
															<option value="ID">Indonesia</option>
															<option value="IR">Iran, Islamic Republic of</option>
															<option value="IQ">Iraq</option>
															<option value="IE">Ireland</option>
															<option value="IM">Isle of Man</option>
															<option value="IL">Israel</option>
															<option value="IT">Italy</option>
															<option value="JM">Jamaica</option>
															<option value="JP">Japan</option>
															<option value="JE">Jersey</option>
															<option value="JO">Jordan</option>
															<option value="KZ">Kazakhstan</option>
															<option value="KE">Kenya</option>
															<option value="KI">Kiribati</option>
															<option value="KP">Korea, Democratic People's Republic of</option>
															<option value="KW">Kuwait</option>
															<option value="KG">Kyrgyzstan</option>
															<option value="LA">Lao People's Democratic Republic</option>
															<option value="LV">Latvia</option>
															<option value="LB">Lebanon</option>
															<option value="LS">Lesotho</option>
															<option value="LR">Liberia</option>
															<option value="LY">Libya</option>
															<option value="LI">Liechtenstein</option>
															<option value="LT">Lithuania</option>
															<option value="LU">Luxembourg</option>
															<option value="MO">Macao</option>
															<option value="MG">Madagascar</option>
															<option value="MW">Malawi</option>
															<option value="MY">Malaysia</option>
															<option value="MV">Maldives</option>
															<option value="ML">Mali</option>
															<option value="MT">Malta</option>
															<option value="MH">Marshall Islands</option>
															<option value="MQ">Martinique</option>
															<option value="MR">Mauritania</option>
															<option value="MU">Mauritius</option>
															<option value="MX">Mexico</option>
															<option value="FM">Micronesia, Federated States of</option>
															<option value="MD">Moldova, Republic of</option>
															<option value="MC">Monaco</option>
															<option value="MN">Mongolia</option>
															<option value="ME">Montenegro</option>
															<option value="MS">Montserrat</option>
															<option value="MA">Morocco</option>
															<option value="MZ">Mozambique</option>
															<option value="MM">Myanmar</option>
															<option value="NA">Namibia</option>
															<option value="NR">Nauru</option>
															<option value="NP">Nepal</option>
															<option value="NL">Netherlands</option>
															<option value="NZ">New Zealand</option>
															<option value="NI">Nicaragua</option>
															<option value="NE">Niger</option>
															<option value="NG">Nigeria</option>
															<option value="NU">Niue</option>
															<option value="NF">Norfolk Island</option>
															<option value="MP">Northern Mariana Islands</option>
															<option value="NO">Norway</option>
															<option value="OM">Oman</option>
															<option value="PK">Pakistan</option>
															<option value="PW">Palau</option>
															<option value="PS">Palestinian Territory, Occupied</option>
															<option value="PA">Panama</option>
															<option value="PG">Papua New Guinea</option>
															<option value="PY">Paraguay</option>
															<option value="PE">Peru</option>
															<option value="PH">Philippines</option>
															<option value="PL">Poland</option>
															<option value="PT">Portugal</option>
															<option value="PR">Puerto Rico</option>
															<option value="QA">Qatar</option>
															<option value="RO">Romania</option>
															<option value="RU">Russian Federation</option>
															<option value="RW">Rwanda</option>
															<option value="BL">Saint Barthélemy</option>
															<option value="KN">Saint Kitts and Nevis</option>
															<option value="LC">Saint Lucia</option>
															<option value="MF">Saint Martin (French part)</option>
															<option value="VC">Saint Vincent and the Grenadines</option>
															<option value="WS">Samoa</option>
															<option value="SM">San Marino</option>
															<option value="ST">Sao Tome and Principe</option>
															<option value="SA">Saudi Arabia</option>
															<option value="SN">Senegal</option>
															<option value="RS">Serbia</option>
															<option value="SC">Seychelles</option>
															<option value="SL">Sierra Leone</option>
															<option value="SG">Singapore</option>
															<option value="SX">Sint Maarten (Dutch part)</option>
															<option value="SK">Slovakia</option>
															<option value="SI">Slovenia</option>
															<option value="SB">Solomon Islands</option>
															<option value="SO">Somalia</option>
															<option value="ZA">South Africa</option>
															<option value="KR">South Korea</option>
															<option value="SS">South Sudan</option>
															<option value="ES">Spain</option>
															<option value="LK">Sri Lanka</option>
															<option value="SD">Sudan</option>
															<option value="SR">Suriname</option>
															<option value="SZ">Swaziland</option>
															<option value="SE">Sweden</option>
															<option value="CH">Switzerland</option>
															<option value="SY">Syrian Arab Republic</option>
															<option value="TW">Taiwan, Province of China</option>
															<option value="TJ">Tajikistan</option>
															<option value="TZ">Tanzania, United Republic of</option>
															<option value="TH">Thailand</option>
															<option value="TG">Togo</option>
															<option value="TK">Tokelau</option>
															<option value="TO">Tonga</option>
															<option value="TT">Trinidad and Tobago</option>
															<option value="TN">Tunisia</option>
															<option value="TR">Turkey</option>
															<option value="TM">Turkmenistan</option>
															<option value="TC">Turks and Caicos Islands</option>
															<option value="TV">Tuvalu</option>
															<option value="UG">Uganda</option>
															<option value="UA">Ukraine</option>
															<option value="AE">United Arab Emirates</option>
															<option value="GB">United Kingdom</option>
															<option value="US" selected="selected">United States</option>
															<option value="UY">Uruguay</option>
															<option value="UZ">Uzbekistan</option>
															<option value="VU">Vanuatu</option>
															<option value="VE">Venezuela, Bolivarian Republic of</option>
															<option value="VN">Vietnam</option>
															<option value="VI">Virgin Islands</option>
															<option value="YE">Yemen</option>
															<option value="ZM">Zambia</option>
															<option value="ZW">Zimbabwe</option>
														</select>
														<!--end::Input-->
													</div>
													<div class="col-6">
														<!--begin::Label-->
														<label class="fs-6 fw-bold mb-2">
															<span class="required">Languages</span>
															<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
														</label>
														<!--end::Label-->
														<!--begin::Input-->
														<select name="languages" aria-label="Select a language" data-control="select2" data-placeholder="Select a language..." data-dropdown-parent="#kt_modal_add_customer" class="form-select form-select-solid fw-bolder">
															<option value="">Select a language...</option>
															<option value="FR">French</option>
															<option value="EN">English</option>
															<option value="AR">Arabe</option>
														</select>
														<!--end::Input-->
													</div>
													<!--end::Input group-->
												</div>
												<!--end::Input group-->

												<!--begin::Input group-->
												<div class="fv-row mb-7 row">
													<!--begin::Label-->
													<label class="fs-6 fw-bold mb-2 required">Birth date</label>
													<!--end::Label-->
													<!--begin::Input-->
													<input class="form-control form-control-solid" name="calendar_event_start_date" placeholder="Pick a start date" id="kt_calendar_datepicker_start_date" />
													<!--end::Input-->
												</div>
												<!--end::Input group-->

												<!--begin::Input group-->
												<div class="fv-row mb-7 row">
													<!--begin::Label-->
													<label class="fs-6 fw-bold mb-2">
														<span class="required">Personal Groups</span>
														<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
													</label>
													<!--end::Label-->
													<!--begin::Input-->
													<select name="languages" aria-label="Select a group" data-control="select2" data-placeholder="Select a group..." data-dropdown-parent="#kt_modal_add_customer" class="form-select form-select-solid fw-bolder">
														<option value="">Select a group...</option>
														<option value="cl1">Clients 1 </option>
														<option value="cl2">Clients 2</option>
														<option value="cl3">Clients 3</option>
													</select>
													<!--end::Input-->
												</div>
												<!--end::Input group-->

												<!--begin::Billing toggle-->
												<div class="fw-bolder fs-3 rotate collapsible mb-7" data-bs-toggle="collapse" href="#kt_modal_add_customer_billing_info" role="button" aria-expanded="false" aria-controls="kt_customer_view_details">Shipping Information
													<span class="ms-2 rotate-180">
														<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
														<span class="svg-icon svg-icon-3">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
															</svg>
														</span>
														<!--end::Svg Icon-->
													</span></div>
												<!--end::Billing toggle-->
												<!--begin::Billing form-->
												<div id="kt_modal_add_customer_billing_info" class="collapse show">
													<!--begin::Input group-->
													<div class="d-flex flex-column mb-7 fv-row">
														<!--begin::Label-->
														<label class="required fs-6 fw-bold mb-2">Address Line 1</label>
														<!--end::Label-->
														<!--begin::Input-->
														<input class="form-control form-control-solid" placeholder="" name="address1" value="101, Collins Street" />
														<!--end::Input-->
													</div>
													<!--end::Input group-->
													<!--begin::Input group-->
													<div class="d-flex flex-column mb-7 fv-row">
														<!--begin::Label-->
														<label class="fs-6 fw-bold mb-2">Address Line 2</label>
														<!--end::Label-->
														<!--begin::Input-->
														<input class="form-control form-control-solid" placeholder="" name="address2" value="" />
														<!--end::Input-->
													</div>
													<!--end::Input group-->
													<!--begin::Input group-->
													<div class="d-flex flex-column mb-7 fv-row">
														<!--begin::Label-->
														<label class="required fs-6 fw-bold mb-2">City</label>
														<!--end::Label-->
														<!--begin::Input-->
														<input class="form-control form-control-solid" placeholder="" name="city" value="Melbourne" />
														<!--end::Input-->
													</div>
													<!--end::Input group-->
													<!--begin::Input group-->
													<div class="row g-9 mb-7">
														<!--begin::Col-->
														<div class="col-md-6 fv-row">
															<!--begin::Label-->
															<label class="required fs-6 fw-bold mb-2">State / Province</label>
															<!--end::Label-->
															<!--begin::Input-->
															<input class="form-control form-control-solid" placeholder="" name="state" value="Victoria" />
															<!--end::Input-->
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-md-6 fv-row">
															<!--begin::Label-->
															<label class="required fs-6 fw-bold mb-2">Post Code</label>
															<!--end::Label-->
															<!--begin::Input-->
															<input class="form-control form-control-solid" placeholder="" name="postcode" value="3000" />
															<!--end::Input-->
														</div>
														<!--end::Col-->
													</div>
													<!--end::Input group-->
												</div>
												<!--end::Billing form-->
											</div>
											<!--end::Scroll-->
										</div>
										<!--end::Modal body-->
										<!--begin::Modal footer-->
										<div class="modal-footer flex-center">
											<!--begin::Button-->
											<button type="reset" id="kt_modal_add_customer_cancel" class="btn btn-light me-3">Discard</button>
											<!--end::Button-->
											<!--begin::Button-->
											<button type="submit" id="kt_modal_add_customer_submit" class="btn btn-primary">
												<span class="indicator-label">Submit</span>
												<span class="indicator-progress">Please wait...
													<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
											</button>
											<!--end::Button-->
										</div>
										<!--end::Modal footer-->
									</form>
									<!--end::Form-->
								</div>
							</div>
						</div>
						<!--end::Modal - Customers - Add-->
						<!--begin::Modal - Adjust Balance-->
						<div class="modal fade" id="kt_customers_export_modal" tabindex="-1" aria-hidden="true">
							<!--begin::Modal dialog-->
							<div class="modal-dialog modal-dialog-centered mw-650px">
								<!--begin::Modal content-->
								<div class="modal-content">
									<!--begin::Modal header-->
									<div class="modal-header">
										<!--begin::Modal title-->
										<h2 class="fw-bolder">Export Customers</h2>
										<!--end::Modal title-->
										<!--begin::Close-->
										<div id="kt_customers_export_close" class="btn btn-icon btn-sm btn-active-icon-primary">
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
											<span class="svg-icon svg-icon-1">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
													<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
										</div>
										<!--end::Close-->
									</div>
									<!--end::Modal header-->
									<!--begin::Modal body-->
									<div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
										<!--begin::Form-->
										<form id="kt_customers_export_form" class="form" action="#">
											<!--begin::Input group-->
											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="fs-5 fw-bold form-label mb-5">Select Date Range:</label>
												<!--end::Label-->
												<!--begin::Input-->
												<input class="form-control form-control-solid" placeholder="Pick a date" name="date" />
												<!--end::Input-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="fs-5 fw-bold form-label mb-5">Select Export Format:</label>
												<!--end::Label-->
												<!--begin::Input-->
												<select data-control="select2" data-placeholder="Select a format" data-hide-search="true" name="format" class="form-select form-select-solid">
													<option value="excell">Excel</option>
													<option value="pdf">PDF</option>
													<option value="cvs">CVS</option>
													<option value="zip">ZIP</option>
												</select>
												<!--end::Input-->
											</div>
											<!--end::Input group-->
											<!--begin::Row-->
											<div class="row fv-row mb-15">
												<!--begin::Label-->
												<label class="fs-5 fw-bold form-label mb-5">Payment Type:</label>
												<!--end::Label-->
												<!--begin::Radio group-->
												<div class="d-flex flex-column">
													<!--begin::Radio button-->
													<label class="form-check form-check-custom form-check-sm form-check-solid mb-3">
														<input class="form-check-input" type="checkbox" value="1" checked="checked" name="payment_type" />
														<span class="form-check-label text-gray-600 fw-bold">All</span>
													</label>
													<!--end::Radio button-->
													<!--begin::Radio button-->
													<label class="form-check form-check-custom form-check-sm form-check-solid mb-3">
														<input class="form-check-input" type="checkbox" value="2" checked="checked" name="payment_type" />
														<span class="form-check-label text-gray-600 fw-bold">Visa</span>
													</label>
													<!--end::Radio button-->
													<!--begin::Radio button-->
													<label class="form-check form-check-custom form-check-sm form-check-solid mb-3">
														<input class="form-check-input" type="checkbox" value="3" name="payment_type" />
														<span class="form-check-label text-gray-600 fw-bold">Mastercard</span>
													</label>
													<!--end::Radio button-->
													<!--begin::Radio button-->
													<label class="form-check form-check-custom form-check-sm form-check-solid">
														<input class="form-check-input" type="checkbox" value="4" name="payment_type" />
														<span class="form-check-label text-gray-600 fw-bold">American Express</span>
													</label>
													<!--end::Radio button-->
												</div>
												<!--end::Input group-->
											</div>
											<!--end::Row-->
											<!--begin::Actions-->
											<div class="text-center">
												<button type="reset" id="kt_customers_export_cancel" class="btn btn-light me-3">Discard</button>
												<button type="submit" id="kt_customers_export_submit" class="btn btn-primary">
													<span class="indicator-label">Submit</span>
													<span class="indicator-progress">Please wait...
														<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
												</button>
											</div>
											<!--end::Actions-->
										</form>
										<!--end::Form-->
									</div>
									<!--end::Modal body-->
								</div>
								<!--end::Modal content-->
							</div>
							<!--end::Modal dialog-->
						</div>
						<!--end::Modal - New Card-->
						<!--begin::Modal - Groups-->
						<div class="modal fade" id="kt_modal_view_users" tabindex="-1" aria-hidden="true">
							<!--begin::Modal dialog-->
							<div class="modal-dialog mw-650px">
								<!--begin::Modal content-->
								<div class="modal-content">
									<!--begin::Modal header-->
									<div class="modal-header pb-0 border-0">
										<!--begin::Title-->
										<h1>Personal Groups</h1>
											<!--end::Title-->
										<!--begin::Close-->
										<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
											<span class="svg-icon svg-icon-1">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
													<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
										</div>
										<!--end::Close-->
									</div>
									<!--begin::Modal header-->
									<!--begin::Modal body-->
									<div class="modal-body scroll-y mx-5 pt-0 pb-15">
										<!--begin::Users-->
										<div class="mt-5">
											<div class="d-flex justify-content-end" style="">
												<a href="#" class="btn btn-sm btn-bg-light btn-active-color-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_users_search">Add new</a>
											</div>
											<!--begin::List-->
											<div class="mh-375px scroll-y me-n7 pe-7">
												<!--begin::User-->
												<div class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
													<!--begin::Details-->
													<div class="d-flex align-items-center">
														<!--begin::Details-->
														<div class="">
															<!--begin::Name-->
															<a href="#" class="d-flex align-items-center fs-5 fw-bolder text-dark text-hover-primary">Clients 1
																<span class="badge badge-light fs-8 fw-bold ms-2">Art Director</span></a>
															<!--end::Name-->
															<!--begin::Email-->
															<div class="fw-bold text-muted">e.smith@kpmg.com.au</div>
															<!--end::Email-->
														</div>
														<!--end::Details-->
													</div>
													<!--end::Details-->
													<!--begin::Stats-->
													<div class="d-flex">
														<!--begin::Sales-->
														<div class="text-end">
															<!--begin::Menu-->
												<div class="me-0">
													<button class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
														<i class="bi bi-three-dots fs-3"></i>
													</button>
													<!--begin::Menu 3-->
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Edit</a>
														</div>
														<!--end::Menu item-->
														
														<!--begin::Menu item-->
														<div class="menu-item px-3 my-1">
															<a href="#" class="menu-link px-3">Delete</a>
														</div>
														<!--end::Menu item-->
													</div>
													<!--end::Menu 3-->
												</div>
												<!--end::Menu-->
														</div>
														<!--end::Sales-->
													</div>
													<!--end::Stats-->
												</div>
												<!--end::User-->
												<!--begin::User-->
												<div class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
													<!--begin::Details-->
													<div class="d-flex align-items-center">
														<!--begin::Details-->
														<div class="">
															<!--begin::Name-->
															<a href="#" class="d-flex align-items-center fs-5 fw-bolder text-dark text-hover-primary">Clients 2
																<span class="badge badge-light fs-8 fw-bold ms-2">Marketing Analytic</span></a>
															<!--end::Name-->
															<!--begin::Email-->
															<div class="fw-bold text-muted">melody@altbox.com</div>
															<!--end::Email-->
														</div>
														<!--end::Details-->
													</div>
													<!--end::Details-->
													<!--begin::Stats-->
													<div class="d-flex">
														<!--begin::Sales-->
														<div class="text-end">
															<!--begin::Menu-->
												<div class="me-0">
													<button class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
														<i class="bi bi-three-dots fs-3"></i>
													</button>
													<!--begin::Menu 3-->
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Edit</a>
														</div>
														<!--end::Menu item-->
														
														<!--begin::Menu item-->
														<div class="menu-item px-3 my-1">
															<a href="#" class="menu-link px-3">Delete</a>
														</div>
														<!--end::Menu item-->
													</div>
													<!--end::Menu 3-->
												</div>
												<!--end::Menu-->
														</div>
														<!--end::Sales-->
													</div>
													<!--end::Stats-->
												</div>
												<!--end::User-->
											</div>
											<!--end::List-->
										</div>
										<!--end::Users-->
									</div>
									<!--end::Modal body-->
								</div>
								<!--end::Modal content-->
							</div>
							<!--end::Modal dialog-->
						</div>
						<!--end::Modal - View Users-->
						<!--end::Modals-->
					</div>
					<!--end::Container-->
				</div>
				<!--end::Content-->
				<!--begin::Footer-->
				<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
					<!--begin::Container-->
					<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
						<!--begin::Copyright-->
						<div class="text-dark order-2 order-md-1">
							<span class="text-muted fw-bold me-1">2021©</span>
							<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
						</div>
						<!--end::Copyright-->
						<!--begin::Menu-->
						<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
							<li class="menu-item">
								<a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
							</li>
							<li class="menu-item">
								<a href="https://keenthemes.com/support" target="_blank" class="menu-link px-2">Support</a>
							</li>
							<li class="menu-item">
								<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
							</li>
						</ul>
						<!--end::Menu-->
					</div>
					<!--end::Container-->
				</div>
				<!--end::Footer-->
			</div>
			<!--end::Wrapper-->
		</div>
		<!--end::Page-->
	</div>
	<!--end::Root-->
	{{ theme()->getView('pages/header_right') }}
	<!--begin::Scrolltop-->
	<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
		<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
		<span class="svg-icon">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
				<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
				<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
			</svg>
		</span>
		<!--end::Svg Icon-->
	</div>
	<!--end::Scrolltop-->
	<!--end::Main-->
	<!--begin::Javascript-->
	<!--begin::Global Javascript Bundle(used by all pages)-->
	<script src="assets/plugins/global/plugins.bundle.js"></script>
	<script src="assets/js/scripts.bundle.js"></script>
	<!--end::Global Javascript Bundle-->
	<!--begin::Page Vendors Javascript(used by this page)-->
	<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
	<!--end::Page Vendors Javascript-->
	<!--begin::Page Custom Javascript(used by this page)-->
	<script src="assets/js/custom/apps/customers/list/export.js"></script>
	<script src="assets/js/custom/apps/customers/list/list.js"></script>
	<script src="assets/js/custom/apps/customers/add.js"></script>
	<script src="assets/js/custom/widgets.js"></script>
	<script src="assets/js/custom/apps/chat/chat.js"></script>
	<script src="assets/js/custom/modals/create-app.js"></script>
	<script src="assets/js/custom/modals/upgrade-plan.js"></script>
	<!--end::Page Custom Javascript-->
	<!--end::Javascript-->
</body>
<!--end::Body-->

</html>