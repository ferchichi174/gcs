<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="../../">
		<title>GoCC</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendor Stylesheets(used by this page)-->
		<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				{{ theme()->getView('pages/aside') }}
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					{{ theme()->getView('pages/header') }}
					<!--end::Header-->
					<!--begin::Toolbar-->
					<div class="toolbar py-2" id="kt_toolbar">
						<!--begin::Container-->
						<div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
							<!--begin::Page title-->
							<div class="flex-grow-1 flex-shrink-0 me-5">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Search contacts</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-3"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="../../demo6/dist/index.html" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">Contacts</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Search</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
							</div>
							<!--end::Page title-->
							<!--begin::Action group-->
							<div class="d-flex align-items-center flex-wrap">
								
								<!--begin::Wrapper-->
								<div class="d-flex align-items-center">
									
									<!--begin::Actions-->
									
									<!--end::Actions-->
								</div>
								<!--end::Wrapper-->
							</div>
							<!--end::Action group-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Toolbar-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-fluid">
							<!--begin::Form-->
							<form action="#">
								<!--begin::Card-->
								<div class="card mb-7">
									<!--begin::Card body-->
									<div class="card-body">
										<!--begin::Compact form-->
										<div class="d-flex align-items-center">
											<!--begin::Input group-->
											<div class="position-relative w-md-400px me-md-2">
												<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
												<span class="svg-icon svg-icon-3 svg-icon-gray-500 position-absolute top-50 translate-middle ms-6">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
														<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<input type="text" class="form-control form-control-solid ps-10" name="search" value="" placeholder="Search" />
											</div>
											<!--end::Input group-->
											<!--begin:Action-->
											<div class="d-flex align-items-center">
												<button type="submit" class="btn btn-primary me-5">Search</button>
												<a id="kt_horizontal_search_advanced_link" class="btn btn-link" data-bs-toggle="collapse" href="#kt_advanced_search_form">Advanced Search</a>
											</div>
											<!--end:Action-->
										</div>
										<!--end::Compact form-->
										<!--begin::Advance form-->
										<div class="collapse" id="kt_advanced_search_form">
											<!--begin::Separator-->
											<div class="separator separator-dashed mt-9 mb-6"></div>
											<!--end::Separator-->
											<!--begin::Row-->
											<div class="row g-8 mb-8">
												<!--begin::Col-->
												<div class="col-xxl-7">
													<label class="fs-6 form-label fw-bolder text-dark">Tags</label>
													<input type="text" class="form-control form-control form-control-solid" name="tags" value="products, users, events" />
												</div>
												<!--end::Col-->
												<!--begin::Col-->
												<div class="col-xxl-5">
													<!--begin::Row-->
													<div class="row g-8">
														<!--begin::Col-->
														<div class="col-lg-6">
															<label class="fs-6 form-label fw-bolder text-dark">Team Type</label>
															<!--begin::Select-->
															<select class="form-select form-select-solid" data-control="select2" data-placeholder="In Progress" data-hide-search="true">
																<option value=""></option>
																<option value="1">Not started</option>
																<option value="2" selected="selected">In Progress</option>
																<option value="3">Done</option>
															</select>
															<!--end::Select-->
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-lg-6">
															<label class="fs-6 form-label fw-bolder text-dark">Select Group</label>
															<!--begin::Radio group-->
															<div class="nav-group nav-group-fluid">
																<!--begin::Option-->
																<label>
																	<input type="radio" class="btn-check" name="type" value="has" checked="checked" />
																	<span class="btn btn-sm btn-color-muted btn-active btn-active-primary fw-bolder px-4">All</span>
																</label>
																<!--end::Option-->
																<!--begin::Option-->
																<label>
																	<input type="radio" class="btn-check" name="type" value="users" />
																	<span class="btn btn-sm btn-color-muted btn-active btn-active-primary fw-bolder px-4">Users</span>
																</label>
																<!--end::Option-->
																<!--begin::Option-->
																<label>
																	<input type="radio" class="btn-check" name="type" value="orders" />
																	<span class="btn btn-sm btn-color-muted btn-active btn-active-primary fw-bolder px-4">Orders</span>
																</label>
																<!--end::Option-->
															</div>
															<!--end::Radio group-->
														</div>
														<!--end::Col-->
													</div>
													<!--end::Row-->
												</div>
												<!--end::Col-->
											</div>
											<!--end::Row-->
											<!--begin::Row-->
											<div class="row g-8">
												<!--begin::Col-->
												<div class="col-xxl-7">
													<!--begin::Row-->
													<div class="row g-8">
														<!--begin::Col-->
														<div class="col-lg-4">
															<label class="fs-6 form-label fw-bolder text-dark">Min. Amount</label>
															<!--begin::Dialer-->
															<div class="position-relative" data-kt-dialer="true" data-kt-dialer-min="1000" data-kt-dialer-max="50000" data-kt-dialer-step="1000" data-kt-dialer-prefix="$" data-kt-dialer-decimals="2">
																<!--begin::Decrease control-->
																<button type="button" class="btn btn-icon btn-active-color-gray-700 position-absolute translate-middle-y top-50 start-0" data-kt-dialer-control="decrease">
																	<!--begin::Svg Icon | path: icons/duotune/general/gen042.svg-->
																	<span class="svg-icon svg-icon-1">
																		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																			<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
																			<rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
																		</svg>
																	</span>
																	<!--end::Svg Icon-->
																</button>
																<!--end::Decrease control-->
																<!--begin::Input control-->
																<input type="text" class="form-control form-control-solid border-0 ps-12" data-kt-dialer-control="input" placeholder="Amount" name="manageBudget" readonly="readonly" value="$50" />
																<!--end::Input control-->
																<!--begin::Increase control-->
																<button type="button" class="btn btn-icon btn-active-color-gray-700 position-absolute translate-middle-y top-50 end-0" data-kt-dialer-control="increase">
																	<!--begin::Svg Icon | path: icons/duotune/general/gen041.svg-->
																	<span class="svg-icon svg-icon-1">
																		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																			<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
																			<rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black" />
																			<rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
																		</svg>
																	</span>
																	<!--end::Svg Icon-->
																</button>
																<!--end::Increase control-->
															</div>
															<!--end::Dialer-->
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-lg-4">
															<label class="fs-6 form-label fw-bolder text-dark">Max. Amount</label>
															<!--begin::Dialer-->
															<div class="position-relative" data-kt-dialer="true" data-kt-dialer-min="1000" data-kt-dialer-max="50000" data-kt-dialer-step="1000" data-kt-dialer-prefix="$" data-kt-dialer-decimals="2">
																<!--begin::Decrease control-->
																<button type="button" class="btn btn-icon btn-active-color-gray-700 position-absolute translate-middle-y top-50 start-0" data-kt-dialer-control="decrease">
																	<!--begin::Svg Icon | path: icons/duotune/general/gen042.svg-->
																	<span class="svg-icon svg-icon-1">
																		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																			<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
																			<rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
																		</svg>
																	</span>
																	<!--end::Svg Icon-->
																</button>
																<!--end::Decrease control-->
																<!--begin::Input control-->
																<input type="text" class="form-control form-control-solid border-0 ps-12" data-kt-dialer-control="input" placeholder="Amount" name="manageBudget" readonly="readonly" value="$100" />
																<!--end::Input control-->
																<!--begin::Increase control-->
																<button type="button" class="btn btn-icon btn-active-color-gray-700 position-absolute translate-middle-y top-50 end-0" data-kt-dialer-control="increase">
																	<!--begin::Svg Icon | path: icons/duotune/general/gen041.svg-->
																	<span class="svg-icon svg-icon-1">
																		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																			<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
																			<rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black" />
																			<rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
																		</svg>
																	</span>
																	<!--end::Svg Icon-->
																</button>
																<!--end::Increase control-->
															</div>
															<!--end::Dialer-->
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-lg-4">
															<label class="fs-6 form-label fw-bolder text-dark">Team Size</label>
															<input type="text" class="form-control form-control form-control-solid" name="city" />
														</div>
														<!--end::Col-->
													</div>
													<!--end::Row-->
												</div>
												<!--end::Col-->
												<!--begin::Col-->
												<div class="col-xxl-5">
													<!--begin::Row-->
													<div class="row g-8">
														<!--begin::Col-->
														<div class="col-lg-6">
															<label class="fs-6 form-label fw-bolder text-dark">Category</label>
															<!--begin::Select-->
															<select class="form-select form-select-solid" data-control="select2" data-placeholder="In Progress" data-hide-search="true">
																<option value=""></option>
																<option value="1">Not started</option>
																<option value="2" selected="selected">Select</option>
																<option value="3">Done</option>
															</select>
															<!--end::Select-->
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-lg-6">
															<label class="fs-6 form-label fw-bolder text-dark">Status</label>
															<div class="form-check form-switch form-check-custom form-check-solid mt-1">
																<input class="form-check-input" type="checkbox" value="" id="flexSwitchChecked" checked="checked" />
																<label class="form-check-label" for="flexSwitchChecked">Active</label>
															</div>
														</div>
														<!--end::Col-->
													</div>
													<!--end::Row-->
												</div>
												<!--end::Col-->
											</div>
											<!--end::Row-->
										</div>
										<!--end::Advance form-->
									</div>
									<!--end::Card body-->
								</div>
								<!--end::Card-->
							</form>
							<!--end::Form-->
							<!--begin::Toolbar-->
							<div class="d-flex flex-wrap flex-stack pb-7">
								<!--begin::Title-->
								<div class="d-flex flex-wrap align-items-center my-1">
									<h3 class="fw-bolder me-5 my-1">57 Items Found
									<span class="text-gray-400 fs-6">by Recent Updates ↓</span></h3>
								</div>
								<!--end::Title-->
								<!--begin::Controls-->
								<div class="d-flex flex-wrap my-1">
									<!--begin::Tab nav-->
									<ul class="nav nav-pills me-6 mb-2 mb-sm-0">
										<li class="nav-item m-0">
											<a class="btn btn-sm btn-icon btn-light btn-color-muted btn-active-primary me-3 active" data-bs-toggle="tab" href="#kt_project_users_card_pane">
												<!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
												<span class="svg-icon svg-icon-2">
													<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
															<rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
															<rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
															<rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
														</g>
													</svg>
												</span>
												<!--end::Svg Icon-->
											</a>
										</li>
										<li class="nav-item m-0">
											<a class="btn btn-sm btn-icon btn-light btn-color-muted btn-active-primary" data-bs-toggle="tab" href="#kt_project_users_table_pane">
												<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
												<span class="svg-icon svg-icon-2">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
														<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
											</a>
										</li>
									</ul>
									<!--end::Tab nav-->
									<!--begin::Actions-->
									<div class="d-flex my-0">
										<!--begin::Select-->
										<select name="status" data-control="select2" data-hide-search="true" data-placeholder="Filter" class="form-select form-select-white form-select-sm w-150px me-5">
											<option value="1">Recently Updated</option>
											<option value="2">Last Month</option>
											<option value="3">Last Quarter</option>
											<option value="4">Last Year</option>
										</select>
										<!--end::Select-->
										<!--begin::Select-->
										<select name="status" data-control="select2" data-hide-search="true" data-placeholder="Export" class="form-select form-select-white form-select-sm w-100px">
											<option value="1">Excel</option>
											<option value="1">PDF</option>
											<option value="2">Print</option>
										</select>
										<!--end::Select-->
									</div>
									<!--end::Actions-->
								</div>
								<!--end::Controls-->
							</div>
							<!--end::Toolbar-->
							<!--begin::Tab Content-->
							<div class="tab-content">
								<!--begin::Tab pane-->
								<div id="kt_project_users_card_pane" class="tab-pane fade show active">
									<!--begin::Row-->
									<div class="row g-6 g-xl-9">
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<img src="assets/media//avatars/150-3.jpg" alt="image" />
														<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n3 mt-n3"></div>
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Karina Clark</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">Art Director at Novica Co.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<span class="symbol-label fs-2x fw-bold text-primary bg-light-primary">S</span>
														<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n3 mt-n3"></div>
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Sean Bean</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">Developer at Loop Inc</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<img src="assets/media//avatars/150-2.jpg" alt="image" />
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Alan Johnson</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">Web Designer at Nextop Ltd.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<img src="assets/media//avatars/150-11.jpg" alt="image" />
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Robert Doe</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">Marketing Analytic at Avito Ltd.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<img src="assets/media//avatars/150-1.jpg" alt="image" />
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Olivia Wild</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">Art Director at Seal Inc.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<span class="symbol-label fs-2x fw-bold text-warning bg-light-warning">A</span>
														<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n3 mt-n3"></div>
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Adam Williams</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">System Arcitect at Wolto Co.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<span class="symbol-label fs-2x fw-bold text-info bg-light-info">P</span>
														<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n3 mt-n3"></div>
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Peter Marcus</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">Art Director at Novica Co.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<span class="symbol-label fs-2x fw-bold text-success bg-light-success">N</span>
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Neil Owen</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">Accountant at Numbers Co.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													<!--begin::Avatar-->
													<div class="symbol symbol-65px symbol-circle mb-5">
														<img src="assets/media//avatars/150-7.jpg" alt="image" />
													</div>
													<!--end::Avatar-->
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Benjamin Jacob</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">Art Director at Novica Co.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
									</div>
									<!--end::Row-->
									<!--begin::Pagination-->
									<div class="d-flex flex-stack flex-wrap pt-10">
										<div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of 50 entries</div>
										<!--begin::Pages-->
										<ul class="pagination">
											<li class="page-item previous">
												<a href="#" class="page-link">
													<i class="previous"></i>
												</a>
											</li>
											<li class="page-item active">
												<a href="#" class="page-link">1</a>
											</li>
											<li class="page-item">
												<a href="#" class="page-link">2</a>
											</li>
											<li class="page-item">
												<a href="#" class="page-link">3</a>
											</li>
											<li class="page-item">
												<a href="#" class="page-link">4</a>
											</li>
											<li class="page-item">
												<a href="#" class="page-link">5</a>
											</li>
											<li class="page-item">
												<a href="#" class="page-link">6</a>
											</li>
											<li class="page-item next">
												<a href="#" class="page-link">
													<i class="next"></i>
												</a>
											</li>
										</ul>
										<!--end::Pages-->
									</div>
									<!--end::Pagination-->
								</div>
								<!--end::Tab pane-->
								<!--begin::Tab pane-->
								<div id="kt_project_users_table_pane" class="tab-pane fade">
									<!--begin::Card-->
									<div class="card card-flush">
										<!--begin::Card body-->
										<div class="card-body pt-0">
											<!--begin::Table container-->
											<div class="table-responsive">
												<!--begin::Table-->
												<table id="kt_project_users_table" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bolder">
													<!--begin::Head-->
													<thead class="fs-7 text-gray-400 text-uppercase">
														<tr>
															<th class="min-w-250px">Manager</th>
															<th class="min-w-150px">Date</th>
															<th class="min-w-90px">Amount</th>
															<th class="min-w-90px">Status</th>
															<th class="min-w-50px text-end">Details</th>
														</tr>
													</thead>
													<!--end::Head-->
													<!--begin::Body-->
													<tbody class="fs-6">
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-1.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">e.smith@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>May 05, 2021</td>
															<td>$531.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">M</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Melody Macy</a>
																		<div class="fw-bold fs-6 text-gray-400">melody@altbox.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>May 05, 2021</td>
															<td>$853.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-26.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Max Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">max@kt.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 20, 2021</td>
															<td>$565.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-4.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Sean Bean</a>
																		<div class="fw-bold fs-6 text-gray-400">sean@dellito.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Nov 10, 2021</td>
															<td>$767.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-15.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Brian Cox</a>
																		<div class="fw-bold fs-6 text-gray-400">brian@exchange.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$919.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-warning text-warning fw-bold">M</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Mikaela Collins</a>
																		<div class="fw-bold fs-6 text-gray-400">mikaela@pexcom.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Nov 10, 2021</td>
															<td>$764.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-8.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Francis Mitcham</a>
																		<div class="fw-bold fs-6 text-gray-400">f.mitcham@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 24, 2021</td>
															<td>$701.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">O</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Olivia Wild</a>
																		<div class="fw-bold fs-6 text-gray-400">olivia@corpmail.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Nov 10, 2021</td>
															<td>$702.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-primary text-primary fw-bold">N</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Neil Owen</a>
																		<div class="fw-bold fs-6 text-gray-400">owen.neil@gmail.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Nov 10, 2021</td>
															<td>$595.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-6.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Dan Wilson</a>
																		<div class="fw-bold fs-6 text-gray-400">dam@consilting.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$561.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">E</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Bold</a>
																		<div class="fw-bold fs-6 text-gray-400">emma@intenso.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Oct 25, 2021</td>
															<td>$461.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-7.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Ana Crown</a>
																		<div class="fw-bold fs-6 text-gray-400">ana.cf@limtel.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>May 05, 2021</td>
															<td>$714.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-info text-info fw-bold">A</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Robert Doe</a>
																		<div class="fw-bold fs-6 text-gray-400">robert@benko.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Mar 10, 2021</td>
															<td>$593.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-17.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">John Miller</a>
																		<div class="fw-bold fs-6 text-gray-400">miller@mapple.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>May 05, 2021</td>
															<td>$585.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-success text-success fw-bold">L</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Lucy Kunic</a>
																		<div class="fw-bold fs-6 text-gray-400">lucy.m@fentech.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Dec 20, 2021</td>
															<td>$850.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-10.jpg" />
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Ethan Wilder</a>
																		<div class="fw-bold fs-6 text-gray-400">ethan@loop.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Dec 20, 2021</td>
															<td>$644.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-1.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">e.smith@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 20, 2021</td>
															<td>$975.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-primary text-primary fw-bold">N</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Neil Owen</a>
																		<div class="fw-bold fs-6 text-gray-400">owen.neil@gmail.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Oct 25, 2021</td>
															<td>$740.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">E</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Bold</a>
																		<div class="fw-bold fs-6 text-gray-400">emma@intenso.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$997.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">E</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Bold</a>
																		<div class="fw-bold fs-6 text-gray-400">emma@intenso.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$614.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">M</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Melody Macy</a>
																		<div class="fw-bold fs-6 text-gray-400">melody@altbox.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>May 05, 2021</td>
															<td>$434.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-warning text-warning fw-bold">M</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Mikaela Collins</a>
																		<div class="fw-bold fs-6 text-gray-400">mikaela@pexcom.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 24, 2021</td>
															<td>$561.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-26.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Max Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">max@kt.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$854.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-4.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Sean Bean</a>
																		<div class="fw-bold fs-6 text-gray-400">sean@dellito.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Mar 10, 2021</td>
															<td>$552.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-17.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">John Miller</a>
																		<div class="fw-bold fs-6 text-gray-400">miller@mapple.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Mar 10, 2021</td>
															<td>$709.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-15.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Brian Cox</a>
																		<div class="fw-bold fs-6 text-gray-400">brian@exchange.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jul 25, 2021</td>
															<td>$716.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-17.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">John Miller</a>
																		<div class="fw-bold fs-6 text-gray-400">miller@mapple.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Aug 19, 2021</td>
															<td>$717.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-1.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">e.smith@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Nov 10, 2021</td>
															<td>$769.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-15.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Brian Cox</a>
																		<div class="fw-bold fs-6 text-gray-400">brian@exchange.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Sep 22, 2021</td>
															<td>$602.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-6.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Dan Wilson</a>
																		<div class="fw-bold fs-6 text-gray-400">dam@consilting.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Nov 10, 2021</td>
															<td>$630.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
													</tbody>
													<!--end::Body-->
												</table>
												<!--end::Table-->
											</div>
											<!--end::Table container-->
										</div>
										<!--end::Card body-->
									</div>
									<!--end::Card-->
								</div>
								<!--end::Tab pane-->
							</div>
							<!--end::Tab Content-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted fw-bold me-1">2021©</span>
								<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Menu-->
							<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
								<li class="menu-item">
									<a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
								</li>
								<li class="menu-item">
									<a href="https://keenthemes.com/support" target="_blank" class="menu-link px-2">Support</a>
								</li>
								<li class="menu-item">
									<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
								</li>
							</ul>
							<!--end::Menu-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		{{ theme()->getView('pages/header_right') }}
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<!--end::Main-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/pages/search/horizontal.js"></script>
		<script src="assets/js/custom/pages/projects/users/users.js"></script>
		<script src="assets/js/custom/widgets.js"></script>
		<script src="assets/js/custom/apps/chat/chat.js"></script>
		<script src="assets/js/custom/modals/create-app.js"></script>
		<script src="assets/js/custom/modals/upgrade-plan.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>