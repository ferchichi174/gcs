<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="../../">
		<title>GoCC</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendor Stylesheets(used by this page)-->
		<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				{{ theme()->getView('pages/aside') }}
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					{{ theme()->getView('pages/header') }}
					<!--end::Header-->
					<!--begin::Toolbar-->
					<div class="toolbar py-3" id="kt_toolbar">
						<!--begin::Container-->
						<div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
							<!--begin::Page title-->
							<div class="flex-grow-1 flex-shrink-0 me-5">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Organization</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-3"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="../../demo6/dist/index.html" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Organization</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
							</div>
							<!--end::Page title-->
							<!--begin::Action group-->
							<div class="d-flex align-items-center flex-wrap">
								
								<!--begin::Wrapper-->
								<div class="d-flex align-items-center">
									
									<!--begin::Actions-->
									<div class="d-flex align-items-center">
										<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light btn-active-color-primary">
											<!--begin::Svg Icon | path: icons/duotune/files/fil005.svg-->
											<span class="svg-icon svg-icon-2x">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13H13V10C13 9.4 12.6 9 12 9C11.4 9 11 9.4 11 10V13H8C7.4 13 7 13.4 7 14C7 14.6 7.4 15 8 15H11V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18V15H16C16.6 15 17 14.6 17 14C17 13.4 16.6 13 16 13Z" fill="black" />
													<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
										</button>
									</div>
									<!--end::Actions-->
								</div>
								<!--end::Wrapper-->
							</div>
							<!--end::Action group-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Toolbar-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid pt-3" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-fluid">
							<!--begin::Navbar-->
						<div class="card mb-6 mb-xl-9">
							<div class="card-body p-0">
								<!--begin::Nav wrapper-->
								<div class="d-flex overflow-auto h-55px">
									<!--begin::Nav links-->
									<ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/countries">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/maps/map004.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M18.4 5.59998C21.9 9.09998 21.9 14.8 18.4 18.3C14.9 21.8 9.2 21.8 5.7 18.3L18.4 5.59998Z" fill="black" />
														<path d="M12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2ZM19.9 11H13V8.8999C14.9 8.6999 16.7 8.00005 18.1 6.80005C19.1 8.00005 19.7 9.4 19.9 11ZM11 19.8999C9.7 19.6999 8.39999 19.2 7.39999 18.5C8.49999 17.7 9.7 17.2001 11 17.1001V19.8999ZM5.89999 6.90002C7.39999 8.10002 9.2 8.8 11 9V11.1001H4.10001C4.30001 9.4001 4.89999 8.00002 5.89999 6.90002ZM7.39999 5.5C8.49999 4.7 9.7 4.19998 11 4.09998V7C9.7 6.8 8.39999 6.3 7.39999 5.5ZM13 17.1001C14.3 17.3001 15.6 17.8 16.6 18.5C15.5 19.3 14.3 19.7999 13 19.8999V17.1001ZM13 4.09998C14.3 4.29998 15.6 4.8 16.6 5.5C15.5 6.3 14.3 6.80002 13 6.90002V4.09998ZM4.10001 13H11V15.1001C9.1 15.3001 7.29999 16 5.89999 17.2C4.89999 16 4.30001 14.6 4.10001 13ZM18.1 17.1001C16.6 15.9001 14.8 15.2 13 15V12.8999H19.9C19.7 14.5999 19.1 16.0001 18.1 17.1001Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Countries scope</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/communications">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/electronics/elc007.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M12.4409 22C13.5455 22 14.4409 21.1046 14.4409 20C14.4409 18.8954 13.5455 18 12.4409 18C11.3364 18 10.4409 18.8954 10.4409 20C10.4409 21.1046 11.3364 22 12.4409 22Z" fill="black" />
														<path opacity="0.3" d="M9.04095 14.8L9.94095 16.1C10.6409 15.6 11.5409 15.3 12.4409 15.3C13.3409 15.3 14.2409 15.6 14.9409 16.1L15.8409 14.8C16.1409 14.3 16.0409 13.6 15.4409 13.4C14.5409 13 13.5409 12.7 12.4409 12.7C11.3409 12.7 10.3409 12.9 9.44095 13.4C8.84095 13.6 8.74095 14.3 9.04095 14.8Z" fill="black" />
														<path opacity="0.3" d="M3.14096 5.80005L4.04095 7.19995C6.44095 5.59995 9.34094 4.69995 12.4409 4.69995C15.5409 4.69995 18.4409 5.59995 20.8409 7.19995L21.7409 5.80005C22.0409 5.30005 21.8409 4.70002 21.3409 4.40002C18.7409 2.90002 15.6409 2 12.4409 2C9.24094 2 6.14095 2.90002 3.54095 4.40002C3.04095 4.70002 2.84096 5.30005 3.14096 5.80005Z" fill="black" />
														<path opacity="0.3" d="M6.14097 10.3L7.04096 11.7C8.64096 10.7 10.441 10.1 12.541 10.1C14.641 10.1 16.441 10.7 18.041 11.7L18.941 10.3C19.241 9.80005 19.141 9.10002 18.541 8.90002C16.741 7.90002 14.741 7.40002 12.541 7.40002C10.341 7.40002 8.34096 7.90002 6.54096 8.90002C5.94096 9.10002 5.74097 9.80005 6.14097 10.3Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Communications</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/integrations">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/files/fil021.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="black" />
														<path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="black" />
														<path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Integrations</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link active btn btn-flex btn-active-light-primary m-0" href="public/organization/reports">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/graphs/gra010.svg-->
												<span class="svg-icon svg-icon-primary svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M13.0021 10.9128V3.01281C13.0021 2.41281 13.5021 1.91281 14.1021 2.01281C16.1021 2.21281 17.9021 3.11284 19.3021 4.61284C20.7021 6.01284 21.6021 7.91285 21.9021 9.81285C22.0021 10.4129 21.5021 10.9128 20.9021 10.9128H13.0021Z" fill="black" />
														<path opacity="0.3" d="M11.0021 13.7128V4.91283C11.0021 4.31283 10.5021 3.81283 9.90208 3.91283C5.40208 4.51283 1.90209 8.41284 2.00209 13.1128C2.10209 18.0128 6.40208 22.0128 11.3021 21.9128C13.1021 21.8128 14.7021 21.3128 16.0021 20.4128C16.5021 20.1128 16.6021 19.3128 16.1021 18.9128L11.0021 13.7128Z" fill="black" />
														<path opacity="0.3" d="M21.9021 14.0128C21.7021 15.6128 21.1021 17.1128 20.1021 18.4128C19.7021 18.9128 19.0021 18.9128 18.6021 18.5128L13.0021 12.9128H20.9021C21.5021 12.9128 22.0021 13.4128 21.9021 14.0128Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4 fw-bolder">Reports displaying</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/landing_pages">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/electronics/elc004.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M2 16C2 16.6 2.4 17 3 17H21C21.6 17 22 16.6 22 16V15H2V16Z" fill="black" />
														<path opacity="0.3" d="M21 3H3C2.4 3 2 3.4 2 4V15H22V4C22 3.4 21.6 3 21 3Z" fill="black" />
														<path opacity="0.3" d="M15 17H9V20H15V17Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Landing pages</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/teams">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com005.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z" fill="black" />
														<path opacity="0.3" d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Teams</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/roles">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/abstract/abs027.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black" />
														<path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Roles</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/extensions">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/technology/teh008.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M11 6.5C11 9 9 11 6.5 11C4 11 2 9 2 6.5C2 4 4 2 6.5 2C9 2 11 4 11 6.5ZM17.5 2C15 2 13 4 13 6.5C13 9 15 11 17.5 11C20 11 22 9 22 6.5C22 4 20 2 17.5 2ZM6.5 13C4 13 2 15 2 17.5C2 20 4 22 6.5 22C9 22 11 20 11 17.5C11 15 9 13 6.5 13ZM17.5 13C15 13 13 15 13 17.5C13 20 15 22 17.5 22C20 22 22 20 22 17.5C22 15 20 13 17.5 13Z" fill="black" />
														<path d="M17.5 16C17.5 16 17.4 16 17.5 16L16.7 15.3C16.1 14.7 15.7 13.9 15.6 13.1C15.5 12.4 15.5 11.6 15.6 10.8C15.7 9.99999 16.1 9.19998 16.7 8.59998L17.4 7.90002H17.5C18.3 7.90002 19 7.20002 19 6.40002C19 5.60002 18.3 4.90002 17.5 4.90002C16.7 4.90002 16 5.60002 16 6.40002V6.5L15.3 7.20001C14.7 7.80001 13.9 8.19999 13.1 8.29999C12.4 8.39999 11.6 8.39999 10.8 8.29999C9.99999 8.19999 9.20001 7.80001 8.60001 7.20001L7.89999 6.5V6.40002C7.89999 5.60002 7.19999 4.90002 6.39999 4.90002C5.59999 4.90002 4.89999 5.60002 4.89999 6.40002C4.89999 7.20002 5.59999 7.90002 6.39999 7.90002H6.5L7.20001 8.59998C7.80001 9.19998 8.19999 9.99999 8.29999 10.8C8.39999 11.5 8.39999 12.3 8.29999 13.1C8.19999 13.9 7.80001 14.7 7.20001 15.3L6.5 16H6.39999C5.59999 16 4.89999 16.7 4.89999 17.5C4.89999 18.3 5.59999 19 6.39999 19C7.19999 19 7.89999 18.3 7.89999 17.5V17.4L8.60001 16.7C9.20001 16.1 9.99999 15.7 10.8 15.6C11.5 15.5 12.3 15.5 13.1 15.6C13.9 15.7 14.7 16.1 15.3 16.7L16 17.4V17.5C16 18.3 16.7 19 17.5 19C18.3 19 19 18.3 19 17.5C19 16.7 18.3 16 17.5 16Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Extensions</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/users">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com014.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="black" />
														<rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="black" />
														<path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="black" />
														<rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Users</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->






									</ul>
									<!--end::Nav links-->
								</div>
								<!--end::Nav wrapper-->
							</div>
						</div>
						<!--end::Navbar-->
							<!--begin::Basic info-->
							<div class="card mb-5 mb-xl-10">
								<!--begin::Card header-->
							<div class="card-header m-0 pt-4 pb-4 bg-lighten">
								<!--begin::Card title-->
								<div class="card-title flex-column">
									<h3 class="fw-bolder mb-1">Displaying Details</h3>
									<div class="fs-6 text-gray-400">Define all settings of your reports</div>
								</div>
								<!--begin::Card title-->
							</div>
							<!--end::Card header-->
								<!--begin::Content-->
								<div id="kt_account_profile_details" class="collapse show">
									<!--begin::Form-->
									<form id="kt_account_profile_details_form" class="form">
										<!--begin::Card body-->
										<div class="card-body border-top p-9">
											
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label required fw-bold fs-6">Full Name</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8">
													<!--begin::Row-->
													<div class="row">
														<!--begin::Col-->
														<div class="col-lg-6 fv-row">
															<input type="text" name="fname" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="First name" value="Max" />
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-lg-6 fv-row">
															<input type="text" name="lname" class="form-control form-control-lg form-control-solid" placeholder="Last name" value="Smith" />
														</div>
														<!--end::Col-->
													</div>
													<!--end::Row-->
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label required fw-bold fs-6">Company</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<input type="text" name="company" class="form-control form-control-lg form-control-solid" placeholder="Company name" value="Keenthemes" />
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label fw-bold fs-6">
													<span class="required">Contact Phone</span>
													<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Phone number must be active"></i>
												</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<input type="tel" name="phone" class="form-control form-control-lg form-control-solid" placeholder="Phone number" value="044 3276 454 935" />
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label fw-bold fs-6">Company Site</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<input type="text" name="website" class="form-control form-control-lg form-control-solid" placeholder="Company website" value="keenthemes.com" />
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label fw-bold fs-6">
													<span class="required">Country</span>
													<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
												</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<select name="country" aria-label="Select a Country" data-control="select2" data-placeholder="Select a country..." class="form-select form-select-solid form-select-lg fw-bold">
														<option value="">Select a Country...</option>
														<option data-kt-flag="flags/afghanistan.svg" value="AF">Afghanistan</option>
														<option data-kt-flag="flags/aland-islands.svg" value="AX">Aland Islands</option>
														<option data-kt-flag="flags/albania.svg" value="AL">Albania</option>
														<option data-kt-flag="flags/algeria.svg" value="DZ">Algeria</option>
														<option data-kt-flag="flags/american-samoa.svg" value="AS">American Samoa</option>
														<option data-kt-flag="flags/andorra.svg" value="AD">Andorra</option>
														<option data-kt-flag="flags/angola.svg" value="AO">Angola</option>
														<option data-kt-flag="flags/anguilla.svg" value="AI">Anguilla</option>
														<option data-kt-flag="flags/antigua-and-barbuda.svg" value="AG">Antigua and Barbuda</option>
														<option data-kt-flag="flags/argentina.svg" value="AR">Argentina</option>
														<option data-kt-flag="flags/armenia.svg" value="AM">Armenia</option>
														<option data-kt-flag="flags/aruba.svg" value="AW">Aruba</option>
														<option data-kt-flag="flags/australia.svg" value="AU">Australia</option>
														<option data-kt-flag="flags/austria.svg" value="AT">Austria</option>
														<option data-kt-flag="flags/azerbaijan.svg" value="AZ">Azerbaijan</option>
														<option data-kt-flag="flags/bahamas.svg" value="BS">Bahamas</option>
														<option data-kt-flag="flags/bahrain.svg" value="BH">Bahrain</option>
														<option data-kt-flag="flags/bangladesh.svg" value="BD">Bangladesh</option>
														<option data-kt-flag="flags/barbados.svg" value="BB">Barbados</option>
														<option data-kt-flag="flags/belarus.svg" value="BY">Belarus</option>
														<option data-kt-flag="flags/belgium.svg" value="BE">Belgium</option>
														<option data-kt-flag="flags/belize.svg" value="BZ">Belize</option>
														<option data-kt-flag="flags/benin.svg" value="BJ">Benin</option>
														<option data-kt-flag="flags/bermuda.svg" value="BM">Bermuda</option>
														<option data-kt-flag="flags/bhutan.svg" value="BT">Bhutan</option>
														<option data-kt-flag="flags/bolivia.svg" value="BO">Bolivia, Plurinational State of</option>
														<option data-kt-flag="flags/bonaire.svg" value="BQ">Bonaire, Sint Eustatius and Saba</option>
														<option data-kt-flag="flags/bosnia-and-herzegovina.svg" value="BA">Bosnia and Herzegovina</option>
														<option data-kt-flag="flags/botswana.svg" value="BW">Botswana</option>
														<option data-kt-flag="flags/brazil.svg" value="BR">Brazil</option>
														<option data-kt-flag="flags/british-indian-ocean-territory.svg" value="IO">British Indian Ocean Territory</option>
														<option data-kt-flag="flags/brunei.svg" value="BN">Brunei Darussalam</option>
														<option data-kt-flag="flags/bulgaria.svg" value="BG">Bulgaria</option>
														<option data-kt-flag="flags/burkina-faso.svg" value="BF">Burkina Faso</option>
														<option data-kt-flag="flags/burundi.svg" value="BI">Burundi</option>
														<option data-kt-flag="flags/cambodia.svg" value="KH">Cambodia</option>
														<option data-kt-flag="flags/cameroon.svg" value="CM">Cameroon</option>
														<option data-kt-flag="flags/canada.svg" value="CA">Canada</option>
														<option data-kt-flag="flags/cape-verde.svg" value="CV">Cape Verde</option>
														<option data-kt-flag="flags/cayman-islands.svg" value="KY">Cayman Islands</option>
														<option data-kt-flag="flags/central-african-republic.svg" value="CF">Central African Republic</option>
														<option data-kt-flag="flags/chad.svg" value="TD">Chad</option>
														<option data-kt-flag="flags/chile.svg" value="CL">Chile</option>
														<option data-kt-flag="flags/china.svg" value="CN">China</option>
														<option data-kt-flag="flags/christmas-island.svg" value="CX">Christmas Island</option>
														<option data-kt-flag="flags/cocos-island.svg" value="CC">Cocos (Keeling) Islands</option>
														<option data-kt-flag="flags/colombia.svg" value="CO">Colombia</option>
														<option data-kt-flag="flags/comoros.svg" value="KM">Comoros</option>
														<option data-kt-flag="flags/cook-islands.svg" value="CK">Cook Islands</option>
														<option data-kt-flag="flags/costa-rica.svg" value="CR">Costa Rica</option>
														<option data-kt-flag="flags/ivory-coast.svg" value="CI">Côte d'Ivoire</option>
														<option data-kt-flag="flags/croatia.svg" value="HR">Croatia</option>
														<option data-kt-flag="flags/cuba.svg" value="CU">Cuba</option>
														<option data-kt-flag="flags/curacao.svg" value="CW">Curaçao</option>
														<option data-kt-flag="flags/czech-republic.svg" value="CZ">Czech Republic</option>
														<option data-kt-flag="flags/denmark.svg" value="DK">Denmark</option>
														<option data-kt-flag="flags/djibouti.svg" value="DJ">Djibouti</option>
														<option data-kt-flag="flags/dominica.svg" value="DM">Dominica</option>
														<option data-kt-flag="flags/dominican-republic.svg" value="DO">Dominican Republic</option>
														<option data-kt-flag="flags/ecuador.svg" value="EC">Ecuador</option>
														<option data-kt-flag="flags/egypt.svg" value="EG">Egypt</option>
														<option data-kt-flag="flags/el-salvador.svg" value="SV">El Salvador</option>
														<option data-kt-flag="flags/equatorial-guinea.svg" value="GQ">Equatorial Guinea</option>
														<option data-kt-flag="flags/eritrea.svg" value="ER">Eritrea</option>
														<option data-kt-flag="flags/estonia.svg" value="EE">Estonia</option>
														<option data-kt-flag="flags/ethiopia.svg" value="ET">Ethiopia</option>
														<option data-kt-flag="flags/falkland-islands.svg" value="FK">Falkland Islands (Malvinas)</option>
														<option data-kt-flag="flags/fiji.svg" value="FJ">Fiji</option>
														<option data-kt-flag="flags/finland.svg" value="FI">Finland</option>
														<option data-kt-flag="flags/france.svg" value="FR">France</option>
														<option data-kt-flag="flags/french-polynesia.svg" value="PF">French Polynesia</option>
														<option data-kt-flag="flags/gabon.svg" value="GA">Gabon</option>
														<option data-kt-flag="flags/gambia.svg" value="GM">Gambia</option>
														<option data-kt-flag="flags/georgia.svg" value="GE">Georgia</option>
														<option data-kt-flag="flags/germany.svg" value="DE">Germany</option>
														<option data-kt-flag="flags/ghana.svg" value="GH">Ghana</option>
														<option data-kt-flag="flags/gibraltar.svg" value="GI">Gibraltar</option>
														<option data-kt-flag="flags/greece.svg" value="GR">Greece</option>
														<option data-kt-flag="flags/greenland.svg" value="GL">Greenland</option>
														<option data-kt-flag="flags/grenada.svg" value="GD">Grenada</option>
														<option data-kt-flag="flags/guam.svg" value="GU">Guam</option>
														<option data-kt-flag="flags/guatemala.svg" value="GT">Guatemala</option>
														<option data-kt-flag="flags/guernsey.svg" value="GG">Guernsey</option>
														<option data-kt-flag="flags/guinea.svg" value="GN">Guinea</option>
														<option data-kt-flag="flags/guinea-bissau.svg" value="GW">Guinea-Bissau</option>
														<option data-kt-flag="flags/haiti.svg" value="HT">Haiti</option>
														<option data-kt-flag="flags/vatican-city.svg" value="VA">Holy See (Vatican City State)</option>
														<option data-kt-flag="flags/honduras.svg" value="HN">Honduras</option>
														<option data-kt-flag="flags/hong-kong.svg" value="HK">Hong Kong</option>
														<option data-kt-flag="flags/hungary.svg" value="HU">Hungary</option>
														<option data-kt-flag="flags/iceland.svg" value="IS">Iceland</option>
														<option data-kt-flag="flags/india.svg" value="IN">India</option>
														<option data-kt-flag="flags/indonesia.svg" value="ID">Indonesia</option>
														<option data-kt-flag="flags/iran.svg" value="IR">Iran, Islamic Republic of</option>
														<option data-kt-flag="flags/iraq.svg" value="IQ">Iraq</option>
														<option data-kt-flag="flags/ireland.svg" value="IE">Ireland</option>
														<option data-kt-flag="flags/isle-of-man.svg" value="IM">Isle of Man</option>
														<option data-kt-flag="flags/israel.svg" value="IL">Israel</option>
														<option data-kt-flag="flags/italy.svg" value="IT">Italy</option>
														<option data-kt-flag="flags/jamaica.svg" value="JM">Jamaica</option>
														<option data-kt-flag="flags/japan.svg" value="JP">Japan</option>
														<option data-kt-flag="flags/jersey.svg" value="JE">Jersey</option>
														<option data-kt-flag="flags/jordan.svg" value="JO">Jordan</option>
														<option data-kt-flag="flags/kazakhstan.svg" value="KZ">Kazakhstan</option>
														<option data-kt-flag="flags/kenya.svg" value="KE">Kenya</option>
														<option data-kt-flag="flags/kiribati.svg" value="KI">Kiribati</option>
														<option data-kt-flag="flags/north-korea.svg" value="KP">Korea, Democratic People's Republic of</option>
														<option data-kt-flag="flags/kuwait.svg" value="KW">Kuwait</option>
														<option data-kt-flag="flags/kyrgyzstan.svg" value="KG">Kyrgyzstan</option>
														<option data-kt-flag="flags/laos.svg" value="LA">Lao People's Democratic Republic</option>
														<option data-kt-flag="flags/latvia.svg" value="LV">Latvia</option>
														<option data-kt-flag="flags/lebanon.svg" value="LB">Lebanon</option>
														<option data-kt-flag="flags/lesotho.svg" value="LS">Lesotho</option>
														<option data-kt-flag="flags/liberia.svg" value="LR">Liberia</option>
														<option data-kt-flag="flags/libya.svg" value="LY">Libya</option>
														<option data-kt-flag="flags/liechtenstein.svg" value="LI">Liechtenstein</option>
														<option data-kt-flag="flags/lithuania.svg" value="LT">Lithuania</option>
														<option data-kt-flag="flags/luxembourg.svg" value="LU">Luxembourg</option>
														<option data-kt-flag="flags/macao.svg" value="MO">Macao</option>
														<option data-kt-flag="flags/madagascar.svg" value="MG">Madagascar</option>
														<option data-kt-flag="flags/malawi.svg" value="MW">Malawi</option>
														<option data-kt-flag="flags/malaysia.svg" value="MY">Malaysia</option>
														<option data-kt-flag="flags/maldives.svg" value="MV">Maldives</option>
														<option data-kt-flag="flags/mali.svg" value="ML">Mali</option>
														<option data-kt-flag="flags/malta.svg" value="MT">Malta</option>
														<option data-kt-flag="flags/marshall-island.svg" value="MH">Marshall Islands</option>
														<option data-kt-flag="flags/martinique.svg" value="MQ">Martinique</option>
														<option data-kt-flag="flags/mauritania.svg" value="MR">Mauritania</option>
														<option data-kt-flag="flags/mauritius.svg" value="MU">Mauritius</option>
														<option data-kt-flag="flags/mexico.svg" value="MX">Mexico</option>
														<option data-kt-flag="flags/micronesia.svg" value="FM">Micronesia, Federated States of</option>
														<option data-kt-flag="flags/moldova.svg" value="MD">Moldova, Republic of</option>
														<option data-kt-flag="flags/monaco.svg" value="MC">Monaco</option>
														<option data-kt-flag="flags/mongolia.svg" value="MN">Mongolia</option>
														<option data-kt-flag="flags/montenegro.svg" value="ME">Montenegro</option>
														<option data-kt-flag="flags/montserrat.svg" value="MS">Montserrat</option>
														<option data-kt-flag="flags/morocco.svg" value="MA">Morocco</option>
														<option data-kt-flag="flags/mozambique.svg" value="MZ">Mozambique</option>
														<option data-kt-flag="flags/myanmar.svg" value="MM">Myanmar</option>
														<option data-kt-flag="flags/namibia.svg" value="NA">Namibia</option>
														<option data-kt-flag="flags/nauru.svg" value="NR">Nauru</option>
														<option data-kt-flag="flags/nepal.svg" value="NP">Nepal</option>
														<option data-kt-flag="flags/netherlands.svg" value="NL">Netherlands</option>
														<option data-kt-flag="flags/new-zealand.svg" value="NZ">New Zealand</option>
														<option data-kt-flag="flags/nicaragua.svg" value="NI">Nicaragua</option>
														<option data-kt-flag="flags/niger.svg" value="NE">Niger</option>
														<option data-kt-flag="flags/nigeria.svg" value="NG">Nigeria</option>
														<option data-kt-flag="flags/niue.svg" value="NU">Niue</option>
														<option data-kt-flag="flags/norfolk-island.svg" value="NF">Norfolk Island</option>
														<option data-kt-flag="flags/northern-mariana-islands.svg" value="MP">Northern Mariana Islands</option>
														<option data-kt-flag="flags/norway.svg" value="NO">Norway</option>
														<option data-kt-flag="flags/oman.svg" value="OM">Oman</option>
														<option data-kt-flag="flags/pakistan.svg" value="PK">Pakistan</option>
														<option data-kt-flag="flags/palau.svg" value="PW">Palau</option>
														<option data-kt-flag="flags/palestine.svg" value="PS">Palestinian Territory, Occupied</option>
														<option data-kt-flag="flags/panama.svg" value="PA">Panama</option>
														<option data-kt-flag="flags/papua-new-guinea.svg" value="PG">Papua New Guinea</option>
														<option data-kt-flag="flags/paraguay.svg" value="PY">Paraguay</option>
														<option data-kt-flag="flags/peru.svg" value="PE">Peru</option>
														<option data-kt-flag="flags/philippines.svg" value="PH">Philippines</option>
														<option data-kt-flag="flags/poland.svg" value="PL">Poland</option>
														<option data-kt-flag="flags/portugal.svg" value="PT">Portugal</option>
														<option data-kt-flag="flags/puerto-rico.svg" value="PR">Puerto Rico</option>
														<option data-kt-flag="flags/qatar.svg" value="QA">Qatar</option>
														<option data-kt-flag="flags/romania.svg" value="RO">Romania</option>
														<option data-kt-flag="flags/russia.svg" value="RU">Russian Federation</option>
														<option data-kt-flag="flags/rwanda.svg" value="RW">Rwanda</option>
														<option data-kt-flag="flags/st-barts.svg" value="BL">Saint Barthélemy</option>
														<option data-kt-flag="flags/saint-kitts-and-nevis.svg" value="KN">Saint Kitts and Nevis</option>
														<option data-kt-flag="flags/st-lucia.svg" value="LC">Saint Lucia</option>
														<option data-kt-flag="flags/sint-maarten.svg" value="MF">Saint Martin (French part)</option>
														<option data-kt-flag="flags/st-vincent-and-the-grenadines.svg" value="VC">Saint Vincent and the Grenadines</option>
														<option data-kt-flag="flags/samoa.svg" value="WS">Samoa</option>
														<option data-kt-flag="flags/san-marino.svg" value="SM">San Marino</option>
														<option data-kt-flag="flags/sao-tome-and-prince.svg" value="ST">Sao Tome and Principe</option>
														<option data-kt-flag="flags/saudi-arabia.svg" value="SA">Saudi Arabia</option>
														<option data-kt-flag="flags/senegal.svg" value="SN">Senegal</option>
														<option data-kt-flag="flags/serbia.svg" value="RS">Serbia</option>
														<option data-kt-flag="flags/seychelles.svg" value="SC">Seychelles</option>
														<option data-kt-flag="flags/sierra-leone.svg" value="SL">Sierra Leone</option>
														<option data-kt-flag="flags/singapore.svg" value="SG">Singapore</option>
														<option data-kt-flag="flags/sint-maarten.svg" value="SX">Sint Maarten (Dutch part)</option>
														<option data-kt-flag="flags/slovakia.svg" value="SK">Slovakia</option>
														<option data-kt-flag="flags/slovenia.svg" value="SI">Slovenia</option>
														<option data-kt-flag="flags/solomon-islands.svg" value="SB">Solomon Islands</option>
														<option data-kt-flag="flags/somalia.svg" value="SO">Somalia</option>
														<option data-kt-flag="flags/south-africa.svg" value="ZA">South Africa</option>
														<option data-kt-flag="flags/south-korea.svg" value="KR">South Korea</option>
														<option data-kt-flag="flags/south-sudan.svg" value="SS">South Sudan</option>
														<option data-kt-flag="flags/spain.svg" value="ES">Spain</option>
														<option data-kt-flag="flags/sri-lanka.svg" value="LK">Sri Lanka</option>
														<option data-kt-flag="flags/sudan.svg" value="SD">Sudan</option>
														<option data-kt-flag="flags/suriname.svg" value="SR">Suriname</option>
														<option data-kt-flag="flags/swaziland.svg" value="SZ">Swaziland</option>
														<option data-kt-flag="flags/sweden.svg" value="SE">Sweden</option>
														<option data-kt-flag="flags/switzerland.svg" value="CH">Switzerland</option>
														<option data-kt-flag="flags/syria.svg" value="SY">Syrian Arab Republic</option>
														<option data-kt-flag="flags/taiwan.svg" value="TW">Taiwan, Province of China</option>
														<option data-kt-flag="flags/tajikistan.svg" value="TJ">Tajikistan</option>
														<option data-kt-flag="flags/tanzania.svg" value="TZ">Tanzania, United Republic of</option>
														<option data-kt-flag="flags/thailand.svg" value="TH">Thailand</option>
														<option data-kt-flag="flags/togo.svg" value="TG">Togo</option>
														<option data-kt-flag="flags/tokelau.svg" value="TK">Tokelau</option>
														<option data-kt-flag="flags/tonga.svg" value="TO">Tonga</option>
														<option data-kt-flag="flags/trinidad-and-tobago.svg" value="TT">Trinidad and Tobago</option>
														<option data-kt-flag="flags/tunisia.svg" value="TN">Tunisia</option>
														<option data-kt-flag="flags/turkey.svg" value="TR">Turkey</option>
														<option data-kt-flag="flags/turkmenistan.svg" value="TM">Turkmenistan</option>
														<option data-kt-flag="flags/turks-and-caicos.svg" value="TC">Turks and Caicos Islands</option>
														<option data-kt-flag="flags/tuvalu.svg" value="TV">Tuvalu</option>
														<option data-kt-flag="flags/uganda.svg" value="UG">Uganda</option>
														<option data-kt-flag="flags/ukraine.svg" value="UA">Ukraine</option>
														<option data-kt-flag="flags/united-arab-emirates.svg" value="AE">United Arab Emirates</option>
														<option data-kt-flag="flags/united-kingdom.svg" value="GB">United Kingdom</option>
														<option data-kt-flag="flags/united-states.svg" value="US">United States</option>
														<option data-kt-flag="flags/uruguay.svg" value="UY">Uruguay</option>
														<option data-kt-flag="flags/uzbekistan.svg" value="UZ">Uzbekistan</option>
														<option data-kt-flag="flags/vanuatu.svg" value="VU">Vanuatu</option>
														<option data-kt-flag="flags/venezuela.svg" value="VE">Venezuela, Bolivarian Republic of</option>
														<option data-kt-flag="flags/vietnam.svg" value="VN">Vietnam</option>
														<option data-kt-flag="flags/virgin-islands.svg" value="VI">Virgin Islands</option>
														<option data-kt-flag="flags/yemen.svg" value="YE">Yemen</option>
														<option data-kt-flag="flags/zambia.svg" value="ZM">Zambia</option>
														<option data-kt-flag="flags/zimbabwe.svg" value="ZW">Zimbabwe</option>
													</select>
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label required fw-bold fs-6">Language</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<!--begin::Input-->
													<select name="language" aria-label="Select a Language" data-control="select2" data-placeholder="Select a language..." class="form-select form-select-solid form-select-lg">
														<option value="">Select a Language...</option>
														<option data-kt-flag="flags/indonesia.svg" value="id">Bahasa Indonesia - Indonesian</option>
														<option data-kt-flag="flags/malaysia.svg" value="msa">Bahasa Melayu - Malay</option>
														<option data-kt-flag="flags/canada.svg" value="ca">Català - Catalan</option>
														<option data-kt-flag="flags/czech-republic.svg" value="cs">Čeština - Czech</option>
														<option data-kt-flag="flags/netherlands.svg" value="da">Dansk - Danish</option>
														<option data-kt-flag="flags/germany.svg" value="de">Deutsch - German</option>
														<option data-kt-flag="flags/united-kingdom.svg" value="en">English</option>
														<option data-kt-flag="flags/united-kingdom.svg" value="en-gb">English UK - British English</option>
														<option data-kt-flag="flags/spain.svg" value="es">Español - Spanish</option>
														<option data-kt-flag="flags/philippines.svg" value="fil">Filipino</option>
														<option data-kt-flag="flags/france.svg" value="fr">Français - French</option>
														<option data-kt-flag="flags/gabon.svg" value="ga">Gaeilge - Irish (beta)</option>
														<option data-kt-flag="flags/greenland.svg" value="gl">Galego - Galician (beta)</option>
														<option data-kt-flag="flags/croatia.svg" value="hr">Hrvatski - Croatian</option>
														<option data-kt-flag="flags/italy.svg" value="it">Italiano - Italian</option>
														<option data-kt-flag="flags/hungary.svg" value="hu">Magyar - Hungarian</option>
														<option data-kt-flag="flags/netherlands.svg" value="nl">Nederlands - Dutch</option>
														<option data-kt-flag="flags/norway.svg" value="no">Norsk - Norwegian</option>
														<option data-kt-flag="flags/poland.svg" value="pl">Polski - Polish</option>
														<option data-kt-flag="flags/portugal.svg" value="pt">Português - Portuguese</option>
														<option data-kt-flag="flags/romania.svg" value="ro">Română - Romanian</option>
														<option data-kt-flag="flags/slovakia.svg" value="sk">Slovenčina - Slovak</option>
														<option data-kt-flag="flags/finland.svg" value="fi">Suomi - Finnish</option>
														<option data-kt-flag="flags/el-salvador.svg" value="sv">Svenska - Swedish</option>
														<option data-kt-flag="flags/virgin-islands.svg" value="vi">Tiếng Việt - Vietnamese</option>
														<option data-kt-flag="flags/turkey.svg" value="tr">Türkçe - Turkish</option>
														<option data-kt-flag="flags/greece.svg" value="el">Ελληνικά - Greek</option>
														<option data-kt-flag="flags/bulgaria.svg" value="bg">Български език - Bulgarian</option>
														<option data-kt-flag="flags/russia.svg" value="ru">Русский - Russian</option>
														<option data-kt-flag="flags/suriname.svg" value="sr">Српски - Serbian</option>
														<option data-kt-flag="flags/ukraine.svg" value="uk">Українська мова - Ukrainian</option>
														<option data-kt-flag="flags/israel.svg" value="he">עִבְרִית - Hebrew</option>
														<option data-kt-flag="flags/pakistan.svg" value="ur">اردو - Urdu (beta)</option>
														<option data-kt-flag="flags/argentina.svg" value="ar">العربية - Arabic</option>
														<option data-kt-flag="flags/argentina.svg" value="fa">فارسی - Persian</option>
														<option data-kt-flag="flags/mauritania.svg" value="mr">मराठी - Marathi</option>
														<option data-kt-flag="flags/india.svg" value="hi">हिन्दी - Hindi</option>
														<option data-kt-flag="flags/bangladesh.svg" value="bn">বাংলা - Bangla</option>
														<option data-kt-flag="flags/guam.svg" value="gu">ગુજરાતી - Gujarati</option>
														<option data-kt-flag="flags/india.svg" value="ta">தமிழ் - Tamil</option>
														<option data-kt-flag="flags/saint-kitts-and-nevis.svg" value="kn">ಕನ್ನಡ - Kannada</option>
														<option data-kt-flag="flags/thailand.svg" value="th">ภาษาไทย - Thai</option>
														<option data-kt-flag="flags/south-korea.svg" value="ko">한국어 - Korean</option>
														<option data-kt-flag="flags/japan.svg" value="ja">日本語 - Japanese</option>
														<option data-kt-flag="flags/china.svg" value="zh-cn">简体中文 - Simplified Chinese</option>
														<option data-kt-flag="flags/taiwan.svg" value="zh-tw">繁體中文 - Traditional Chinese</option>
													</select>
													<!--end::Input-->
													<!--begin::Hint-->
													<div class="form-text">Please select a preferred language, including date, time, and number formatting.</div>
													<!--end::Hint-->
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label required fw-bold fs-6">Time Zone</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<select name="timezone" aria-label="Select a Timezone" data-control="select2" data-placeholder="Select a timezone.." class="form-select form-select-solid form-select-lg">
														<option value="">Select a Timezone..</option>
														<option data-bs-offset="-39600" value="International Date Line West">(GMT-11:00) International Date Line West</option>
														<option data-bs-offset="-39600" value="Midway Island">(GMT-11:00) Midway Island</option>
														<option data-bs-offset="-39600" value="Samoa">(GMT-11:00) Samoa</option>
														<option data-bs-offset="-36000" value="Hawaii">(GMT-10:00) Hawaii</option>
														<option data-bs-offset="-28800" value="Alaska">(GMT-08:00) Alaska</option>
														<option data-bs-offset="-25200" value="Pacific Time (US &amp; Canada)">(GMT-07:00) Pacific Time (US &amp; Canada)</option>
														<option data-bs-offset="-25200" value="Tijuana">(GMT-07:00) Tijuana</option>
														<option data-bs-offset="-25200" value="Arizona">(GMT-07:00) Arizona</option>
														<option data-bs-offset="-21600" value="Mountain Time (US &amp; Canada)">(GMT-06:00) Mountain Time (US &amp; Canada)</option>
														<option data-bs-offset="-21600" value="Chihuahua">(GMT-06:00) Chihuahua</option>
														<option data-bs-offset="-21600" value="Mazatlan">(GMT-06:00) Mazatlan</option>
														<option data-bs-offset="-21600" value="Saskatchewan">(GMT-06:00) Saskatchewan</option>
														<option data-bs-offset="-21600" value="Central America">(GMT-06:00) Central America</option>
														<option data-bs-offset="-18000" value="Central Time (US &amp; Canada)">(GMT-05:00) Central Time (US &amp; Canada)</option>
														<option data-bs-offset="-18000" value="Guadalajara">(GMT-05:00) Guadalajara</option>
														<option data-bs-offset="-18000" value="Mexico City">(GMT-05:00) Mexico City</option>
														<option data-bs-offset="-18000" value="Monterrey">(GMT-05:00) Monterrey</option>
														<option data-bs-offset="-18000" value="Bogota">(GMT-05:00) Bogota</option>
														<option data-bs-offset="-18000" value="Lima">(GMT-05:00) Lima</option>
														<option data-bs-offset="-18000" value="Quito">(GMT-05:00) Quito</option>
														<option data-bs-offset="-14400" value="Eastern Time (US &amp; Canada)">(GMT-04:00) Eastern Time (US &amp; Canada)</option>
														<option data-bs-offset="-14400" value="Indiana (East)">(GMT-04:00) Indiana (East)</option>
														<option data-bs-offset="-14400" value="Caracas">(GMT-04:00) Caracas</option>
														<option data-bs-offset="-14400" value="La Paz">(GMT-04:00) La Paz</option>
														<option data-bs-offset="-14400" value="Georgetown">(GMT-04:00) Georgetown</option>
														<option data-bs-offset="-10800" value="Atlantic Time (Canada)">(GMT-03:00) Atlantic Time (Canada)</option>
														<option data-bs-offset="-10800" value="Santiago">(GMT-03:00) Santiago</option>
														<option data-bs-offset="-10800" value="Brasilia">(GMT-03:00) Brasilia</option>
														<option data-bs-offset="-10800" value="Buenos Aires">(GMT-03:00) Buenos Aires</option>
														<option data-bs-offset="-9000" value="Newfoundland">(GMT-02:30) Newfoundland</option>
														<option data-bs-offset="-7200" value="Greenland">(GMT-02:00) Greenland</option>
														<option data-bs-offset="-7200" value="Mid-Atlantic">(GMT-02:00) Mid-Atlantic</option>
														<option data-bs-offset="-3600" value="Cape Verde Is.">(GMT-01:00) Cape Verde Is.</option>
														<option data-bs-offset="0" value="Azores">(GMT) Azores</option>
														<option data-bs-offset="0" value="Monrovia">(GMT) Monrovia</option>
														<option data-bs-offset="0" value="UTC">(GMT) UTC</option>
														<option data-bs-offset="3600" value="Dublin">(GMT+01:00) Dublin</option>
														<option data-bs-offset="3600" value="Edinburgh">(GMT+01:00) Edinburgh</option>
														<option data-bs-offset="3600" value="Lisbon">(GMT+01:00) Lisbon</option>
														<option data-bs-offset="3600" value="London">(GMT+01:00) London</option>
														<option data-bs-offset="3600" value="Casablanca">(GMT+01:00) Casablanca</option>
														<option data-bs-offset="3600" value="West Central Africa">(GMT+01:00) West Central Africa</option>
														<option data-bs-offset="7200" value="Belgrade">(GMT+02:00) Belgrade</option>
														<option data-bs-offset="7200" value="Bratislava">(GMT+02:00) Bratislava</option>
														<option data-bs-offset="7200" value="Budapest">(GMT+02:00) Budapest</option>
														<option data-bs-offset="7200" value="Ljubljana">(GMT+02:00) Ljubljana</option>
														<option data-bs-offset="7200" value="Prague">(GMT+02:00) Prague</option>
														<option data-bs-offset="7200" value="Sarajevo">(GMT+02:00) Sarajevo</option>
														<option data-bs-offset="7200" value="Skopje">(GMT+02:00) Skopje</option>
														<option data-bs-offset="7200" value="Warsaw">(GMT+02:00) Warsaw</option>
														<option data-bs-offset="7200" value="Zagreb">(GMT+02:00) Zagreb</option>
														<option data-bs-offset="7200" value="Brussels">(GMT+02:00) Brussels</option>
														<option data-bs-offset="7200" value="Copenhagen">(GMT+02:00) Copenhagen</option>
														<option data-bs-offset="7200" value="Madrid">(GMT+02:00) Madrid</option>
														<option data-bs-offset="7200" value="Paris">(GMT+02:00) Paris</option>
														<option data-bs-offset="7200" value="Amsterdam">(GMT+02:00) Amsterdam</option>
														<option data-bs-offset="7200" value="Berlin">(GMT+02:00) Berlin</option>
														<option data-bs-offset="7200" value="Bern">(GMT+02:00) Bern</option>
														<option data-bs-offset="7200" value="Rome">(GMT+02:00) Rome</option>
														<option data-bs-offset="7200" value="Stockholm">(GMT+02:00) Stockholm</option>
														<option data-bs-offset="7200" value="Vienna">(GMT+02:00) Vienna</option>
														<option data-bs-offset="7200" value="Cairo">(GMT+02:00) Cairo</option>
														<option data-bs-offset="7200" value="Harare">(GMT+02:00) Harare</option>
														<option data-bs-offset="7200" value="Pretoria">(GMT+02:00) Pretoria</option>
														<option data-bs-offset="10800" value="Bucharest">(GMT+03:00) Bucharest</option>
														<option data-bs-offset="10800" value="Helsinki">(GMT+03:00) Helsinki</option>
														<option data-bs-offset="10800" value="Kiev">(GMT+03:00) Kiev</option>
														<option data-bs-offset="10800" value="Kyiv">(GMT+03:00) Kyiv</option>
														<option data-bs-offset="10800" value="Riga">(GMT+03:00) Riga</option>
														<option data-bs-offset="10800" value="Sofia">(GMT+03:00) Sofia</option>
														<option data-bs-offset="10800" value="Tallinn">(GMT+03:00) Tallinn</option>
														<option data-bs-offset="10800" value="Vilnius">(GMT+03:00) Vilnius</option>
														<option data-bs-offset="10800" value="Athens">(GMT+03:00) Athens</option>
														<option data-bs-offset="10800" value="Istanbul">(GMT+03:00) Istanbul</option>
														<option data-bs-offset="10800" value="Minsk">(GMT+03:00) Minsk</option>
														<option data-bs-offset="10800" value="Jerusalem">(GMT+03:00) Jerusalem</option>
														<option data-bs-offset="10800" value="Moscow">(GMT+03:00) Moscow</option>
														<option data-bs-offset="10800" value="St. Petersburg">(GMT+03:00) St. Petersburg</option>
														<option data-bs-offset="10800" value="Volgograd">(GMT+03:00) Volgograd</option>
														<option data-bs-offset="10800" value="Kuwait">(GMT+03:00) Kuwait</option>
														<option data-bs-offset="10800" value="Riyadh">(GMT+03:00) Riyadh</option>
														<option data-bs-offset="10800" value="Nairobi">(GMT+03:00) Nairobi</option>
														<option data-bs-offset="10800" value="Baghdad">(GMT+03:00) Baghdad</option>
														<option data-bs-offset="14400" value="Abu Dhabi">(GMT+04:00) Abu Dhabi</option>
														<option data-bs-offset="14400" value="Muscat">(GMT+04:00) Muscat</option>
														<option data-bs-offset="14400" value="Baku">(GMT+04:00) Baku</option>
														<option data-bs-offset="14400" value="Tbilisi">(GMT+04:00) Tbilisi</option>
														<option data-bs-offset="14400" value="Yerevan">(GMT+04:00) Yerevan</option>
														<option data-bs-offset="16200" value="Tehran">(GMT+04:30) Tehran</option>
														<option data-bs-offset="16200" value="Kabul">(GMT+04:30) Kabul</option>
														<option data-bs-offset="18000" value="Ekaterinburg">(GMT+05:00) Ekaterinburg</option>
														<option data-bs-offset="18000" value="Islamabad">(GMT+05:00) Islamabad</option>
														<option data-bs-offset="18000" value="Karachi">(GMT+05:00) Karachi</option>
														<option data-bs-offset="18000" value="Tashkent">(GMT+05:00) Tashkent</option>
														<option data-bs-offset="19800" value="Chennai">(GMT+05:30) Chennai</option>
														<option data-bs-offset="19800" value="Kolkata">(GMT+05:30) Kolkata</option>
														<option data-bs-offset="19800" value="Mumbai">(GMT+05:30) Mumbai</option>
														<option data-bs-offset="19800" value="New Delhi">(GMT+05:30) New Delhi</option>
														<option data-bs-offset="19800" value="Sri Jayawardenepura">(GMT+05:30) Sri Jayawardenepura</option>
														<option data-bs-offset="20700" value="Kathmandu">(GMT+05:45) Kathmandu</option>
														<option data-bs-offset="21600" value="Astana">(GMT+06:00) Astana</option>
														<option data-bs-offset="21600" value="Dhaka">(GMT+06:00) Dhaka</option>
														<option data-bs-offset="21600" value="Almaty">(GMT+06:00) Almaty</option>
														<option data-bs-offset="21600" value="Urumqi">(GMT+06:00) Urumqi</option>
														<option data-bs-offset="23400" value="Rangoon">(GMT+06:30) Rangoon</option>
														<option data-bs-offset="25200" value="Novosibirsk">(GMT+07:00) Novosibirsk</option>
														<option data-bs-offset="25200" value="Bangkok">(GMT+07:00) Bangkok</option>
														<option data-bs-offset="25200" value="Hanoi">(GMT+07:00) Hanoi</option>
														<option data-bs-offset="25200" value="Jakarta">(GMT+07:00) Jakarta</option>
														<option data-bs-offset="25200" value="Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
														<option data-bs-offset="28800" value="Beijing">(GMT+08:00) Beijing</option>
														<option data-bs-offset="28800" value="Chongqing">(GMT+08:00) Chongqing</option>
														<option data-bs-offset="28800" value="Hong Kong">(GMT+08:00) Hong Kong</option>
														<option data-bs-offset="28800" value="Kuala Lumpur">(GMT+08:00) Kuala Lumpur</option>
														<option data-bs-offset="28800" value="Singapore">(GMT+08:00) Singapore</option>
														<option data-bs-offset="28800" value="Taipei">(GMT+08:00) Taipei</option>
														<option data-bs-offset="28800" value="Perth">(GMT+08:00) Perth</option>
														<option data-bs-offset="28800" value="Irkutsk">(GMT+08:00) Irkutsk</option>
														<option data-bs-offset="28800" value="Ulaan Bataar">(GMT+08:00) Ulaan Bataar</option>
														<option data-bs-offset="32400" value="Seoul">(GMT+09:00) Seoul</option>
														<option data-bs-offset="32400" value="Osaka">(GMT+09:00) Osaka</option>
														<option data-bs-offset="32400" value="Sapporo">(GMT+09:00) Sapporo</option>
														<option data-bs-offset="32400" value="Tokyo">(GMT+09:00) Tokyo</option>
														<option data-bs-offset="32400" value="Yakutsk">(GMT+09:00) Yakutsk</option>
														<option data-bs-offset="34200" value="Darwin">(GMT+09:30) Darwin</option>
														<option data-bs-offset="34200" value="Adelaide">(GMT+09:30) Adelaide</option>
														<option data-bs-offset="36000" value="Canberra">(GMT+10:00) Canberra</option>
														<option data-bs-offset="36000" value="Melbourne">(GMT+10:00) Melbourne</option>
														<option data-bs-offset="36000" value="Sydney">(GMT+10:00) Sydney</option>
														<option data-bs-offset="36000" value="Brisbane">(GMT+10:00) Brisbane</option>
														<option data-bs-offset="36000" value="Hobart">(GMT+10:00) Hobart</option>
														<option data-bs-offset="36000" value="Vladivostok">(GMT+10:00) Vladivostok</option>
														<option data-bs-offset="36000" value="Guam">(GMT+10:00) Guam</option>
														<option data-bs-offset="36000" value="Port Moresby">(GMT+10:00) Port Moresby</option>
														<option data-bs-offset="36000" value="Solomon Is.">(GMT+10:00) Solomon Is.</option>
														<option data-bs-offset="39600" value="Magadan">(GMT+11:00) Magadan</option>
														<option data-bs-offset="39600" value="New Caledonia">(GMT+11:00) New Caledonia</option>
														<option data-bs-offset="43200" value="Fiji">(GMT+12:00) Fiji</option>
														<option data-bs-offset="43200" value="Kamchatka">(GMT+12:00) Kamchatka</option>
														<option data-bs-offset="43200" value="Marshall Is.">(GMT+12:00) Marshall Is.</option>
														<option data-bs-offset="43200" value="Auckland">(GMT+12:00) Auckland</option>
														<option data-bs-offset="43200" value="Wellington">(GMT+12:00) Wellington</option>
														<option data-bs-offset="46800" value="Nuku'alofa">(GMT+13:00) Nuku'alofa</option>
													</select>
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label fw-bold fs-6">Currency</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<select name="currnecy" aria-label="Select a Timezone" data-control="select2" data-placeholder="Select a currency.." class="form-select form-select-solid form-select-lg">
														<option value="">Select a currency..</option>
														<option data-kt-flag="flags/united-states.svg" value="USD">
														<b>USD</b>&#160;-&#160;USA dollar</option>
														<option data-kt-flag="flags/united-kingdom.svg" value="GBP">
														<b>GBP</b>&#160;-&#160;British pound</option>
														<option data-kt-flag="flags/australia.svg" value="AUD">
														<b>AUD</b>&#160;-&#160;Australian dollar</option>
														<option data-kt-flag="flags/japan.svg" value="JPY">
														<b>JPY</b>&#160;-&#160;Japanese yen</option>
														<option data-kt-flag="flags/sweden.svg" value="SEK">
														<b>SEK</b>&#160;-&#160;Swedish krona</option>
														<option data-kt-flag="flags/canada.svg" value="CAD">
														<b>CAD</b>&#160;-&#160;Canadian dollar</option>
														<option data-kt-flag="flags/switzerland.svg" value="CHF">
														<b>CHF</b>&#160;-&#160;Swiss franc</option>
													</select>
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-6">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label required fw-bold fs-6">Communication</label>
												<!--end::Label-->
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<!--begin::Options-->
													<div class="d-flex align-items-center mt-3">
														<!--begin::Option-->
														<label class="form-check form-check-inline form-check-solid me-5">
															<input class="form-check-input" name="communication[]" type="checkbox" value="1" />
															<span class="fw-bold ps-2 fs-6">Email</span>
														</label>
														<!--end::Option-->
														<!--begin::Option-->
														<label class="form-check form-check-inline form-check-solid">
															<input class="form-check-input" name="communication[]" type="checkbox" value="2" />
															<span class="fw-bold ps-2 fs-6">Phone</span>
														</label>
														<!--end::Option-->
													</div>
													<!--end::Options-->
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-0">
												<!--begin::Label-->
												<label class="col-lg-4 col-form-label fw-bold fs-6">Allow Marketing</label>
												<!--begin::Label-->
												<!--begin::Label-->
												<div class="col-lg-8 d-flex align-items-center">
													<div class="form-check form-check-solid form-switch fv-row">
														<input class="form-check-input w-45px h-30px" type="checkbox" id="allowmarketing" checked="checked" />
														<label class="form-check-label" for="allowmarketing"></label>
													</div>
												</div>
												<!--begin::Label-->
											</div>
											<!--end::Input group-->
										</div>
										<!--end::Card body-->
										<!--begin::Actions-->
										<div class="card-footer d-flex justify-content-end py-6 px-9">
											<button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
											<button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
										</div>
										<!--end::Actions-->
									</form>
									<!--end::Form-->
								</div>
								<!--end::Content-->
							</div>
							<!--end::Basic info-->
							<!--begin::Connected Accounts-->
							<div class="card mb-5 mb-xl-10">
								<!--begin::Card header-->
								<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_connected_accounts" aria-expanded="true" aria-controls="kt_account_connected_accounts">
									<div class="card-title m-0">
										<h3 class="fw-bolder m-0">Connected Accounts</h3>
									</div>
								</div>
								<!--end::Card header-->
								<!--begin::Content-->
								<div id="kt_account_connected_accounts" class="collapse show">
									<!--begin::Card body-->
									<div class="card-body border-top p-9">
										<!--begin::Notice-->
										<div class="notice d-flex bg-light-primary rounded border-primary border border-dashed mb-9 p-6">
											<!--begin::Icon-->
											<!--begin::Svg Icon | path: icons/duotune/art/art006.svg-->
											<span class="svg-icon svg-icon-2tx svg-icon-primary me-4">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<path opacity="0.3" d="M22 19V17C22 16.4 21.6 16 21 16H8V3C8 2.4 7.6 2 7 2H5C4.4 2 4 2.4 4 3V19C4 19.6 4.4 20 5 20H21C21.6 20 22 19.6 22 19Z" fill="black" />
													<path d="M20 5V21C20 21.6 19.6 22 19 22H17C16.4 22 16 21.6 16 21V8H8V4H19C19.6 4 20 4.4 20 5ZM3 8H4V4H3C2.4 4 2 4.4 2 5V7C2 7.6 2.4 8 3 8Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
											<!--end::Icon-->
											<!--begin::Wrapper-->
											<div class="d-flex flex-stack flex-grow-1">
												<!--begin::Content-->
												<div class="fw-bold">
													<div class="fs-6 text-gray-700">Two-factor authentication adds an extra layer of security to your account. To log in, in you'll need to provide a 4 digit amazing code.
													<a href="#" class="fw-bolder">Learn More</a></div>
												</div>
												<!--end::Content-->
											</div>
											<!--end::Wrapper-->
										</div>
										<!--end::Notice-->
										<!--begin::Items-->
										<div class="py-2">
											<!--begin::Item-->
											<div class="d-flex flex-stack">
												<div class="d-flex">
													<img src="assets/media/svg/brand-logos/google-icon.svg" class="w-30px me-6" alt="" />
													<div class="d-flex flex-column">
														<a href="#" class="fs-5 text-dark text-hover-primary fw-bolder">Google</a>
														<div class="fs-6 fw-bold text-gray-400">Plan properly your workflow</div>
													</div>
												</div>
												<div class="d-flex justify-content-end">
													<div class="form-check form-check-solid form-switch">
														<input class="form-check-input w-45px h-30px" type="checkbox" id="googleswitch" checked="checked" />
														<label class="form-check-label" for="googleswitch"></label>
													</div>
												</div>
											</div>
											<!--end::Item-->
											<div class="separator separator-dashed my-5"></div>
											<!--begin::Item-->
											<div class="d-flex flex-stack">
												<div class="d-flex">
													<img src="assets/media/svg/brand-logos/github.svg" class="w-30px me-6" alt="" />
													<div class="d-flex flex-column">
														<a href="#" class="fs-5 text-dark text-hover-primary fw-bolder">Github</a>
														<div class="fs-6 fw-bold text-gray-400">Keep eye on on your Repositories</div>
													</div>
												</div>
												<div class="d-flex justify-content-end">
													<div class="form-check form-check-solid form-switch">
														<input class="form-check-input w-45px h-30px" type="checkbox" id="githubswitch" checked="checked" />
														<label class="form-check-label" for="githubswitch"></label>
													</div>
												</div>
											</div>
											<!--end::Item-->
											<div class="separator separator-dashed my-5"></div>
											<!--begin::Item-->
											<div class="d-flex flex-stack">
												<div class="d-flex">
													<img src="assets/media/svg/brand-logos/slack-icon.svg" class="w-30px me-6" alt="" />
													<div class="d-flex flex-column">
														<a href="#" class="fs-5 text-dark text-hover-primary fw-bolder">Slack</a>
														<div class="fs-6 fw-bold text-gray-400">Integrate Projects Discussions</div>
													</div>
												</div>
												<div class="d-flex justify-content-end">
													<div class="form-check form-check-solid form-switch">
														<input class="form-check-input w-45px h-30px" type="checkbox" id="slackswitch" />
														<label class="form-check-label" for="slackswitch"></label>
													</div>
												</div>
											</div>
											<!--end::Item-->
										</div>
										<!--end::Items-->
									</div>
									<!--end::Card body-->
									<!--begin::Card footer-->
									<div class="card-footer d-flex justify-content-end py-6 px-9">
										<button class="btn btn-light btn-active-light-primary me-2">Discard</button>
										<button class="btn btn-primary">Save Changes</button>
									</div>
									<!--end::Card footer-->
								</div>
								<!--end::Content-->
							</div>
							<!--end::Connected Accounts-->
							<!--begin::Notifications-->
							<div class="card mb-5 mb-xl-10">
								<!--begin::Card header-->
								<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_email_preferences" aria-expanded="true" aria-controls="kt_account_email_preferences">
									<div class="card-title m-0">
										<h3 class="fw-bolder m-0">Email Preferences</h3>
									</div>
								</div>
								<!--begin::Card header-->
								<!--begin::Content-->
								<div id="kt_account_email_preferences" class="collapse show">
									<!--begin::Form-->
									<form class="form">
										<!--begin::Card body-->
										<div class="card-body border-top px-9 py-9">
											<!--begin::Option-->
											<label class="form-check form-check-custom form-check-solid align-items-start">
												<!--begin::Input-->
												<input class="form-check-input me-3" type="checkbox" name="email-preferences[]" value="1" />
												<!--end::Input-->
												<!--begin::Label-->
												<span class="form-check-label d-flex flex-column align-items-start">
													<span class="fw-bolder fs-5 mb-0">Successful Payments</span>
													<span class="text-muted fs-6">Receive a notification for every successful payment.</span>
												</span>
												<!--end::Label-->
											</label>
											<!--end::Option-->
											<!--begin::Option-->
											<div class="separator separator-dashed my-6"></div>
											<!--end::Option-->
											<!--begin::Option-->
											<label class="form-check form-check-custom form-check-solid align-items-start">
												<!--begin::Input-->
												<input class="form-check-input me-3" type="checkbox" name="email-preferences[]" checked="checked" value="1" />
												<!--end::Input-->
												<!--begin::Label-->
												<span class="form-check-label d-flex flex-column align-items-start">
													<span class="fw-bolder fs-5 mb-0">Payouts</span>
													<span class="text-muted fs-6">Receive a notification for every initiated payout.</span>
												</span>
												<!--end::Label-->
											</label>
											<!--end::Option-->
											<!--begin::Option-->
											<div class="separator separator-dashed my-6"></div>
											<!--end::Option-->
											<!--begin::Option-->
											<label class="form-check form-check-custom form-check-solid align-items-start">
												<!--begin::Input-->
												<input class="form-check-input me-3" type="checkbox" name="email-preferences[]" value="1" />
												<!--end::Input-->
												<!--begin::Label-->
												<span class="form-check-label d-flex flex-column align-items-start">
													<span class="fw-bolder fs-5 mb-0">Fee Collection</span>
													<span class="text-muted fs-6">Receive a notification each time you collect a fee from sales</span>
												</span>
												<!--end::Label-->
											</label>
											<!--end::Option-->
											<!--begin::Option-->
											<div class="separator separator-dashed my-6"></div>
											<!--end::Option-->
											<!--begin::Option-->
											<label class="form-check form-check-custom form-check-solid align-items-start">
												<!--begin::Input-->
												<input class="form-check-input me-3" type="checkbox" name="email-preferences[]" checked="checked" value="1" />
												<!--end::Input-->
												<!--begin::Label-->
												<span class="form-check-label d-flex flex-column align-items-start">
													<span class="fw-bolder fs-5 mb-0">Customer Payment Dispute</span>
													<span class="text-muted fs-6">Receive a notification if a payment is disputed by a customer and for dispute purposes.</span>
												</span>
												<!--end::Label-->
											</label>
											<!--end::Option-->
											<!--begin::Option-->
											<div class="separator separator-dashed my-6"></div>
											<!--end::Option-->
											<!--begin::Option-->
											<label class="form-check form-check-custom form-check-solid align-items-start">
												<!--begin::Input-->
												<input class="form-check-input me-3" type="checkbox" name="email-preferences[]" value="1" />
												<!--end::Input-->
												<!--begin::Label-->
												<span class="form-check-label d-flex flex-column align-items-start">
													<span class="fw-bolder fs-5 mb-0">Refund Alerts</span>
													<span class="text-muted fs-6">Receive a notification if a payment is stated as risk by the Finance Department.</span>
												</span>
												<!--end::Label-->
											</label>
											<!--end::Option-->
											<!--begin::Option-->
											<div class="separator separator-dashed my-6"></div>
											<!--end::Option-->
											<!--begin::Option-->
											<label class="form-check form-check-custom form-check-solid align-items-start">
												<!--begin::Input-->
												<input class="form-check-input me-3" type="checkbox" name="email-preferences[]" checked="checked" value="1" />
												<!--end::Input-->
												<!--begin::Label-->
												<span class="form-check-label d-flex flex-column align-items-start">
													<span class="fw-bolder fs-5 mb-0">Invoice Payments</span>
													<span class="text-muted fs-6">Receive a notification if a customer sends an incorrect amount to pay their invoice.</span>
												</span>
												<!--end::Label-->
											</label>
											<!--end::Option-->
											<!--begin::Option-->
											<div class="separator separator-dashed my-6"></div>
											<!--end::Option-->
											<!--begin::Option-->
											<label class="form-check form-check-custom form-check-solid align-items-start">
												<!--begin::Input-->
												<input class="form-check-input me-3" type="checkbox" name="email-preferences[]" value="1" />
												<!--end::Input-->
												<!--begin::Label-->
												<span class="form-check-label d-flex flex-column align-items-start">
													<span class="fw-bolder fs-5 mb-0">Webhook API Endpoints</span>
													<span class="text-muted fs-6">Receive notifications for consistently failing webhook API endpoints.</span>
												</span>
												<!--end::Label-->
											</label>
											<!--end::Option-->
										</div>
										<!--end::Card body-->
										<!--begin::Card footer-->
										<div class="card-footer d-flex justify-content-end py-6 px-9">
											<button class="btn btn-light btn-active-light-primary me-2">Discard</button>
											<button class="btn btn-primary px-6">Save Changes</button>
										</div>
										<!--end::Card footer-->
									</form>
									<!--end::Form-->
								</div>
								<!--end::Content-->
							</div>
							<!--end::Notifications-->
							<!--begin::Notifications-->
							<div class="card mb-5 mb-xl-10">
								<!--begin::Card header-->
								<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_notifications" aria-expanded="true" aria-controls="kt_account_notifications">
									<div class="card-title m-0">
										<h3 class="fw-bolder m-0">Notifications</h3>
									</div>
								</div>
								<!--begin::Card header-->
								<!--begin::Content-->
								<div id="kt_account_notifications" class="collapse show">
									<!--begin::Form-->
									<form class="form">
										<!--begin::Card body-->
										<div class="card-body border-top px-9 pt-3 pb-4">
											<!--begin::Table-->
											<div class="table-responsive">
												<table class="table table-row-dashed border-gray-300 align-middle gy-6">
													<tbody class="fs-6 fw-bold">
														<!--begin::Table row-->
														<tr>
															<td class="min-w-250px fs-4 fw-bolder">Notifications</td>
															<td class="w-125px">
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="kt_settings_notification_email" checked="checked" data-kt-check="true" data-kt-check-target="[data-kt-settings-notification=email]" />
																	<label class="form-check-label ps-2" for="kt_settings_notification_email">Email</label>
																</div>
															</td>
															<td class="w-125px">
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="kt_settings_notification_phone" checked="checked" data-kt-check="true" data-kt-check-target="[data-kt-settings-notification=phone]" />
																	<label class="form-check-label ps-2" for="kt_settings_notification_phone">Phone</label>
																</div>
															</td>
														</tr>
														<!--begin::Table row-->
														<!--begin::Table row-->
														<tr>
															<td>Billing Updates</td>
															<td>
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="1" id="billing1" checked="checked" data-kt-settings-notification="email" />
																	<label class="form-check-label ps-2" for="billing1"></label>
																</div>
															</td>
															<td>
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="billing2" checked="checked" data-kt-settings-notification="phone" />
																	<label class="form-check-label ps-2" for="billing2"></label>
																</div>
															</td>
														</tr>
														<!--begin::Table row-->
														<!--begin::Table row-->
														<tr>
															<td>New Team Members</td>
															<td>
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="team1" checked="checked" data-kt-settings-notification="email" />
																	<label class="form-check-label ps-2" for="team1"></label>
																</div>
															</td>
															<td>
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="team2" data-kt-settings-notification="phone" />
																	<label class="form-check-label ps-2" for="team2"></label>
																</div>
															</td>
														</tr>
														<!--begin::Table row-->
														<!--begin::Table row-->
														<tr>
															<td>Completed Projects</td>
															<td>
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="project1" data-kt-settings-notification="email" />
																	<label class="form-check-label ps-2" for="project1"></label>
																</div>
															</td>
															<td>
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="project2" checked="checked" data-kt-settings-notification="phone" />
																	<label class="form-check-label ps-2" for="project2"></label>
																</div>
															</td>
														</tr>
														<!--begin::Table row-->
														<!--begin::Table row-->
														<tr>
															<td class="border-bottom-0">Newsletters</td>
															<td class="border-bottom-0">
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="newsletter1" data-kt-settings-notification="email" />
																	<label class="form-check-label ps-2" for="newsletter1"></label>
																</div>
															</td>
															<td class="border-bottom-0">
																<div class="form-check form-check-solid">
																	<input class="form-check-input" type="checkbox" value="" id="newsletter2" data-kt-settings-notification="phone" />
																	<label class="form-check-label ps-2" for="newsletter2"></label>
																</div>
															</td>
														</tr>
														<!--begin::Table row-->
													</tbody>
												</table>
											</div>
											<!--end::Table-->
										</div>
										<!--end::Card body-->
										<!--begin::Card footer-->
										<div class="card-footer d-flex justify-content-end py-6 px-9">
											<button class="btn btn-light btn-active-light-primary me-2">Discard</button>
											<button class="btn btn-primary px-6">Save Changes</button>
										</div>
										<!--end::Card footer-->
									</form>
									<!--end::Form-->
								</div>
								<!--end::Content-->
							</div>
							<!--end::Notifications-->

							<!--begin::Modals-->
							<!--begin::Modal - Two-factor authentication-->
							<div class="modal fade" id="kt_modal_two_factor_authentication" tabindex="-1" aria-hidden="true">
								<!--begin::Modal header-->
								<div class="modal-dialog modal-dialog-centered mw-650px">
									<!--begin::Modal content-->
									<div class="modal-content">
										<!--begin::Modal header-->
										<div class="modal-header flex-stack">
											<!--begin::Title-->
											<h2>Choose An Authentication Method</h2>
											<!--end::Title-->
											<!--begin::Close-->
											<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
												<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
												<span class="svg-icon svg-icon-1">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
														<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
											</div>
											<!--end::Close-->
										</div>
										<!--begin::Modal header-->
										<!--begin::Modal body-->
										<div class="modal-body scroll-y pt-10 pb-15 px-lg-17">
											<!--begin::Options-->
											<div data-kt-element="options">
												<!--begin::Notice-->
												<p class="text-muted fs-5 fw-bold mb-10">In addition to your username and password, you’ll have to enter a code (delivered via app or SMS) to log into your account.</p>
												<!--end::Notice-->
												<!--begin::Wrapper-->
												<div class="pb-10">
													<!--begin::Option-->
													<input type="radio" class="btn-check" name="auth_option" value="apps" checked="checked" id="kt_modal_two_factor_authentication_option_1" />
													<label class="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-5" for="kt_modal_two_factor_authentication_option_1">
														<!--begin::Svg Icon | path: icons/duotune/coding/cod001.svg-->
														<span class="svg-icon svg-icon-4x me-4">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<path opacity="0.3" d="M22.1 11.5V12.6C22.1 13.2 21.7 13.6 21.2 13.7L19.9 13.9C19.7 14.7 19.4 15.5 18.9 16.2L19.7 17.2999C20 17.6999 20 18.3999 19.6 18.7999L18.8 19.6C18.4 20 17.8 20 17.3 19.7L16.2 18.9C15.5 19.3 14.7 19.7 13.9 19.9L13.7 21.2C13.6 21.7 13.1 22.1 12.6 22.1H11.5C10.9 22.1 10.5 21.7 10.4 21.2L10.2 19.9C9.4 19.7 8.6 19.4 7.9 18.9L6.8 19.7C6.4 20 5.7 20 5.3 19.6L4.5 18.7999C4.1 18.3999 4.1 17.7999 4.4 17.2999L5.2 16.2C4.8 15.5 4.4 14.7 4.2 13.9L2.9 13.7C2.4 13.6 2 13.1 2 12.6V11.5C2 10.9 2.4 10.5 2.9 10.4L4.2 10.2C4.4 9.39995 4.7 8.60002 5.2 7.90002L4.4 6.79993C4.1 6.39993 4.1 5.69993 4.5 5.29993L5.3 4.5C5.7 4.1 6.3 4.10002 6.8 4.40002L7.9 5.19995C8.6 4.79995 9.4 4.39995 10.2 4.19995L10.4 2.90002C10.5 2.40002 11 2 11.5 2H12.6C13.2 2 13.6 2.40002 13.7 2.90002L13.9 4.19995C14.7 4.39995 15.5 4.69995 16.2 5.19995L17.3 4.40002C17.7 4.10002 18.4 4.1 18.8 4.5L19.6 5.29993C20 5.69993 20 6.29993 19.7 6.79993L18.9 7.90002C19.3 8.60002 19.7 9.39995 19.9 10.2L21.2 10.4C21.7 10.5 22.1 11 22.1 11.5ZM12.1 8.59998C10.2 8.59998 8.6 10.2 8.6 12.1C8.6 14 10.2 15.6 12.1 15.6C14 15.6 15.6 14 15.6 12.1C15.6 10.2 14 8.59998 12.1 8.59998Z" fill="black" />
																<path d="M17.1 12.1C17.1 14.9 14.9 17.1 12.1 17.1C9.30001 17.1 7.10001 14.9 7.10001 12.1C7.10001 9.29998 9.30001 7.09998 12.1 7.09998C14.9 7.09998 17.1 9.29998 17.1 12.1ZM12.1 10.1C11 10.1 10.1 11 10.1 12.1C10.1 13.2 11 14.1 12.1 14.1C13.2 14.1 14.1 13.2 14.1 12.1C14.1 11 13.2 10.1 12.1 10.1Z" fill="black" />
															</svg>
														</span>
														<!--end::Svg Icon-->
														<span class="d-block fw-bold text-start">
															<span class="text-dark fw-bolder d-block fs-3">Authenticator Apps</span>
															<span class="text-muted fw-bold fs-6">Get codes from an app like Google Authenticator, Microsoft Authenticator, Authy or 1Password.</span>
														</span>
													</label>
													<!--end::Option-->
													<!--begin::Option-->
													<input type="radio" class="btn-check" name="auth_option" value="sms" id="kt_modal_two_factor_authentication_option_2" />
													<label class="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center" for="kt_modal_two_factor_authentication_option_2">
														<!--begin::Svg Icon | path: icons/duotune/communication/com003.svg-->
														<span class="svg-icon svg-icon-4x me-4">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<path opacity="0.3" d="M2 4V16C2 16.6 2.4 17 3 17H13L16.6 20.6C17.1 21.1 18 20.8 18 20V17H21C21.6 17 22 16.6 22 16V4C22 3.4 21.6 3 21 3H3C2.4 3 2 3.4 2 4Z" fill="black" />
																<path d="M18 9H6C5.4 9 5 8.6 5 8C5 7.4 5.4 7 6 7H18C18.6 7 19 7.4 19 8C19 8.6 18.6 9 18 9ZM16 12C16 11.4 15.6 11 15 11H6C5.4 11 5 11.4 5 12C5 12.6 5.4 13 6 13H15C15.6 13 16 12.6 16 12Z" fill="black" />
															</svg>
														</span>
														<!--end::Svg Icon-->
														<span class="d-block fw-bold text-start">
															<span class="text-dark fw-bolder d-block fs-3">SMS</span>
															<span class="text-muted fw-bold fs-6">We will send a code via SMS if you need to use your backup login method.</span>
														</span>
													</label>
													<!--end::Option-->
												</div>
												<!--end::Options-->
												<!--begin::Action-->
												<button class="btn btn-primary w-100" data-kt-element="options-select">Continue</button>
												<!--end::Action-->
											</div>
											<!--end::Options-->
											<!--begin::Apps-->
											<div class="d-none" data-kt-element="apps">
												<!--begin::Heading-->
												<h3 class="text-dark fw-bolder mb-7">Authenticator Apps</h3>
												<!--end::Heading-->
												<!--begin::Description-->
												<div class="text-gray-500 fw-bold fs-6 mb-10">Using an authenticator app like
												<a href="https://support.google.com/accounts/answer/1066447?hl=en" target="_blank">Google Authenticator</a>,
												<a href="https://www.microsoft.com/en-us/account/authenticator" target="_blank">Microsoft Authenticator</a>,
												<a href="https://authy.com/download/" target="_blank">Authy</a>, or
												<a href="https://support.1password.com/one-time-passwords/" target="_blank">1Password</a>, scan the QR code. It will generate a 6 digit code for you to enter below.
												<!--begin::QR code image-->
												<div class="pt-5 text-center">
													<img src="assets/media/misc/qr.png" alt="" class="mw-150px" />
												</div>
												<!--end::QR code image--></div>
												<!--end::Description-->
												<!--begin::Notice-->
												<div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-10 p-6">
													<!--begin::Icon-->
													<!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
													<span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
															<rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
															<rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
														</svg>
													</span>
													<!--end::Svg Icon-->
													<!--end::Icon-->
													<!--begin::Wrapper-->
													<div class="d-flex flex-stack flex-grow-1">
														<!--begin::Content-->
														<div class="fw-bold">
															<div class="fs-6 text-gray-700">If you having trouble using the QR code, select manual entry on your app, and enter your username and the code:
															<div class="fw-bolder text-dark pt-2">KBSS3QDAAFUMCBY63YCKI5WSSVACUMPN</div></div>
														</div>
														<!--end::Content-->
													</div>
													<!--end::Wrapper-->
												</div>
												<!--end::Notice-->
												<!--begin::Form-->
												<form data-kt-element="apps-form" class="form" action="#">
													<!--begin::Input group-->
													<div class="mb-10 fv-row">
														<input type="text" class="form-control form-control-lg form-control-solid" placeholder="Enter authentication code" name="code" />
													</div>
													<!--end::Input group-->
													<!--begin::Actions-->
													<div class="d-flex flex-center">
														<button type="reset" data-kt-element="apps-cancel" class="btn btn-light me-3">Cancel</button>
														<button type="submit" data-kt-element="apps-submit" class="btn btn-primary">
															<span class="indicator-label">Submit</span>
															<span class="indicator-progress">Please wait...
															<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
														</button>
													</div>
													<!--end::Actions-->
												</form>
												<!--end::Form-->
											</div>
											<!--end::Options-->
											<!--begin::SMS-->
											<div class="d-none" data-kt-element="sms">
												<!--begin::Heading-->
												<h3 class="text-dark fw-bolder fs-3 mb-5">SMS: Verify Your Mobile Number</h3>
												<!--end::Heading-->
												<!--begin::Notice-->
												<div class="text-muted fw-bold mb-10">Enter your mobile phone number with country code and we will send you a verification code upon request.</div>
												<!--end::Notice-->
												<!--begin::Form-->
												<form data-kt-element="sms-form" class="form" action="#">
													<!--begin::Input group-->
													<div class="mb-10 fv-row">
														<input type="text" class="form-control form-control-lg form-control-solid" placeholder="Mobile number with country code..." name="mobile" />
													</div>
													<!--end::Input group-->
													<!--begin::Actions-->
													<div class="d-flex flex-center">
														<button type="reset" data-kt-element="sms-cancel" class="btn btn-light me-3">Cancel</button>
														<button type="submit" data-kt-element="sms-submit" class="btn btn-primary">
															<span class="indicator-label">Submit</span>
															<span class="indicator-progress">Please wait...
															<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
														</button>
													</div>
													<!--end::Actions-->
												</form>
												<!--end::Form-->
											</div>
											<!--end::SMS-->
										</div>
										<!--begin::Modal body-->
									</div>
									<!--end::Modal content-->
								</div>
								<!--end::Modal header-->
							</div>
							<!--end::Modal - Two-factor authentication-->
							<!--end::Modals-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted fw-bold me-1">2021©</span>
								<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Menu-->
							<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
								<li class="menu-item">
									<a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
								</li>
								<li class="menu-item">
									<a href="https://keenthemes.com/support" target="_blank" class="menu-link px-2">Support</a>
								</li>
								<li class="menu-item">
									<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
								</li>
							</ul>
							<!--end::Menu-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		{{ theme()->getView('pages/header_right') }}
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<!--end::Main-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/pages/projects/project/project.js"></script>
		<script src="assets/js/custom/modals/users-search.js"></script>
		<script src="assets/js/custom/modals/new-target.js"></script>
		<script src="assets/js/custom/widgets.js"></script>
		<script src="assets/js/custom/apps/chat/chat.js"></script>
		<script src="assets/js/custom/modals/create-app.js"></script>
		<script src="assets/js/custom/modals/upgrade-plan.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>