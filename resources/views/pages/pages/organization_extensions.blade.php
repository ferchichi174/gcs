<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="../../">
		<title>GoCC</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendor Stylesheets(used by this page)-->
		<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				{{ theme()->getView('pages/aside') }}
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					{{ theme()->getView('pages/header') }}
					<!--end::Header-->
					<!--begin::Toolbar-->
					<div class="toolbar py-3" id="kt_toolbar">
						<!--begin::Container-->
						<div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
							<!--begin::Page title-->
							<div class="flex-grow-1 flex-shrink-0 me-5">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Organization</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-3"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="../../demo6/dist/index.html" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Organization</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
							</div>
							<!--end::Page title-->
							<!--begin::Action group-->
							<div class="d-flex align-items-center flex-wrap">
								
								<!--begin::Wrapper-->
								<div class="d-flex align-items-center">
									
									<!--begin::Actions-->
									<div class="d-flex align-items-center">
										<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light btn-active-color-primary">
											<!--begin::Svg Icon | path: icons/duotune/files/fil005.svg-->
											<span class="svg-icon svg-icon-2x">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13H13V10C13 9.4 12.6 9 12 9C11.4 9 11 9.4 11 10V13H8C7.4 13 7 13.4 7 14C7 14.6 7.4 15 8 15H11V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18V15H16C16.6 15 17 14.6 17 14C17 13.4 16.6 13 16 13Z" fill="black" />
													<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
										</button>
									</div>
									<!--end::Actions-->
								</div>
								<!--end::Wrapper-->
							</div>
							<!--end::Action group-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Toolbar-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid pt-3" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-fluid">
							<!--begin::Navbar-->
						<div class="card mb-6 mb-xl-9">
							<div class="card-body p-0">
								<!--begin::Nav wrapper-->
								<div class="d-flex overflow-auto h-55px">
									<!--begin::Nav links-->
									<ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/countries">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/maps/map004.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M18.4 5.59998C21.9 9.09998 21.9 14.8 18.4 18.3C14.9 21.8 9.2 21.8 5.7 18.3L18.4 5.59998Z" fill="black" />
														<path d="M12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2ZM19.9 11H13V8.8999C14.9 8.6999 16.7 8.00005 18.1 6.80005C19.1 8.00005 19.7 9.4 19.9 11ZM11 19.8999C9.7 19.6999 8.39999 19.2 7.39999 18.5C8.49999 17.7 9.7 17.2001 11 17.1001V19.8999ZM5.89999 6.90002C7.39999 8.10002 9.2 8.8 11 9V11.1001H4.10001C4.30001 9.4001 4.89999 8.00002 5.89999 6.90002ZM7.39999 5.5C8.49999 4.7 9.7 4.19998 11 4.09998V7C9.7 6.8 8.39999 6.3 7.39999 5.5ZM13 17.1001C14.3 17.3001 15.6 17.8 16.6 18.5C15.5 19.3 14.3 19.7999 13 19.8999V17.1001ZM13 4.09998C14.3 4.29998 15.6 4.8 16.6 5.5C15.5 6.3 14.3 6.80002 13 6.90002V4.09998ZM4.10001 13H11V15.1001C9.1 15.3001 7.29999 16 5.89999 17.2C4.89999 16 4.30001 14.6 4.10001 13ZM18.1 17.1001C16.6 15.9001 14.8 15.2 13 15V12.8999H19.9C19.7 14.5999 19.1 16.0001 18.1 17.1001Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Countries scope</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/communications">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/electronics/elc007.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M12.4409 22C13.5455 22 14.4409 21.1046 14.4409 20C14.4409 18.8954 13.5455 18 12.4409 18C11.3364 18 10.4409 18.8954 10.4409 20C10.4409 21.1046 11.3364 22 12.4409 22Z" fill="black" />
														<path opacity="0.3" d="M9.04095 14.8L9.94095 16.1C10.6409 15.6 11.5409 15.3 12.4409 15.3C13.3409 15.3 14.2409 15.6 14.9409 16.1L15.8409 14.8C16.1409 14.3 16.0409 13.6 15.4409 13.4C14.5409 13 13.5409 12.7 12.4409 12.7C11.3409 12.7 10.3409 12.9 9.44095 13.4C8.84095 13.6 8.74095 14.3 9.04095 14.8Z" fill="black" />
														<path opacity="0.3" d="M3.14096 5.80005L4.04095 7.19995C6.44095 5.59995 9.34094 4.69995 12.4409 4.69995C15.5409 4.69995 18.4409 5.59995 20.8409 7.19995L21.7409 5.80005C22.0409 5.30005 21.8409 4.70002 21.3409 4.40002C18.7409 2.90002 15.6409 2 12.4409 2C9.24094 2 6.14095 2.90002 3.54095 4.40002C3.04095 4.70002 2.84096 5.30005 3.14096 5.80005Z" fill="black" />
														<path opacity="0.3" d="M6.14097 10.3L7.04096 11.7C8.64096 10.7 10.441 10.1 12.541 10.1C14.641 10.1 16.441 10.7 18.041 11.7L18.941 10.3C19.241 9.80005 19.141 9.10002 18.541 8.90002C16.741 7.90002 14.741 7.40002 12.541 7.40002C10.341 7.40002 8.34096 7.90002 6.54096 8.90002C5.94096 9.10002 5.74097 9.80005 6.14097 10.3Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Communications</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/integrations">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/files/fil021.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="black" />
														<path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="black" />
														<path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Integrations</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/reports">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/graphs/gra010.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M13.0021 10.9128V3.01281C13.0021 2.41281 13.5021 1.91281 14.1021 2.01281C16.1021 2.21281 17.9021 3.11284 19.3021 4.61284C20.7021 6.01284 21.6021 7.91285 21.9021 9.81285C22.0021 10.4129 21.5021 10.9128 20.9021 10.9128H13.0021Z" fill="black" />
														<path opacity="0.3" d="M11.0021 13.7128V4.91283C11.0021 4.31283 10.5021 3.81283 9.90208 3.91283C5.40208 4.51283 1.90209 8.41284 2.00209 13.1128C2.10209 18.0128 6.40208 22.0128 11.3021 21.9128C13.1021 21.8128 14.7021 21.3128 16.0021 20.4128C16.5021 20.1128 16.6021 19.3128 16.1021 18.9128L11.0021 13.7128Z" fill="black" />
														<path opacity="0.3" d="M21.9021 14.0128C21.7021 15.6128 21.1021 17.1128 20.1021 18.4128C19.7021 18.9128 19.0021 18.9128 18.6021 18.5128L13.0021 12.9128H20.9021C21.5021 12.9128 22.0021 13.4128 21.9021 14.0128Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Reports displaying</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/landing_pages">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/electronics/elc004.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M2 16C2 16.6 2.4 17 3 17H21C21.6 17 22 16.6 22 16V15H2V16Z" fill="black" />
														<path opacity="0.3" d="M21 3H3C2.4 3 2 3.4 2 4V15H22V4C22 3.4 21.6 3 21 3Z" fill="black" />
														<path opacity="0.3" d="M15 17H9V20H15V17Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Landing pages</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/teams">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com005.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z" fill="black" />
														<path opacity="0.3" d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Teams</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/roles">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/abstract/abs027.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black" />
														<path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Roles</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link active btn btn-flex btn-active-light-primary m-0" href="public/organization/extensions">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/technology/teh008.svg-->
												<span class="svg-icon svg-icon-primary svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path opacity="0.3" d="M11 6.5C11 9 9 11 6.5 11C4 11 2 9 2 6.5C2 4 4 2 6.5 2C9 2 11 4 11 6.5ZM17.5 2C15 2 13 4 13 6.5C13 9 15 11 17.5 11C20 11 22 9 22 6.5C22 4 20 2 17.5 2ZM6.5 13C4 13 2 15 2 17.5C2 20 4 22 6.5 22C9 22 11 20 11 17.5C11 15 9 13 6.5 13ZM17.5 13C15 13 13 15 13 17.5C13 20 15 22 17.5 22C20 22 22 20 22 17.5C22 15 20 13 17.5 13Z" fill="black" />
														<path d="M17.5 16C17.5 16 17.4 16 17.5 16L16.7 15.3C16.1 14.7 15.7 13.9 15.6 13.1C15.5 12.4 15.5 11.6 15.6 10.8C15.7 9.99999 16.1 9.19998 16.7 8.59998L17.4 7.90002H17.5C18.3 7.90002 19 7.20002 19 6.40002C19 5.60002 18.3 4.90002 17.5 4.90002C16.7 4.90002 16 5.60002 16 6.40002V6.5L15.3 7.20001C14.7 7.80001 13.9 8.19999 13.1 8.29999C12.4 8.39999 11.6 8.39999 10.8 8.29999C9.99999 8.19999 9.20001 7.80001 8.60001 7.20001L7.89999 6.5V6.40002C7.89999 5.60002 7.19999 4.90002 6.39999 4.90002C5.59999 4.90002 4.89999 5.60002 4.89999 6.40002C4.89999 7.20002 5.59999 7.90002 6.39999 7.90002H6.5L7.20001 8.59998C7.80001 9.19998 8.19999 9.99999 8.29999 10.8C8.39999 11.5 8.39999 12.3 8.29999 13.1C8.19999 13.9 7.80001 14.7 7.20001 15.3L6.5 16H6.39999C5.59999 16 4.89999 16.7 4.89999 17.5C4.89999 18.3 5.59999 19 6.39999 19C7.19999 19 7.89999 18.3 7.89999 17.5V17.4L8.60001 16.7C9.20001 16.1 9.99999 15.7 10.8 15.6C11.5 15.5 12.3 15.5 13.1 15.6C13.9 15.7 14.7 16.1 15.3 16.7L16 17.4V17.5C16 18.3 16.7 19 17.5 19C18.3 19 19 18.3 19 17.5C19 16.7 18.3 16 17.5 16Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4 fw-bolder">Extensions</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
										<!--begin::Nav item-->
										<li class="nav-item">
											<a class="nav-link btn btn-flex m-0" href="public/organization/users">
												<!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com014.svg-->
												<span class="svg-icon svg-icon-2 me-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="black" />
														<rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="black" />
														<path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="black" />
														<rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
												<span class="d-flex flex-column align-items-start">
													<span class="fs-4">Users</span>
												</span>
											</a>
										</li>
										<!--end::Nav item-->
									</ul>
									<!--end::Nav links-->
								</div>
								<!--end::Nav wrapper-->
							</div>
						</div>
						<!--end::Navbar-->
							<!--begin::Table-->
							<div class="card mt-6 mt-xl-9">
								<!--begin::Card header-->
								<div class="card-header m-0 pt-4 pb-4 bg-lighten">
									<!--begin::Card title-->
									<div class="card-title flex-column">
										<h3 class="fw-bolder mb-1">Extensions list</h3>
										<div class="fs-6 text-gray-400">List extensions of the call services</div>
									</div>
									<!--begin::Card title-->
									<!--begin::Card toolbar-->
									<div class="card-toolbar my-1">
									    <a href="../../demo1/dist/account/settings.html" class="btn btn-primary btn-sm align-self-center me-4">Add</a>
                                        <!--begin::Tab nav-->
									<ul class="nav nav-pills me-6 mb-2 mb-sm-0">
										<li class="nav-item m-0">
											<a class="btn btn-sm btn-icon btn-light btn-color-muted btn-active-primary me-3 active" data-bs-toggle="tab" href="#kt_project_users_card_pane">
												<!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
												<span class="svg-icon svg-icon-2">
													<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
															<rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
															<rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
															<rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
														</g>
													</svg>
												</span>
												<!--end::Svg Icon-->
											</a>
										</li>
										<li class="nav-item m-0">
											<a class="btn btn-sm btn-icon btn-light btn-color-muted btn-active-primary" data-bs-toggle="tab" href="#kt_project_users_table_pane">
												<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
												<span class="svg-icon svg-icon-2">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
														<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
											</a>
										</li>
									</ul>
									<!--end::Tab nav-->
                                        <!--begin::Select-->
										<div class="me-4 my-1">
											<select id="kt_filter_orders" name="orders" data-control="select2" data-hide-search="true" class="w-125px form-select form-select-solid form-select-sm">
												<option value="All" selected="selected">All</option>
												<option value="Approved">Approved</option>
												<option value="Declined">Declined</option>
												<option value="In Progress">In Progress</option>
												<option value="In Transit">In Transit</option>
											</select>
										</div>
										<!--end::Select-->
										<!--begin::Search-->
										<div class="d-flex align-items-center position-relative my-1">
											<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
											<span class="svg-icon svg-icon-3 position-absolute ms-3">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
													<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
											<input type="text" id="kt_filter_search" class="form-control form-control-solid form-select-sm w-150px ps-9" placeholder="Search" />
										</div>
										<!--end::Search-->
									</div>
									<!--begin::Card toolbar-->
								</div>
								<!--end::Card header-->
								<!--begin::Card body-->
								<div class="card-body pt-0">
									
							<!--begin::Tab Content-->
							<div class="tab-content">
								<!--begin::Tab pane-->
								<div id="kt_project_users_card_pane" class="tab-pane fade show active">
									<!--begin::Row-->
									<div class="row g-6 g-xl-9">
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">1017</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">CALL IN.</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">1068</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">CALL IN</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 col-xxl-4">
											<!--begin::Card-->
											<div class="card">
												<!--begin::Card body-->
												<div class="card-body d-flex flex-center flex-column pt-12 p-9">
													
													<!--begin::Name-->
													<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">4562456245627</a>
													<!--end::Name-->
													<!--begin::Position-->
													<div class="fw-bold text-gray-400 mb-6">CALL OUT</div>
													<!--end::Position-->
													<!--begin::Info-->
													<div class="d-flex flex-center flex-wrap">
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
															<div class="fw-bold text-gray-400">Earnings</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">23</div>
															<div class="fw-bold text-gray-400">Tasks</div>
														</div>
														<!--end::Stats-->
														<!--begin::Stats-->
														<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
															<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
															<div class="fw-bold text-gray-400">Sales</div>
														</div>
														<!--end::Stats-->
													</div>
													<!--end::Info-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
										</div>
										<!--end::Col-->
									</div>
									<!--end::Row-->
								</div>
								<!--end::Tab pane-->
								<!--begin::Tab pane-->
								<div id="kt_project_users_table_pane" class="tab-pane fade">
									<!--begin::Card-->
									<div class="card card-flush">
										<!--begin::Card body-->
										<div class="card-body pt-0">
											<!--begin::Table container-->
											<div class="table-responsive">
												<!--begin::Table-->
												<table id="kt_project_users_table" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bolder">
													<!--begin::Head-->
													<thead class="fs-7 text-gray-400 text-uppercase">
														<tr>
															<th class="min-w-250px">Manager</th>
															<th class="min-w-150px">Date</th>
															<th class="min-w-90px">Amount</th>
															<th class="min-w-90px">Status</th>
															<th class="min-w-50px text-end">Details</th>
														</tr>
													</thead>
													<!--end::Head-->
													<!--begin::Body-->
													<tbody class="fs-6">
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-1.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">e.smith@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 20, 2021</td>
															<td>$784.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">M</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Melody Macy</a>
																		<div class="fw-bold fs-6 text-gray-400">melody@altbox.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Dec 20, 2021</td>
															<td>$810.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-26.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Max Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">max@kt.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 24, 2021</td>
															<td>$582.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-4.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Sean Bean</a>
																		<div class="fw-bold fs-6 text-gray-400">sean@dellito.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Sep 22, 2021</td>
															<td>$867.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-15.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Brian Cox</a>
																		<div class="fw-bold fs-6 text-gray-400">brian@exchange.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 24, 2021</td>
															<td>$449.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-warning text-warning fw-bold">M</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Mikaela Collins</a>
																		<div class="fw-bold fs-6 text-gray-400">mikaela@pexcom.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Dec 20, 2021</td>
															<td>$816.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-8.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Francis Mitcham</a>
																		<div class="fw-bold fs-6 text-gray-400">f.mitcham@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Feb 21, 2021</td>
															<td>$630.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">O</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Olivia Wild</a>
																		<div class="fw-bold fs-6 text-gray-400">olivia@corpmail.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 24, 2021</td>
															<td>$772.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-primary text-primary fw-bold">N</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Neil Owen</a>
																		<div class="fw-bold fs-6 text-gray-400">owen.neil@gmail.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Nov 10, 2021</td>
															<td>$562.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-6.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Dan Wilson</a>
																		<div class="fw-bold fs-6 text-gray-400">dam@consilting.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Mar 10, 2021</td>
															<td>$796.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">E</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Bold</a>
																		<div class="fw-bold fs-6 text-gray-400">emma@intenso.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Aug 19, 2021</td>
															<td>$605.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-7.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Ana Crown</a>
																		<div class="fw-bold fs-6 text-gray-400">ana.cf@limtel.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$601.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-info text-info fw-bold">A</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Robert Doe</a>
																		<div class="fw-bold fs-6 text-gray-400">robert@benko.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Dec 20, 2021</td>
															<td>$984.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-17.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">John Miller</a>
																		<div class="fw-bold fs-6 text-gray-400">miller@mapple.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Sep 22, 2021</td>
															<td>$544.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-success text-success fw-bold">L</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Lucy Kunic</a>
																		<div class="fw-bold fs-6 text-gray-400">lucy.m@fentech.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$649.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-10.jpg" />
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Ethan Wilder</a>
																		<div class="fw-bold fs-6 text-gray-400">ethan@loop.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Dec 20, 2021</td>
															<td>$894.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-success text-success fw-bold">L</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Lucy Kunic</a>
																		<div class="fw-bold fs-6 text-gray-400">lucy.m@fentech.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Sep 22, 2021</td>
															<td>$767.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-8.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Francis Mitcham</a>
																		<div class="fw-bold fs-6 text-gray-400">f.mitcham@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jul 25, 2021</td>
															<td>$717.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-1.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">e.smith@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Aug 19, 2021</td>
															<td>$483.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-26.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Max Smith</a>
																		<div class="fw-bold fs-6 text-gray-400">max@kt.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Dec 20, 2021</td>
															<td>$802.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-6.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Dan Wilson</a>
																		<div class="fw-bold fs-6 text-gray-400">dam@consilting.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$609.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">O</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Olivia Wild</a>
																		<div class="fw-bold fs-6 text-gray-400">olivia@corpmail.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Dec 20, 2021</td>
															<td>$826.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">M</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Melody Macy</a>
																		<div class="fw-bold fs-6 text-gray-400">melody@altbox.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$436.00</td>
															<td>
																<span class="badge badge-light-danger fw-bolder px-4 py-3">Rejected</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-17.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">John Miller</a>
																		<div class="fw-bold fs-6 text-gray-400">miller@mapple.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jul 25, 2021</td>
															<td>$635.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-8.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Francis Mitcham</a>
																		<div class="fw-bold fs-6 text-gray-400">f.mitcham@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Mar 10, 2021</td>
															<td>$805.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-success text-success fw-bold">L</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Lucy Kunic</a>
																		<div class="fw-bold fs-6 text-gray-400">lucy.m@fentech.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Sep 22, 2021</td>
															<td>$462.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">E</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Emma Bold</a>
																		<div class="fw-bold fs-6 text-gray-400">emma@intenso.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Jun 20, 2021</td>
															<td>$920.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<span class="symbol-label bg-light-danger text-danger fw-bold">O</span>
																		</div>
																		<!--end::Avatar-->
																		<!--begin::Online-->
																		<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n1 mt-n1"></div>
																		<!--end::Online-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Olivia Wild</a>
																		<div class="fw-bold fs-6 text-gray-400">olivia@corpmail.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Feb 21, 2021</td>
															<td>$657.00</td>
															<td>
																<span class="badge badge-light-success fw-bolder px-4 py-3">Approved</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-8.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Francis Mitcham</a>
																		<div class="fw-bold fs-6 text-gray-400">f.mitcham@kpmg.com.au</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Apr 15, 2021</td>
															<td>$616.00</td>
															<td>
																<span class="badge badge-light-info fw-bolder px-4 py-3">In progress</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
														<tr>
															<td>
																<!--begin::User-->
																<div class="d-flex align-items-center">
																	<!--begin::Wrapper-->
																	<div class="me-5 position-relative">
																		<!--begin::Avatar-->
																		<div class="symbol symbol-35px symbol-circle">
																			<img alt="Pic" src="assets/media/avatars/150-7.jpg" />
																		</div>
																		<!--end::Avatar-->
																	</div>
																	<!--end::Wrapper-->
																	<!--begin::Info-->
																	<div class="d-flex flex-column justify-content-center">
																		<a href="" class="mb-1 text-gray-800 text-hover-primary">Ana Crown</a>
																		<div class="fw-bold fs-6 text-gray-400">ana.cf@limtel.com</div>
																	</div>
																	<!--end::Info-->
																</div>
																<!--end::User-->
															</td>
															<td>Nov 10, 2021</td>
															<td>$482.00</td>
															<td>
																<span class="badge badge-light-warning fw-bolder px-4 py-3">Pending</span>
															</td>
															<td class="text-end">
																<a href="#" class="btn btn-light btn-sm">View</a>
															</td>
														</tr>
													</tbody>
													<!--end::Body-->
												</table>
												<!--end::Table-->
											</div>
											<!--end::Table container-->
										</div>
										<!--end::Card body-->
									</div>
									<!--end::Card-->
								</div>
								<!--end::Tab pane-->
							</div>
							<!--end::Tab Content-->
								</div>
								<!--end::Card body-->
							</div>
							<!--end::Card-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted fw-bold me-1">2021©</span>
								<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Menu-->
							<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
								<li class="menu-item">
									<a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
								</li>
								<li class="menu-item">
									<a href="https://keenthemes.com/support" target="_blank" class="menu-link px-2">Support</a>
								</li>
								<li class="menu-item">
									<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
								</li>
							</ul>
							<!--end::Menu-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		{{ theme()->getView('pages/header_right') }}
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<!--end::Main-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/pages/projects/project/project.js"></script>
		<script src="assets/js/custom/modals/users-search.js"></script>
		<script src="assets/js/custom/modals/new-target.js"></script>
		<script src="assets/js/custom/widgets.js"></script>
		<script src="assets/js/custom/apps/chat/chat.js"></script>
		<script src="assets/js/custom/modals/create-app.js"></script>
		<script src="assets/js/custom/modals/upgrade-plan.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>