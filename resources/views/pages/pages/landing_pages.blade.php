<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="../../">
		<title>Metronic - the world's #1 selling Bootstrap Admin Theme Ecosystem for HTML, Vue, React, Angular &amp; Laravel by Keenthemes</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				{{ theme()->getView('pages/aside') }}
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					{{ theme()->getView('pages/header') }}
					<!--end::Header-->
					<!--begin::Toolbar-->
					<div class="toolbar py-2" id="kt_toolbar">
						<!--begin::Container-->
						<div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
							<!--begin::Page title-->
							<div class="flex-grow-1 flex-shrink-0 me-5">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Drawer Chat</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-3"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="../../demo6/dist/index.html" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">Apps</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">Chat</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Drawer Chat</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
							</div>
							<!--end::Page title-->
							<!--begin::Action group-->
							<div class="d-flex align-items-center flex-wrap">
								<!--begin::Wrapper-->
								<div class="flex-shrink-0 me-2">
									<ul class="nav">
										<li class="nav-item">
											<a class="nav-link btn btn-sm btn-color-muted btn-active-color-primary btn-active-light active fw-bold fs-7 px-4 me-1" data-bs-toggle="tab" href="#">Day</a>
										</li>
										<li class="nav-item">
											<a class="nav-link btn btn-sm btn-color-muted btn-active-color-primary btn-active-light fw-bold fs-7 px-4 me-1" data-bs-toggle="tab" href="">Week</a>
										</li>
										<li class="nav-item">
											<a class="nav-link btn btn-sm btn-color-muted btn-active-color-primary btn-active-light fw-bold fs-7 px-4" data-bs-toggle="tab" href="#">Year</a>
										</li>
									</ul>
								</div>
								<!--end::Wrapper-->
								<!--begin::Wrapper-->
								<div class="d-flex align-items-center">
									<!--begin::Daterangepicker-->
									<a href="#" class="btn btn-sm btn-bg-light btn-color-gray-500 btn-active-color-primary me-2" id="kt_dashboard_daterangepicker" data-bs-toggle="tooltip" data-bs-dismiss="click" data-bs-trigger="hover" title="Select dashboard daterange">
										<span class="fw-bold me-1" id="kt_dashboard_daterangepicker_title">Range:</span>
										<span class="fw-bolder" id="kt_dashboard_daterangepicker_date">October 14</span>
									</a>
									<!--end::Daterangepicker-->
									<!--begin::Actions-->
									<div class="d-flex align-items-center">
										<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light btn-active-color-primary">
											<!--begin::Svg Icon | path: icons/duotune/files/fil005.svg-->
											<span class="svg-icon svg-icon-2x">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13H13V10C13 9.4 12.6 9 12 9C11.4 9 11 9.4 11 10V13H8C7.4 13 7 13.4 7 14C7 14.6 7.4 15 8 15H11V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18V15H16C16.6 15 17 14.6 17 14C17 13.4 16.6 13 16 13Z" fill="black" />
													<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
										</button>
									</div>
									<!--end::Actions-->
								</div>
								<!--end::Wrapper-->
							</div>
							<!--end::Action group-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Toolbar-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-xxl">
							<!--begin::Heading-->
							<div class="d-flex flex-wrap flex-stack mb-6">
								<!--begin::Title-->
								<h3 class="fw-bolder my-2">My Contacts
								<span class="fs-6 fw-bold ms-1">(59)</span></h3>
								<!--end::Title-->
								<!--begin::Controls-->
								<div class="d-flex my-2">
									<!--begin::Select-->
									<select name="status" data-control="select2" data-hide-search="true" class="form-select form-select-white form-select-sm w-125px">
										<option value="Online" selected="selected">Online</option>
										<option value="Pending">Pending</option>
										<option value="Declined">Declined</option>
										<option value="Accepted">Accepted</option>
									</select>
									<!--end::Select-->
								</div>
								<!--end::Controls-->
							</div>
							<!--end::Heading-->
							<!--begin::Contacts-->
							<div class="row g-6 g-xl-9">
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<img alt="Pic" src="assets/media/avatars/150-1.jpg" />
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Emma Smith</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Art Director</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<span class="symbol-label bg-light-danger text-danger fs-5 fw-bolder">M</span>
													<div class="symbol-badge bg-success start-100 top-100 border-4 h-15px w-15px ms-n3 mt-n3"></div>
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Melody Macy</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Marketing Analytic</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<img alt="Pic" src="assets/media/avatars/150-26.jpg" />
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Max Smith</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Software Enginer</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<img alt="Pic" src="assets/media/avatars/150-4.jpg" />
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Sean Bean</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Web Developer</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<img alt="Pic" src="assets/media/avatars/150-15.jpg" />
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Brian Cox</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">UI/UX Designer</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<span class="symbol-label bg-light-warning text-warning fs-5 fw-bolder">M</span>
													<div class="symbol-badge bg-success start-100 top-100 border-4 h-15px w-15px ms-n3 mt-n3"></div>
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Mikaela Collins</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Head Of Marketing</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<img alt="Pic" src="assets/media/avatars/150-8.jpg" />
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Francis Mitcham</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Software Arcitect</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<span class="symbol-label bg-light-danger text-danger fs-5 fw-bolder">O</span>
													<div class="symbol-badge bg-success start-100 top-100 border-4 h-15px w-15px ms-n3 mt-n3"></div>
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Olivia Wild</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">System Admin</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<span class="symbol-label bg-light-primary text-primary fs-5 fw-bolder">N</span>
													<div class="symbol-badge bg-success start-100 top-100 border-4 h-15px w-15px ms-n3 mt-n3"></div>
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Neil Owen</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Account Manager</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<img alt="Pic" src="assets/media/avatars/150-6.jpg" />
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Dan Wilson</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Web Desinger</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<span class="symbol-label bg-light-danger text-danger fs-5 fw-bolder">E</span>
													<div class="symbol-badge bg-success start-100 top-100 border-4 h-15px w-15px ms-n3 mt-n3"></div>
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Emma Bold</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Corporate Finance</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-xxl-4">
									<!--begin::Card-->
									<div class="card">
										<!--begin::Card body-->
										<div class="card-body d-flex flex-center flex-column p-9">
											<!--begin::Wrapper-->
											<div class="mb-5">
												<!--begin::Avatar-->
												<div class="symbol symbol-75px symbol-circle">
													<img alt="Pic" src="assets/media/avatars/150-7.jpg" />
												</div>
												<!--end::Avatar-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Name-->
											<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">Ana Crown</a>
											<!--end::Name-->
											<!--begin::Position-->
											<div class="fw-bold text-gray-400 mb-6">Customer Relationship</div>
											<!--end::Position-->
											<!--begin::Info-->
											<div class="d-flex flex-center flex-wrap mb-5">
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 mx-3 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$14,560</div>
													<div class="fw-bold text-gray-400">Avg. Earnings</div>
												</div>
												<!--end::Stats-->
												<!--begin::Stats-->
												<div class="border border-gray-300 border-dashed rounded min-w-125px py-3 mx-3 px-4 mb-3">
													<div class="fs-6 fw-bolder text-gray-700">$236,400</div>
													<div class="fw-bold text-gray-400">Total Sales</div>
												</div>
												<!--end::Stats-->
											</div>
											<!--end::Info-->
											<!--begin::Link-->
											<button class="btn btn-sm btn-light-primary fw-bolder" data-kt-drawer-show="true" data-kt-drawer-target="#kt_drawer_chat">Send Message</button>
											<!--end::Link-->
										</div>
										<!--begin::Card body-->
									</div>
									<!--begin::Card-->
								</div>
								<!--end::Col-->
							</div>
							<!--end::Contacts-->
							<!--begin::Pagination-->
							<div class="d-flex flex-stack flex-wrap pt-10">
								<div class="fs-6 fw-bold text-gray-700">Showing 1 to 10 of 50 entries</div>
								<!--begin::Pages-->
								<ul class="pagination">
									<li class="page-item previous">
										<a href="#" class="page-link">
											<i class="previous"></i>
										</a>
									</li>
									<li class="page-item active">
										<a href="#" class="page-link">1</a>
									</li>
									<li class="page-item">
										<a href="#" class="page-link">2</a>
									</li>
									<li class="page-item">
										<a href="#" class="page-link">3</a>
									</li>
									<li class="page-item">
										<a href="#" class="page-link">4</a>
									</li>
									<li class="page-item">
										<a href="#" class="page-link">5</a>
									</li>
									<li class="page-item">
										<a href="#" class="page-link">6</a>
									</li>
									<li class="page-item next">
										<a href="#" class="page-link">
											<i class="next"></i>
										</a>
									</li>
								</ul>
								<!--end::Pages-->
							</div>
							<!--end::Pagination-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted fw-bold me-1">2021©</span>
								<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Menu-->
							<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
								<li class="menu-item">
									<a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
								</li>
								<li class="menu-item">
									<a href="https://keenthemes.com/support" target="_blank" class="menu-link px-2">Support</a>
								</li>
								<li class="menu-item">
									<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
								</li>
							</ul>
							<!--end::Menu-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		{{ theme()->getView('pages/header_right') }}
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<!--end::Main-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/widgets.js"></script>
		<script src="assets/js/custom/apps/chat/chat.js"></script>
		<script src="assets/js/custom/modals/create-app.js"></script>
		<script src="assets/js/custom/modals/upgrade-plan.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>