@extends('layout.demo6.base')

@section('toolbar')
	<!--begin::Action group-->
	<div class="d-flex align-items-center flex-wrap">
		<!--begin::Wrapper-->
		<div class="d-flex align-items-center me-2">
			<a href="javascript:" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder" data-bs-toggle="modal" data-bs-target="#kt_modal_add_customer"><i class="bi bi-person-plus-fill"></i> @lang('actions.create_attribute', ['attribute' => trans_choice('names.contact', 1)])</a>
		</div>
		<div class="d-flex align-items-center me-2">
			<a href="javascript:" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"><i class="bi bi-file-plus-fill"></i> @lang('actions.import_attribute', ['attribute' => trans_choice('names.contact', 2)])</a>
		</div>
		<div class="d-flex align-items-center me-2">
			<a href="javascript:" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder" data-bs-toggle="modal" data-bs-target="#kt_modal_view_users"><i class="bi bi-collection-fill"></i> @choice('names.contact_group', 2)</a>
		</div>
		<div class="d-flex align-items-center">
			<a href="javascript:" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"><i class="bi bi-search"></i> @lang('messages.advanced_search')</a>
		</div>
		<!--end::Wrapper-->
	</div>
	<!--end::Action group-->
@endsection
@section('content-container')
	<!--begin::Card-->
	<div class="card">
		<!--begin::Card header-->
		<div class="card-header border-0 pt-6">
			<!--begin::Card title-->
			<div class="card-title">
				<!--begin::Search-->
				<div class="d-flex align-items-center position-relative my-1">
					{!! theme()->getSvgIcon('assets/media/icons/duotune/general/gen021.svg', 'svg-icon-1 position-absolute ms-4') !!}
					<input type="text" data-kt-customer-table-filter="search" class="form-control form-control-sm form-control-solid w-250px ps-15" placeholder="@lang('messages.search_value')"/>
				</div>
				<!--end::Search-->
			</div>
			<!--begin::Card title-->
			<!--begin::Card toolbar-->
			<div class="card-toolbar">
				<!--begin::Toolbar-->
				<div class="d-flex justify-content-end" data-kt-customer-table-toolbar="base">
					<!--begin::Filter-->
					<button type="button" class="btn btn-sm btn-light-primary me-3" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
						{!! theme()->getSvgIcon('assets/media/icons/duotune/general/gen031.svg', 'svg-icon-2') !!} @lang('actions.filter')
					</button>
					<!--begin::Menu 1-->
					<div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true" id="kt-toolbar-filter">
						<!--begin::Header-->
						<div class="px-7 py-5">
							<div class="fs-4 text-dark fw-bolder">@lang('actions.attribute_options', ['attribute' => trans('actions.filter')])</div>
						</div>
						<!--end::Header-->
						<!--begin::Separator-->
						<div class="separator border-gray-200"></div>
						<!--end::Separator-->
						<!--begin::Content-->
						<div class="px-7 py-5">
							<!--begin::Input group-->
							<div class="mb-10">
								<!--begin::Label-->
								<label class="form-label fs-5 fw-bold mb-3">Month:</label>
								<!--end::Label-->
								<!--begin::Input-->
								<select class="form-select form-select-solid fw-bolder" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" data-kt-customer-table-filter="month" data-dropdown-parent="#kt-toolbar-filter">
									<option></option>
									<option value="aug">August</option>
									<option value="sep">September</option>
									<option value="oct">October</option>
									<option value="nov">November</option>
									<option value="dec">December</option>
								</select>
								<!--end::Input-->
							</div>
							<!--end::Input group-->
							<!--begin::Input group-->
							<div class="mb-10">
								<!--begin::Label-->
								<label class="form-label fs-5 fw-bold mb-3">Payment Type:</label>
								<!--end::Label-->
								<!--begin::Options-->
								<div class="d-flex flex-column flex-wrap fw-bold" data-kt-customer-table-filter="payment_type">
									<!--begin::Option-->
									<label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
										<input class="form-check-input" type="radio" name="payment_type" value="all" checked="checked"/>
										<span class="form-check-label text-gray-600">All</span>
									</label>
									<!--end::Option-->
									<!--begin::Option-->
									<label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
										<input class="form-check-input" type="radio" name="payment_type" value="visa"/>
										<span class="form-check-label text-gray-600">Visa</span>
									</label>
									<!--end::Option-->
									<!--begin::Option-->
									<label class="form-check form-check-sm form-check-custom form-check-solid mb-3">
										<input class="form-check-input" type="radio" name="payment_type" value="mastercard"/>
										<span class="form-check-label text-gray-600">Mastercard</span>
									</label>
									<!--end::Option-->
									<!--begin::Option-->
									<label class="form-check form-check-sm form-check-custom form-check-solid">
										<input class="form-check-input" type="radio" name="payment_type" value="american_express"/>
										<span class="form-check-label text-gray-600">American Express</span>
									</label>
									<!--end::Option-->
								</div>
								<!--end::Options-->
							</div>
							<!--end::Input group-->
							<!--begin::Actions-->
							<div class="d-flex justify-content-end">
								<button type="reset" class="btn btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true" data-kt-customer-table-filter="reset">Reset</button>
								<button type="submit" class="btn btn-primary" data-kt-menu-dismiss="true" data-kt-customer-table-filter="filter">Apply</button>
							</div>
							<!--end::Actions-->
						</div>
						<!--end::Content-->
					</div>
					<!--end::Menu 1-->
					<!--end::Filter-->
					<!--begin::Export-->
					<button type="button" class="btn btn-sm btn-light-primary" data-bs-toggle="modal" data-bs-target="#kt_customers_export_modal">
						{!! theme()->getSvgIcon('assets/media/icons/duotune/arrows/arr078.svg', 'svg-icon-2') !!} @lang('actions.export')
					</button>
					<!--end::Export-->
				</div>
				<!--end::Toolbar-->
				<!--begin::Group actions-->
				<div class="d-flex justify-content-end align-items-center d-none" data-kt-customer-table-toolbar="selected">
					<div class="fw-bolder me-5">
						<span class="me-2" data-kt-customer-table-select="selected_count"></span>@lang('actions.selected')
					</div>
					<button type="button" class="btn btn-danger" data-kt-customer-table-select="delete_selected">@lang('actions.delete_attribute', ['attribute' => trans('actions.selected')])</button>
				</div>
				<!--end::Group actions-->
			</div>
			<!--end::Card toolbar-->
		</div>
		<!--end::Card header-->
		<!--begin::Card body-->
		<div class="card-body pt-0">
			<!--begin::Table-->
			<table class="table align-middle table-row-dashed fs-6 gy-5" id="contacts-table">
				<!--begin::Table head-->
				<thead>
					<!--begin::Table row-->
					<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
						<th class="w-10px pe-2">
							<div class="form-check form-check-sm form-check-custom form-check-solid me-3">
								<input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_customers_table .form-check-input" value="1"/>
							</div>
						</th>
						<th class="min-w-125px">@translate('attributes.id')</th>
						<th class="min-w-125px">@translate('attributes.last_name')</th>
						<th class="min-w-125px">@translate('attributes.first_name')</th>
						<th class="min-w-125px">@translate('attributes.middle_name')</th>
						<th class="min-w-125px">@translate('attributes.gender')</th>
						<th class="min-w-125px">@translate('attributes.country')</th>
						<th class="min-w-125px">@translate('attributes.language', 2)</th>
						<th class="min-w-125px">@translate('attributes.birth_date')</th>
						<th class="min-w-125px">@translate('attributes.creation_date')</th>
						<th class="min-w-125px">@translate('attributes.attribute_type', replace: ['attribute' => ['messages.creation']])</th>
						<th class="min-w-125px">@translate('attributes.creator')</th>
						<th class="min-w-125px">@translate('actions.attribute_details', replace: ['attribute' => ['attributes.creator']])</th>
						<th class="min-w-125px">@translate('attributes.attribute_status', replace: ['attribute' => ['messages.detailed']])</th>
						<th class="min-w-125px">@translate('attributes.phone')</th>
						<th class="min-w-125px">@translate('attributes.attribute_type', replace: ['attribute' => ['attributes.phone']])</th>
						<th class="min-w-125px">@translate('attributes.email')</th>
						<th class="min-w-125px">@translate('attributes.attribute_type', replace: ['attribute' => ['attributes.email']])</th>
						<th class="min-w-125px">@translate('attributes.fax')</th>
						<th class="min-w-125px">@translate('attributes.attribute_status', replace: ['attribute' => ['attributes.verification']])</th>
						<th class="min-w-125px">@translate('attributes.attribute_type', replace: ['attribute' => ['attributes.verification']])</th>
						<th class="min-w-125px">@translate('actions.attribute_details', replace: ['attribute' => ['attributes.verification']])</th>
						<th class="min-w-125px">@translate('attributes.attribute_date', replace: ['attribute' => ['attributes.verification']])</th>
						<th class="min-w-125px">@translate('attributes.attribute_verification_flag', replace: ['attribute' => ['attributes.phone']])</th>
						<th class="min-w-125px">@translate('attributes.attribute_verification_flag', replace: ['attribute' => ['attributes.email']])</th>
						<th class="min-w-125px">@translate('attributes.attribute_verification_flag', replace: ['attribute' => ['attributes.fax']])</th>
						<th class="min-w-125px">@translate('messages.do_not_deal')</th>
						<th class="min-w-125px">@translate('messages.do_not_deal_attribute', replace: ['attribute' => ['attributes.date']])</th>
						<th class="min-w-125px">@translate('messages.do_not_deal_attribute', replace: ['attribute' => ['attributes.declarant']])</th>
						<th class="min-w-125px">@translate('messages.do_not_deal_attribute', replace: ['attribute' => ['attributes.reason']])</th>
						<th class="min-w-125px">@translate('messages.do_not_call')</th>
						<th class="min-w-125px">@translate('messages.do_not_call_attribute', replace: ['attribute' => ['attributes.date']])</th>
						<th class="min-w-125px">@translate('messages.do_not_call_attribute', replace: ['attribute' => ['attributes.declarant']])</th>
						<th class="min-w-125px">@translate('messages.do_not_call_attribute', replace: ['attribute' => ['attributes.reason']])</th>
						<th class="min-w-125px">@translate('messages.do_not_chat')</th>
						<th class="min-w-125px">@translate('messages.do_not_chat_attribute', replace: ['attribute' => ['attributes.date']])</th>
						<th class="min-w-125px">@translate('messages.do_not_chat_attribute', replace: ['attribute' => ['attributes.declarant']])</th>
						<th class="min-w-125px">@translate('messages.do_not_chat_attribute', replace: ['attribute' => ['attributes.reason']])</th>
						<th class="min-w-125px">@translate('messages.do_not_text')</th>
						<th class="min-w-125px">@translate('messages.do_not_text_attribute', replace: ['attribute' => ['attributes.date']])</th>
						<th class="min-w-125px">@translate('messages.do_not_text_attribute', replace: ['attribute' => ['attributes.declarant']])</th>
						<th class="min-w-125px">@translate('messages.do_not_text_attribute', replace: ['attribute' => ['attributes.reason']])</th>
						<th class="min-w-125px">@translate('messages.do_not_email')</th>
						<th class="min-w-125px">@translate('messages.do_not_email_attribute', replace: ['attribute' => ['attributes.date']])</th>
						<th class="min-w-125px">@translate('messages.do_not_email_attribute', replace: ['attribute' => ['attributes.declarant']])</th>
						<th class="min-w-125px">@translate('messages.do_not_email_attribute', replace: ['attribute' => ['attributes.reason']])</th>
						<th class="min-w-125px">@translate('attributes.personal_group')</th>
						<th class="text-end min-w-70px">@translate('attributes.action', 2)</th>
					</tr>
					<!--end::Table row-->
				</thead>
				<!--end::Table head-->
				<!--begin::Table body-->
				<tbody class="fw-bold text-gray-600">
				</tbody>
				<!--end::Table body-->
			</table>
			<!--end::Table-->
		</div>
		<!--end::Card body-->
	</div>
	<!--end::Card-->
@endsection
@section('modals-container')
	<!--begin::Modal-Contact-Add-->
	<div class="modal fade" tabindex="-1" id="kt_modal_add_customer">
		<form id="new-organization-country-scope-form" onkeydown="return event.key !== 'Enter';">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-950px">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Form-->
					<form class="form" action="#" id="kt_modal_add_customer_form" data-kt-redirect="../../demo6/dist/apps/customers/list.html">
						<!--begin::Modal header-->
						<div class="modal-header" id="kt_modal_add_customer_header">
							<!--begin::Modal title-->
							<h2 class="fw-bolder">Add a Contact</h2>
							<!--end::Modal title-->
							<!--begin::Close-->
							<div id="kt_modal_add_customer_close" class="btn btn-icon btn-sm btn-active-icon-primary">
								<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
								<span class="svg-icon svg-icon-1">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
										<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"/>
										<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"/>
									</svg>
								</span>
								<!--end::Svg Icon-->
							</div>
							<!--end::Close-->
						</div>
						<!--end::Modal header-->
						<!--begin::Modal body-->
						<div class="modal-body py-10 px-lg-17">
							<!--begin::Scroll-->
							<div class="scroll-y me-n7 pe-7" id="kt_modal_add_customer_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_customer_header" data-kt-scroll-wrappers="#kt_modal_add_customer_scroll" data-kt-scroll-offset="300px">
								<!--begin::Input group-->
								<div class="fv-row mb-7 row" data-kt-buttons="true" data-kt-buttons-target="[data-kt-button]">
									<!--begin::Label-->
									<label class="required fs-6 fw-bold mb-2">Gender</label>
									<!--end::Label-->
									<div>
										<label class="btn btn-outline-secondary text-muted text-hover-white text-active-white btn-outline btn-active-success" data-kt-button="true">
											<!--begin::Input-->
											<input class="btn-check" type="radio" name="gender" value="1" data-com.bitwarden.browser.user-edited="yes">
											<!--end::Input-->
											Male
										</label>
										<!--end::Radio-->
										<!--begin::Radio-->
										<label class="btn btn-outline-secondary text-muted text-hover-white text-active-white btn-outline btn-active-success active" data-kt-button="true">
											<!--begin::Input-->
											<input class="btn-check" type="radio" name="gender" checked="checked" value="2" data-com.bitwarden.browser.user-edited="yes">
											<!--end::Input-->
											Female
										</label>
										<!--end::Radio-->
									</div>
								</div>
								<!--begin::Input group-->
								
								<!--begin::Input group-->
								<div class="fv-row mb-7 row">
									<div class="col-4">
										<!--begin::Label-->
										<label class="required fs-6 fw-bold mb-2">First Name</label>
										<!--end::Label-->
										<!--begin::Input-->
										<input type="text" class="form-control form-control-solid" placeholder="" name="firstname" value="Sean Bean"/>
										<!--end::Input-->
									</div>
									<div class="col-4">
										<!--begin::Label-->
										<label class="required fs-6 fw-bold mb-2">Middle Name</label>
										<!--end::Label-->
										<!--begin::Input-->
										<input type="text" class="form-control form-control-solid" placeholder="" name="middlename" value="Sean Bean"/>
										<!--end::Input-->
									</div>
									<div class="col-4">
										<!--begin::Label-->
										<label class="required fs-6 fw-bold mb-2">Last Name</label>
										<!--end::Label-->
										<!--begin::Input-->
										<input type="text" class="form-control form-control-solid" placeholder="" name="lastname" value="Sean Bean"/>
										<!--end::Input-->
									</div>
								</div>
								<!--end::Input group-->
								
								<!--begin::Input group-->
								<div class="fv-row mb-7 row">
									<div class="col-6">
										<!--begin::Label-->
										<label class="fs-6 fw-bold mb-2">
											<span class="required">Country</span>
											<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
										</label>
										<!--end::Label-->
										<!--begin::Input-->
										<select name="country" aria-label="Select a Country" data-control="select2" data-placeholder="Select a Country..." data-dropdown-parent="#kt_modal_add_customer" class="form-select form-select-solid fw-bolder">
											<option value="">Select a Country...</option>
											<option value="AF">Afghanistan</option>
											<option value="AX">Aland Islands</option>
											<option value="AL">Albania</option>
											<option value="DZ">Algeria</option>
											<option value="AS">American Samoa</option>
											<option value="AD">Andorra</option>
											<option value="AO">Angola</option>
											<option value="AI">Anguilla</option>
											<option value="AG">Antigua and Barbuda</option>
											<option value="AR">Argentina</option>
											<option value="AM">Armenia</option>
											<option value="AW">Aruba</option>
											<option value="AU">Australia</option>
											<option value="AT">Austria</option>
											<option value="AZ">Azerbaijan</option>
											<option value="BS">Bahamas</option>
											<option value="BH">Bahrain</option>
											<option value="BD">Bangladesh</option>
											<option value="BB">Barbados</option>
											<option value="BY">Belarus</option>
											<option value="BE">Belgium</option>
											<option value="BZ">Belize</option>
											<option value="BJ">Benin</option>
											<option value="BM">Bermuda</option>
											<option value="BT">Bhutan</option>
											<option value="BO">Bolivia, Plurinational State of</option>
											<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
											<option value="BA">Bosnia and Herzegovina</option>
											<option value="BW">Botswana</option>
											<option value="BR">Brazil</option>
											<option value="IO">British Indian Ocean Territory</option>
											<option value="BN">Brunei Darussalam</option>
											<option value="BG">Bulgaria</option>
											<option value="BF">Burkina Faso</option>
											<option value="BI">Burundi</option>
											<option value="KH">Cambodia</option>
											<option value="CM">Cameroon</option>
											<option value="CA">Canada</option>
											<option value="CV">Cape Verde</option>
											<option value="KY">Cayman Islands</option>
											<option value="CF">Central African Republic</option>
											<option value="TD">Chad</option>
											<option value="CL">Chile</option>
											<option value="CN">China</option>
											<option value="CX">Christmas Island</option>
											<option value="CC">Cocos (Keeling) Islands</option>
											<option value="CO">Colombia</option>
											<option value="KM">Comoros</option>
											<option value="CK">Cook Islands</option>
											<option value="CR">Costa Rica</option>
											<option value="CI">Côte d'Ivoire</option>
											<option value="HR">Croatia</option>
											<option value="CU">Cuba</option>
											<option value="CW">Curaçao</option>
											<option value="CZ">Czech Republic</option>
											<option value="DK">Denmark</option>
											<option value="DJ">Djibouti</option>
											<option value="DM">Dominica</option>
											<option value="DO">Dominican Republic</option>
											<option value="EC">Ecuador</option>
											<option value="EG">Egypt</option>
											<option value="SV">El Salvador</option>
											<option value="GQ">Equatorial Guinea</option>
											<option value="ER">Eritrea</option>
											<option value="EE">Estonia</option>
											<option value="ET">Ethiopia</option>
											<option value="FK">Falkland Islands (Malvinas)</option>
											<option value="FJ">Fiji</option>
											<option value="FI">Finland</option>
											<option value="FR">France</option>
											<option value="PF">French Polynesia</option>
											<option value="GA">Gabon</option>
											<option value="GM">Gambia</option>
											<option value="GE">Georgia</option>
											<option value="DE">Germany</option>
											<option value="GH">Ghana</option>
											<option value="GI">Gibraltar</option>
											<option value="GR">Greece</option>
											<option value="GL">Greenland</option>
											<option value="GD">Grenada</option>
											<option value="GU">Guam</option>
											<option value="GT">Guatemala</option>
											<option value="GG">Guernsey</option>
											<option value="GN">Guinea</option>
											<option value="GW">Guinea-Bissau</option>
											<option value="HT">Haiti</option>
											<option value="VA">Holy See (Vatican City State)</option>
											<option value="HN">Honduras</option>
											<option value="HK">Hong Kong</option>
											<option value="HU">Hungary</option>
											<option value="IS">Iceland</option>
											<option value="IN">India</option>
											<option value="ID">Indonesia</option>
											<option value="IR">Iran, Islamic Republic of</option>
											<option value="IQ">Iraq</option>
											<option value="IE">Ireland</option>
											<option value="IM">Isle of Man</option>
											<option value="IL">Israel</option>
											<option value="IT">Italy</option>
											<option value="JM">Jamaica</option>
											<option value="JP">Japan</option>
											<option value="JE">Jersey</option>
											<option value="JO">Jordan</option>
											<option value="KZ">Kazakhstan</option>
											<option value="KE">Kenya</option>
											<option value="KI">Kiribati</option>
											<option value="KP">Korea, Democratic People's Republic of</option>
											<option value="KW">Kuwait</option>
											<option value="KG">Kyrgyzstan</option>
											<option value="LA">Lao People's Democratic Republic</option>
											<option value="LV">Latvia</option>
											<option value="LB">Lebanon</option>
											<option value="LS">Lesotho</option>
											<option value="LR">Liberia</option>
											<option value="LY">Libya</option>
											<option value="LI">Liechtenstein</option>
											<option value="LT">Lithuania</option>
											<option value="LU">Luxembourg</option>
											<option value="MO">Macao</option>
											<option value="MG">Madagascar</option>
											<option value="MW">Malawi</option>
											<option value="MY">Malaysia</option>
											<option value="MV">Maldives</option>
											<option value="ML">Mali</option>
											<option value="MT">Malta</option>
											<option value="MH">Marshall Islands</option>
											<option value="MQ">Martinique</option>
											<option value="MR">Mauritania</option>
											<option value="MU">Mauritius</option>
											<option value="MX">Mexico</option>
											<option value="FM">Micronesia, Federated States of</option>
											<option value="MD">Moldova, Republic of</option>
											<option value="MC">Monaco</option>
											<option value="MN">Mongolia</option>
											<option value="ME">Montenegro</option>
											<option value="MS">Montserrat</option>
											<option value="MA">Morocco</option>
											<option value="MZ">Mozambique</option>
											<option value="MM">Myanmar</option>
											<option value="NA">Namibia</option>
											<option value="NR">Nauru</option>
											<option value="NP">Nepal</option>
											<option value="NL">Netherlands</option>
											<option value="NZ">New Zealand</option>
											<option value="NI">Nicaragua</option>
											<option value="NE">Niger</option>
											<option value="NG">Nigeria</option>
											<option value="NU">Niue</option>
											<option value="NF">Norfolk Island</option>
											<option value="MP">Northern Mariana Islands</option>
											<option value="NO">Norway</option>
											<option value="OM">Oman</option>
											<option value="PK">Pakistan</option>
											<option value="PW">Palau</option>
											<option value="PS">Palestinian Territory, Occupied</option>
											<option value="PA">Panama</option>
											<option value="PG">Papua New Guinea</option>
											<option value="PY">Paraguay</option>
											<option value="PE">Peru</option>
											<option value="PH">Philippines</option>
											<option value="PL">Poland</option>
											<option value="PT">Portugal</option>
											<option value="PR">Puerto Rico</option>
											<option value="QA">Qatar</option>
											<option value="RO">Romania</option>
											<option value="RU">Russian Federation</option>
											<option value="RW">Rwanda</option>
											<option value="BL">Saint Barthélemy</option>
											<option value="KN">Saint Kitts and Nevis</option>
											<option value="LC">Saint Lucia</option>
											<option value="MF">Saint Martin (French part)</option>
											<option value="VC">Saint Vincent and the Grenadines</option>
											<option value="WS">Samoa</option>
											<option value="SM">San Marino</option>
											<option value="ST">Sao Tome and Principe</option>
											<option value="SA">Saudi Arabia</option>
											<option value="SN">Senegal</option>
											<option value="RS">Serbia</option>
											<option value="SC">Seychelles</option>
											<option value="SL">Sierra Leone</option>
											<option value="SG">Singapore</option>
											<option value="SX">Sint Maarten (Dutch part)</option>
											<option value="SK">Slovakia</option>
											<option value="SI">Slovenia</option>
											<option value="SB">Solomon Islands</option>
											<option value="SO">Somalia</option>
											<option value="ZA">South Africa</option>
											<option value="KR">South Korea</option>
											<option value="SS">South Sudan</option>
											<option value="ES">Spain</option>
											<option value="LK">Sri Lanka</option>
											<option value="SD">Sudan</option>
											<option value="SR">Suriname</option>
											<option value="SZ">Swaziland</option>
											<option value="SE">Sweden</option>
											<option value="CH">Switzerland</option>
											<option value="SY">Syrian Arab Republic</option>
											<option value="TW">Taiwan, Province of China</option>
											<option value="TJ">Tajikistan</option>
											<option value="TZ">Tanzania, United Republic of</option>
											<option value="TH">Thailand</option>
											<option value="TG">Togo</option>
											<option value="TK">Tokelau</option>
											<option value="TO">Tonga</option>
											<option value="TT">Trinidad and Tobago</option>
											<option value="TN">Tunisia</option>
											<option value="TR">Turkey</option>
											<option value="TM">Turkmenistan</option>
											<option value="TC">Turks and Caicos Islands</option>
											<option value="TV">Tuvalu</option>
											<option value="UG">Uganda</option>
											<option value="UA">Ukraine</option>
											<option value="AE">United Arab Emirates</option>
											<option value="GB">United Kingdom</option>
											<option value="US" selected="selected">United States</option>
											<option value="UY">Uruguay</option>
											<option value="UZ">Uzbekistan</option>
											<option value="VU">Vanuatu</option>
											<option value="VE">Venezuela, Bolivarian Republic of</option>
											<option value="VN">Vietnam</option>
											<option value="VI">Virgin Islands</option>
											<option value="YE">Yemen</option>
											<option value="ZM">Zambia</option>
											<option value="ZW">Zimbabwe</option>
										</select>
										<!--end::Input-->
									</div>
									<div class="col-6">
										<!--begin::Label-->
										<label class="fs-6 fw-bold mb-2">
											<span class="required">Languages</span>
											<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
										</label>
										<!--end::Label-->
										<!--begin::Input-->
										<select name="languages" aria-label="Select a language" data-control="select2" data-placeholder="Select a language..." data-dropdown-parent="#kt_modal_add_customer" class="form-select form-select-solid fw-bolder">
											<option value="">Select a language...</option>
											<option value="FR">French</option>
											<option value="EN">English</option>
											<option value="AR">Arabe</option>
										</select>
										<!--end::Input-->
									</div>
									<!--end::Input group-->
								</div>
								<!--end::Input group-->
								
								<!--begin::Input group-->
								<div class="fv-row mb-7 row">
									<!--begin::Label-->
									<label class="fs-6 fw-bold mb-2 required">Birth date</label>
									<!--end::Label-->
									<!--begin::Input-->
									<input class="form-control form-control-solid" name="calendar_event_start_date" placeholder="Pick a start date" id="kt_calendar_datepicker_start_date"/>
									<!--end::Input-->
								</div>
								<!--end::Input group-->
								
								<!--begin::Input group-->
								<div class="fv-row mb-7 row">
									<!--begin::Label-->
									<label class="fs-6 fw-bold mb-2">
										<span class="required">Personal Groups</span>
										<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Country of origination"></i>
									</label>
									<!--end::Label-->
									<!--begin::Input-->
									<select name="languages" aria-label="Select a group" data-control="select2" data-placeholder="Select a group..." data-dropdown-parent="#kt_modal_add_customer" class="form-select form-select-solid fw-bolder">
										<option value="">Select a group...</option>
										<option value="cl1">Clients 1</option>
										<option value="cl2">Clients 2</option>
										<option value="cl3">Clients 3</option>
									</select>
									<!--end::Input-->
								</div>
								<!--end::Input group-->
								
								<!--begin::Billing toggle-->
								<div class="fw-bolder fs-3 rotate collapsible mb-7" data-bs-toggle="collapse" href="#kt_modal_add_customer_billing_info" role="button" aria-expanded="false" aria-controls="kt_customer_view_details">Shipping Information
									<span class="ms-2 rotate-180">
										<!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
										<span class="svg-icon svg-icon-3">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
												<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black"/>
											</svg>
										</span>
										<!--end::Svg Icon-->
									</span></div>
								<!--end::Billing toggle-->
								<!--begin::Billing form-->
								<div id="kt_modal_add_customer_billing_info" class="collapse show">
									<!--begin::Input group-->
									<div class="d-flex flex-column mb-7 fv-row">
										<!--begin::Label-->
										<label class="required fs-6 fw-bold mb-2">Address Line 1</label>
										<!--end::Label-->
										<!--begin::Input-->
										<input class="form-control form-control-solid" placeholder="" name="address1" value="101, Collins Street"/>
										<!--end::Input-->
									</div>
									<!--end::Input group-->
									<!--begin::Input group-->
									<div class="d-flex flex-column mb-7 fv-row">
										<!--begin::Label-->
										<label class="fs-6 fw-bold mb-2">Address Line 2</label>
										<!--end::Label-->
										<!--begin::Input-->
										<input class="form-control form-control-solid" placeholder="" name="address2" value=""/>
										<!--end::Input-->
									</div>
									<!--end::Input group-->
									<!--begin::Input group-->
									<div class="d-flex flex-column mb-7 fv-row">
										<!--begin::Label-->
										<label class="required fs-6 fw-bold mb-2">City</label>
										<!--end::Label-->
										<!--begin::Input-->
										<input class="form-control form-control-solid" placeholder="" name="city" value="Melbourne"/>
										<!--end::Input-->
									</div>
									<!--end::Input group-->
									<!--begin::Input group-->
									<div class="row g-9 mb-7">
										<!--begin::Col-->
										<div class="col-md-6 fv-row">
											<!--begin::Label-->
											<label class="required fs-6 fw-bold mb-2">State / Province</label>
											<!--end::Label-->
											<!--begin::Input-->
											<input class="form-control form-control-solid" placeholder="" name="state" value="Victoria"/>
											<!--end::Input-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-md-6 fv-row">
											<!--begin::Label-->
											<label class="required fs-6 fw-bold mb-2">Post Code</label>
											<!--end::Label-->
											<!--begin::Input-->
											<input class="form-control form-control-solid" placeholder="" name="postcode" value="3000"/>
											<!--end::Input-->
										</div>
										<!--end::Col-->
									</div>
									<!--end::Input group-->
								</div>
								<!--end::Billing form-->
							</div>
							<!--end::Scroll-->
						</div>
						<!--end::Modal body-->
						<!--begin::Modal footer-->
						<div class="modal-footer flex-center">
							<!--begin::Button-->
							<button type="reset" id="kt_modal_add_customer_cancel" class="btn btn-light me-3">Discard</button>
							<!--end::Button-->
							<!--begin::Button-->
							<button type="submit" id="kt_modal_add_customer_submit" class="btn btn-primary">
								<span class="indicator-label">Submit</span>
								<span class="indicator-progress">Please wait...
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
							</button>
							<!--end::Button-->
						</div>
						<!--end::Modal footer-->
					</form>
					<!--end::Form-->
				</div>
			</div>
		</form>
	</div>
	<!--end::Modal-Contact-Add-->
	
	<div class="modal fade" tabindex="-1" id="new-organization-country-scope-modal">
		<form id="new-organization-country-scope-form" onkeydown="return event.key !== 'Enter';">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">@lang('actions.create_attribute', ['attribute' => trans_choice('names.country_scope', 1)])</h5>
						<!--begin::Close-->
						<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
							<span class="svg-icon svg-icon-2x"></span>
						</div>
						<!--end::Close-->
					</div>
					<div class="modal-body">
						<div class="form-row">
							<div class="form-group col-12 fv-row">
								<label class="form-label" for="new-organization-country-scope-code">@choice('attributes.country', 1)</label>
								<select name="code" id="new-organization-country-scope-code" class="form-control form-control-sm" data-control="select2" data-dropdown-parent="#new-organization-country-scope-modal">
									<option selected disabled></option>
									@isset($allowedCountriesList)
										@foreach ($allowedCountriesList as $country)
											<option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
										@endforeach
									@endisset
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-sm btn-light" data-bs-dismiss="modal">@lang('actions.close')</button>
						<button type="submit" class="btn btn-sm btn-primary">@lang('actions.submit')</button>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection
@section('style')
	<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('scripts')
	<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
	<script src="{{ asset('assets/js/pages/contacts/contacts/index.js') }}"></script>
@endsection
