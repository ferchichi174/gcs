@extends('layout.demo6.base')

@section('toolbar')
	<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light btn-active-color-primary" data-bs-toggle="modal" data-bs-target="#new-organization-country-scope-modal">
		{!! theme()->getSvgIcon('assets/media/icons/duotune/files/fil005.svg', 'svg-icon-2x') !!}
	</button>
@endsection
@section('content-container')
	@include('partials.organization.navbar')
	
	<!--begin::Table-->
	<div class="card mt-6 mt-xl-9">
		<!--begin::Card header-->
		<div class="card-header m-0 pt-4 pb-4 bg-lighten">
			<!--begin::Card title-->
			<div class="card-title flex-column">
				<h3 class="fw-bolder mb-1">{{ trans_choice('actions.list', 1, ['attribute' => trans_choice('names.country_scope', 2)]) }}</h3>
				<div class="fs-6 text-gray-400">{{ trans_choice('messages.country_scopes_table_description', 1) }}</div>
			</div>
			<!--begin::Card title-->
			<!--begin::Card toolbar-->
			<div class="card-toolbar my-1">
				<button class="btn btn-primary btn-sm align-self-center me-4" data-bs-toggle="modal" data-bs-target="#new-organization-country-scope-modal">{{ trans_choice('actions.create_attribute', 1, ['attribute' => trans_choice('names.country_scope', 1)]) }}</button>
				<!--begin::Select-->
				<div class="me-4 my-1">
					<select name="datatable-scope-status" data-control="select2" data-hide-search="true" class="w-125px form-select form-select-solid form-select-sm">
						<option value="all" selected="selected">@lang('values.all')</option>
						@isset($countryScopeStatusesList)
							@foreach($countryScopeStatusesList AS $status)
								<option value="{{ $status['name'] }}">{{ $status['name'] }}</option>
							@endforeach
						@endisset
					</select>
				</div>
				<!--end::Select-->
				<!--begin::Search-->
				<div class="d-flex align-items-center position-relative my-1">
					{!! theme()->getSvgIcon("demo6/media/icons/duotune/general/gen021.svg", "svg-icon-3 position-absolute ms-3") !!}
					<input type="text" id="kt_filter_search" class="form-control form-control-solid form-select-sm w-150px ps-9" data-datatable-filter="search" placeholder="Search"/>
				</div>
				<!--end::Search-->
			</div>
			<!--begin::Card toolbar-->
		</div>
		<!--end::Card header-->
		<!--begin::Card body-->
		<div class="card-body pt-0">
			<!--begin::Table container-->
			<div>
				<!--begin::Table-->
				<table id="organization-country-scopes-table" class="table align-middle table-row-bordered table-row-dashed fw-bolder fs-6 gy-5">
					<!--begin::Head-->
					<thead class="text-start text-gray-400 fs-7 text-uppercase gs-0">
						<tr>
							<th class="">ID</th>
							<th class="">{{ trans_choice('attributes.name', 1) }}</th>
							<th class="">{{ trans_choice('attributes.status', 1) }}</th>
							<th class="text-end min-w-100px">{{ trans_choice('attributes.action', 2) }}</th>
						</tr>
					</thead>
					<!--end::Head-->
					<!--begin::Body-->
					<tbody class="fs-6"></tbody>
					<!--end::Body-->
				</table>
				<!--end::Table-->
			</div>
			<!--end::Table container-->
		</div>
		<!--end::Card body-->
	</div>
	<!--end::Table-->
@endsection
@section('modals-container')
	<div class="modal fade" tabindex="-1" id="new-organization-country-scope-modal">
		<form id="new-organization-country-scope-form" onkeydown="return event.key !== 'Enter';">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">@lang('actions.create_attribute', ['attribute' => trans_choice('names.country_scope', 1)])</h5>
						<!--begin::Close-->
						<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
							<span class="svg-icon svg-icon-2x"></span>
						</div>
						<!--end::Close-->
					</div>
					<div class="modal-body">
						<div class="form-row">
							<div class="form-group col-12 fv-row">
								<label class="form-label" for="new-organization-country-scope-code">@choice('attributes.country', 1)</label>
								<select name="code" id="new-organization-country-scope-code" class="form-control form-control-sm" data-control="select2" data-dropdown-parent="#new-organization-country-scope-modal">
									<option selected disabled></option>
									@isset($allowedCountriesList)
										@foreach ($allowedCountriesList as $country)
											<option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
										@endforeach
									@endisset
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-sm btn-light" data-bs-dismiss="modal">@lang('actions.close')</button>
						<button type="submit" class="btn btn-sm btn-primary">@lang('actions.submit')</button>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection
@section('style')
	<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('scripts')
	<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
	<script src="{{ asset('assets/js/pages/organisation/country-scopes/index.js') }}"></script>
@endsection
