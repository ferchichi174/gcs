@extends('layout.demo6.base')

@section('toolbar')
	<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light btn-active-color-primary" data-bs-toggle="modal" data-bs-target="#new-organization-country-scope-modal">
		{!! theme()->getSvgIcon('assets/media/icons/duotune/files/fil005.svg', 'svg-icon-2x') !!}
	</button>
@endsection
@section('content-container')
	@include('partials.organization.navbar')
	
	<!--begin::Table-->
	<div class="card mt-6 mt-xl-9 bg-transparent">
		<!--begin::Card header-->
		<div class="bg-white rounded">
			<div class="card-header m-0 pt-4 pb-4 bg-lighten">
				<!--begin::Card title-->
				<div class="card-title flex-column">
					<h3 class="fw-bolder mb-1">{{ trans_choice('actions.list', 1, ['attribute' => trans_choice('names.landing_page', 2)]) }}</h3>
					<div class="fs-6 text-gray-400">{{ trans_choice('messages.landing_pages_description', 1) }}</div>
				</div>
				<!--begin::Card title-->
				<!--begin::Card toolbar-->
				<div class="card-toolbar my-1">
					<!--begin::Display changer -->
					<ul class="nav nav-pills me-6 mb-2 mb-sm-0">
						<li class="nav-item m-0">
							<a class="btn btn-sm btn-icon btn-light btn-color-muted btn-active-primary me-3 active" data-bs-toggle="tab" href="#landing-pages-list-container">
								{!! theme()->getSvgIcon('assets/media/icons/duotune/general/gen024.svg', 'svg-icon-2x') !!}
							</a>
						</li>
						<li class="nav-item m-0">
							<a class="btn btn-sm btn-icon btn-light btn-color-muted btn-active-primary" data-bs-toggle="tab" href="#landing-pages-table-container">
								{!! theme()->getSvgIcon('assets/media/icons/duotune/abstract/abs015.svg', 'svg-icon-2x') !!}
							</a>
						</li>
					</ul>
					<!--end::Display changer -->
					<!--begin::Select-->
					<div class="me-4 my-1">
						<select name="datatable-landing-page-status" data-control="select2" data-hide-search="true" class="w-125px form-select form-select-solid form-select-sm">
							<option value="all" selected="selected">@lang('values.all')</option>
							@isset($landingPageStatusesList)
								@foreach($landingPageStatusesList AS $status)
									<option value="{{ $status['name'] }}">{{ $status['name'] }}</option>
								@endforeach
							@endisset
						</select>
					</div>
					<!--end::Select-->
					<!--begin::Search-->
					<div class="d-flex align-items-center position-relative my-1">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/general/gen021.svg", "svg-icon-3 position-absolute ms-3") !!}
						<input type="text" id="kt_filter_search" class="form-control form-control-solid form-select-sm w-150px ps-9" data-datatable-filter="search" placeholder="Search"/>
					</div>
					<!--end::Search-->
				</div>
				<!--begin::Card toolbar-->
			</div>
		</div>
		<!--end::Card header-->
		<div class="card-body p-0">
			<div class="tab-content">
				<div id="landing-pages-table-container" class="tab-pane fade p-8 pt-0 rounded-bottom bg-white">
					<table id="landing-pages-table" class="table align-middle table-row-bordered table-row-dashed fw-bolder px-4 fs-6 gy-5">
						<!--begin::Head-->
						<thead class="text-start text-gray-400 fs-7 text-uppercase gs-0">
							<tr>
								<th class="">{{ trans_choice('attributes.id', 1) }}</th>
								<th class="">{{ trans_choice('attributes.name', 1) }}</th>
								<th class="">{{ trans_choice('attributes.domain_name', 1) }}</th>
								<th class="">{{ trans_choice('attributes.url', 1) }}</th>
								<th class="">{{ trans_choice('attributes.starting_date', 1) }}</th>
								<th class="">{{ trans_choice('attributes.status', 1) }}</th>
								<th class="text-end min-w-100px">{{ trans_choice('attributes.action', 2) }}</th>
							</tr>
						</thead>
						<!--end::Head-->
						<!--begin::Body-->
						<tbody class="fs-6"></tbody>
						<!--end::Body-->
					</table>
				</div>
				<div id="landing-pages-list-container" class="tab-pane fade active show">
					<div id="landing-pages-list" class="row g-1 mt-5 mb-5">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end::Table-->
@endsection
@section('modals-container')
	<div class="modal fade" tabindex="-1" id="landing-page-details-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">@lang('actions.attribute_details', ['attribute' => trans_choice('names.landing_page', 1)])</h5>
					<!--begin::Close-->
					<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
						<span class="svg-icon svg-icon-2x"></span>
					</div>
					<!--end::Close-->
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="form-group col-12 mb-4">
							<label class="fw-bold text-muted">@choice('attributes.name', 1):</label>
							<div class="fw-bolder fs-6 text-gray-800 landing-page-name"></div>
						</div>
						<div class="form-group col-12 mb-4">
							<label class="fw-bold text-muted">@choice('attributes.domain_name', 1):</label>
							<div class="fw-bolder fs-6 text-gray-800 landing-page-domain_name"></div>
						</div>
						<div class="form-group col-12 mb-4">
							<label class="fw-bold text-muted">@choice('attributes.url', 1):</label>
							<div class="fw-bolder fs-6 text-gray-800 landing-page-url"></div>
						</div>
						<div class="form-group col-12 mb-4">
							<label class="fw-bold text-muted">@choice('attributes.starting_date', 1):</label>
							<div class="fw-bolder fs-6 text-gray-800 landing-page-created_at"></div>
						</div>
						<div class="form-group col-12 mb-4">
							<label class="fw-bold text-muted">@choice('attributes.status', 1):</label>
							<div class="fw-bolder fs-6 text-gray-800 landing-page-status"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-light" data-bs-dismiss="modal">@lang('actions.close')</button>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('style')
	<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('scripts')
	<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
	<script src="{{ asset('assets/plugins/custom/fslightbox/fslightbox.bundle.js') }}"></script>
	<script src="{{ asset('assets/js/pages/organisation/landing-pages/index.js') }}"></script>
@endsection
