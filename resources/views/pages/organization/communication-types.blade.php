@extends('layout.demo6.base')

@section('toolbar')
	<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light btn-active-color-primary" data-bs-toggle="modal" data-bs-target="#new-organization-country-scope-modal">
		{!! theme()->getSvgIcon('assets/media/icons/duotune/files/fil005.svg', 'svg-icon-2x') !!}
	</button>
@endsection
@section('content-container')
	@include('partials.organization.navbar')
	
	<!--begin::Table-->
	<div class="card mt-6 mt-xl-9">
		<!--begin::Card header-->
		<div class="card-header m-0 pt-4 pb-4 bg-lighten">
			<!--begin::Card title-->
			<div class="card-title flex-column">
				<h3 class="fw-bolder mb-1">{{ trans_choice('actions.list', 1, ['attribute' => trans_choice('names.communication', 2)]) }}</h3>
				<div class="fs-6 text-gray-400">{{ trans_choice('messages.communication_table_description', 1) }}</div>
			</div>
			<!--begin::Card title-->
			<!--begin::Card toolbar-->
			<div class="card-toolbar my-1">
				<!--begin::Select-->
				<div class="me-4 my-1">
					<select name="datatable-type-status" data-control="select2" data-hide-search="true" class="w-125px form-select form-select-solid form-select-sm">
						<option value="all" selected="selected">@lang('values.all')</option>
						@isset($accountCommunicationTypeStatusesList)
							@foreach($accountCommunicationTypeStatusesList AS $status)
								<option value="{{ $status['name'] }}">{{ $status['name'] }}</option>
							@endforeach
						@endisset
					</select>
				</div>
				<!--end::Select-->
				<!--begin::Search-->
				<div class="d-flex align-items-center position-relative my-1">
					{!! theme()->getSvgIcon("demo6/media/icons/duotune/general/gen021.svg", "svg-icon-3 position-absolute ms-3") !!}
					<input type="text" id="kt_filter_search" class="form-control form-control-solid form-select-sm w-150px ps-9" data-datatable-filter="search" placeholder="Search"/>
				</div>
				<!--end::Search-->
			</div>
			<!--begin::Card toolbar-->
		</div>
		<!--end::Card header-->
		<!--begin::Card body-->
		<div class="card-body pt-0">
			<!--begin::Table container-->
			<div class="row g-1 mt-5 mb-5">
				@isset($communicationTypesList)
					@foreach($communicationTypesList AS $communicationType)
						@php
							$color= ['primary', 'secondary', 'success', 'warning', 'info', 'dark'][array_rand(['primary', 'secondary', 'success', 'warning', 'info', 'dark'])];
						@endphp
						<div class="col-12 col-md-6 col-xl-4 communication-type-card">
							<!--begin::Card-->
							<div class="card">
								<!--begin::Card body-->
								<div class="card-body d-flex flex-left flex-column">
									<div class="row align-items-sm-center">
										<!--begin::Avatar-->
										<div class="col-auto">
											<div class="symbol symbol-45px symbol-circle">
												<span class="symbol-label fs-2 fw-bold text-{{ $color }} bg-light-{{ $color }}">{{ $communicationType->icon }}</span>
											</div>
										</div>
										<!--end::Avatar-->
										<!--begin::Section-->
										<div class="col">
											<div class="d-flex align-items-center flex-row-fluid flex-wrap">
												<div class="flex-grow-1">
													<a href="javascript:" class="text-gray-800 text-hover-primary fs-6 fw-bolder communication-type-name" data-status="{{ $communicationType->statusName }}">{{ $communicationType->name }} </a><span data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark" title="@lang('messages.show_detailed_description')"><i class="fas fa-info-circle view-detailed-description"></i></span>
													<span class="text-muted fw-bold d-block fs-7 communication-type-description">{{ $communicationType->description }}</span>
													<span class="d-none communication-type-detailed-description">{!! $communicationType->detailedDescription !!} </span>
												</div>
											</div>
										</div>
										<div class="col-auto">
											@if ($communicationType->status == \App\Models\Organization\CommunicationType::STATUS_AVAILABLE)
												@if ($accountCommunicationTypesList->where('status', '!=', \App\Models\Organization\AccountCommunicationType::STATUS_INACTIVE)->where('communication_type_id', $communicationType->id)->count() == 0)
													<div @if($communicationType->requiresApproval) data-bs-custom-class="tooltip-dark" data-bs-toggle="tooltip" title="@lang('messages.action_requires_approval')" @endif>
														<button data-id="{{ $communicationType->id }}" class="btn btn-sm btn-light-success activate-communication-type">{!! theme()->getSvgIcon('demo6/media/icons/duotune/arrows/arr075.svg', 'svg-icon-3') !!} @lang('actions.connect')</button>
													</div>
												@else
													@if ($accountCommunicationTypesList->where('status', \App\Models\Organization\AccountCommunicationType::STATUS_ACTIVE)->where('communication_type_id', $communicationType->id)->count() > 0)
														<button class="btn btn-sm btn-light-primary bg-hover-danger disconnect-communication-type" data-id="{{ $accountCommunicationTypesList->where('status', \App\Models\Organization\AccountCommunicationType::STATUS_ACTIVE)->where('communication_type_id', $communicationType->id)->first()->id }}"><span class="normal-state">{!! theme()->getSvgIcon('demo6/media/icons/duotune/arrows/arr012.svg', 'svg-icon-3') !!} @lang('values.connected')</span><span class="hover-state d-none">{!! theme()->getSvgIcon('demo6/media/icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!} @lang('actions.disconnect')</span></button>
													@elseif($accountCommunicationTypesList->where('status', \App\Models\Organization\AccountCommunicationType::STATUS_IN_PROGRESS)->where('communication_type_id', $communicationType->id)->count() > 0)
														<button class="btn btn-sm btn-light-info disabled">{!! theme()->getSvgIcon('demo6/media/icons/duotune/abstract/abs013.svg', 'svg-icon-3') !!} @lang('values.in_progress')</button>
													@elseif($accountCommunicationTypesList->where('status', \App\Models\Organization\AccountCommunicationType::STATUS_DECLINED)->where('communication_type_id', $communicationType->id)->count() > 0)
														<button class="btn btn-sm btn-light-danger disabled">{!! theme()->getSvgIcon('demo6/media/icons/duotune/arrows/arr015.svg', 'svg-icon-3') !!} @lang('values.declined')</button>
													@endif
												@endif
											@else
												<button class="btn btn-sm btn-light disabled">{!! theme()->getSvgIcon('demo6/media/icons/duotune/abstract/abs024.svg', 'svg-icon-3') !!} @lang('values.coming_soon')</button>
											@endif
										</div>
										<!--end::Section-->
									</div>
								</div>
								<!--begin::Card body-->
							</div>
							<!--begin::Card-->
						</div>
					@endforeach
				@endisset
			</div>
			<!--end::Table container-->
		</div>
		<!--end::Card body-->
	</div>
	<!--end::Table-->
@endsection
@section('modals-container')
	<div class="modal fade" tabindex="-1" id="communication-type-description-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">@lang('actions.attribute_advanced_details', ['attribute' => trans_choice('names.communication_type', 2)])</h5>
					<!--begin::Close-->
					<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
						<span class="svg-icon svg-icon-2x"></span>
					</div>
					<!--end::Close-->
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="form-group col-12">
							<h3 class="fs-2hx text-dark text-center mb-8 communication-type-name"></h3>
						</div>
						<div class="form-group col-12 communication-type-description"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-light" data-bs-dismiss="modal">@lang('actions.close')</button>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('style')
@endsection
@section('scripts')
	<script src="{{ asset('assets/js/pages/organisation/communication-types/index.js') }}"></script>
@endsection
