<!--begin::Navbar-->
<div class="card mb-6 mb-xl-9">
	<div class="card-body p-0">
		<!--begin::Nav wrapper-->
		<div class="d-flex overflow-auto h-55px">
			<!--begin::Nav links-->
			<ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.country-scopes.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.country-scopes.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/maps/map004.svg", "svg-icon-2 ".(Route::is('organization.country-scopes.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4 fw-bolder">{{ trans_choice('names.country_scope', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.communication-types.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.communication-types.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/electronics/elc007.svg", "svg-icon-2 ".(Route::is('organization.communication-types.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4">{{ trans_choice('names.communication', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.integrations.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.integrations.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/files/fil021.svg", "svg-icon-2 ".(Route::is('organization.integrations.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4">{{ trans_choice('names.integration', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.reports.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.reports.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/graphs/gra010.svg", "svg-icon-2 ".(Route::is('organization.reports.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4">{{ trans_choice('names.report_display', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.landing-pages.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.landing-pages.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/electronics/elc004.svg", "svg-icon-2 ".(Route::is('organization.landing-pages.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4">{{ trans_choice('names.landing_page', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.teams.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.teams.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/communication/com005.svg", "svg-icon-2 ".(Route::is('organization.teams.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4">{{ trans_choice('names.team', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.roles.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.roles.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/abstract/abs027.svg", "svg-icon-2 ".(Route::is('organization.roles.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4">{{ trans_choice('names.role', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.extensions.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.extensions.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/technology/teh008.svg", "svg-icon-2 ".(Route::is('organization.extensions.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4">{{ trans_choice('names.extension', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
				<!--begin::Nav item-->
				<li class="nav-item">
					<a class="nav-link {{ Route::is('organization.users.index') ? 'active btn-active-light-primary' : '' }} btn btn-flex m-0" href="{{ route('organization.users.index') }}">
						{!! theme()->getSvgIcon("demo6/media/icons/duotune/communication/com014.svg", "svg-icon-2 ".(Route::is('organization.users.index') ? 'svg-icon-primary' : '')." me-3") !!}
						<span class="d-flex flex-column align-items-start">
							<span class="fs-4">{{ trans_choice('names.user', 2) }}</span>
						</span>
					</a>
				</li>
				<!--end::Nav item-->
			</ul>
			<!--end::Nav links-->
		</div>
		<!--end::Nav wrapper-->
	</div>
</div>
<!--end::Navbar-->