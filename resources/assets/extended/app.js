window.pageBlock = new KTBlockUI(document.body, {overlayClass: "bg-dark bg-opacity-25", message: `<div class="blockui-message"><span class="spinner-border text-primary"></span> ${Lang.get('messages.processing')}...</div>`});

Swal.update({
	confirmButtonText: Lang.get('actions.confirm'),
	cancelButtonText: Lang.get('actions.cancel'),
});

window.errorSwal = function (options) {
	let opts = {
		title: Lang.get('messages.something_went_wrong'),
		text: Lang.get('messages.reload_page_try_again'),
		icon: 'warning',
	};
	if (options !== undefined) {
		opts = $.extend(opts, options);
	}
	swal.fire(opts);
};