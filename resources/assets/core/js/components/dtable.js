"use strict";

var DTable = function (element, options) {
	$.fn.dataTable.ext.buttons.bulkDelete = {
		text: Lang.get('messages.delete'),
		titleAttr: Lang.get('messages.coming_soon'),
		attr: {
			"data-toggle": "tooltip"
		},
		className: 'disabled',
		action: function (e, dt, node, config) {
			if (dt.rows({selected: true}).count() > 0) {
				console.log('deleted rows: ' + dt.rows({selected: true}).count());
			}
		}
	};
	$.fn.dataTable.ext.buttons.bulkEdit = {
		text: Lang.get('messages.edit'),
		titleAttr: Lang.get('messages.coming_soon'),
		attr: {
			"data-toggle": "tooltip"
		},
		className: 'disabled',
		action: function (e, dt, node, config) {
			if (dt.rows({selected: true}).count() > 0) {
				console.log('editing rows: ' + dt.rows({selected: true}).count());
			}
		}
	};
	$.fn.dataTable.ext.buttons.bulkArchive = {
		text: Lang.get('messages.archive'),
		titleAttr: Lang.get('messages.coming_soon'),
		attr: {
			"data-toggle": "tooltip"
		},
		className: 'disabled',
		action: function (e, dt, node, config) {
			if (dt.rows({selected: true}).count() > 0) {
				console.log('archiving rows: ' + dt.rows({selected: true}).count());
			}
		}
	};
	var opts = $.extend(true, {
		dom: `<'row'<'col-sm-12'tr>>
			<'row datatable-controls'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
		responsive: true,
		lengthMenu: [5, 10, 25, 50],
		pageLength: 10,
		searchDelay: 1000,
		processing: true,
		serverSide: true,
		drawCallback: function () {
			$('#' + this[0].id).find('[data-toggle="tooltip"]').each(function () {
				KTApp.initTooltip($(this));
			});
			$('#' + this[0].id).find('[data-toggle="popover"]').each(function () {
				KTApp.initPopover($(this));
			});
		},
	}, options);
	return $(element).DataTable(opts);
};

// webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
	module.exports = DTable;
}