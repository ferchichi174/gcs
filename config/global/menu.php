<?php

return array(
	// Documentation menu
	'documentation' => array(
		// Getting Started
		array(
			'heading' => 'Getting Started',
		),

		// Overview
		array(
			'title' => 'Overview',
			'path' => 'documentation/getting-started/overview',
		),

		// Build
		array(
			'title' => 'Build',
			'path' => 'documentation/getting-started/build',
		),

		array(
			'title' => 'Multi-demo',
			'attributes' => array("data-kt-menu-trigger" => "click"),
			'classes' => array('item' => 'menu-accordion'),
			'sub' => array(
				'class' => 'menu-sub-accordion',
				'items' => array(
					array(
						'title' => 'Overview',
						'path' => 'documentation/getting-started/multi-demo/overview',
						'bullet' => '<span class="bullet bullet-dot"></span>',
					),
					array(
						'title' => 'Build',
						'path' => 'documentation/getting-started/multi-demo/build',
						'bullet' => '<span class="bullet bullet-dot"></span>',
					),
				),
			),
		),

		// File Structure
		array(
			'title' => 'File Structure',
			'path' => 'documentation/getting-started/file-structure',
		),

		// Customization
		array(
			'title' => 'Customization',
			'attributes' => array("data-kt-menu-trigger" => "click"),
			'classes' => array('item' => 'menu-accordion'),
			'sub' => array(
				'class' => 'menu-sub-accordion',
				'items' => array(
					array(
						'title' => 'SASS',
						'path' => 'documentation/getting-started/customization/sass',
						'bullet' => '<span class="bullet bullet-dot"></span>',
					),
					array(
						'title' => 'Javascript',
						'path' => 'documentation/getting-started/customization/javascript',
						'bullet' => '<span class="bullet bullet-dot"></span>',
					),
				),
			),
		),

		// Dark skin
		array(
			'title' => 'Dark Mode Version',
			'path' => 'documentation/getting-started/dark-mode',
		),

		// RTL
		array(
			'title' => 'RTL Version',
			'path' => 'documentation/getting-started/rtl',
		),

		// Troubleshoot
		array(
			'title' => 'Troubleshoot',
			'path' => 'documentation/getting-started/troubleshoot',
		),

		// Changelog
		array(
			'title' => 'Changelog <span class="badge badge-changelog badge-light-danger bg-hover-danger text-hover-white fw-bold fs-9 px-2 ms-2">v' . theme()->getVersion() . '</span>',
			'breadcrumb-title' => 'Changelog',
			'path' => 'documentation/getting-started/changelog',
		),

		// References
		array(
			'title' => 'References',
			'path' => 'documentation/getting-started/references',
		),


		// Separator
		array(
			'custom' => '<div class="h-30px"></div>',
		),

		// Configuration
		array(
			'heading' => 'Configuration',
		),

		// General
		array(
			'title' => 'General',
			'path' => 'documentation/configuration/general',
		),

		// Menu
		array(
			'title' => 'Menu',
			'path' => 'documentation/configuration/menu',
		),

		// Page
		array(
			'title' => 'Page',
			'path' => 'documentation/configuration/page',
		),

		// Page
		array(
			'title' => 'Add NPM Plugin',
			'path' => 'documentation/configuration/npm-plugins',
		),


		// Separator
		array(
			'custom' => '<div class="h-30px"></div>',
		),

		// General
		array(
			'heading' => 'General',
		),

		// DataTables
		array(
			'title' => 'DataTables',
			'classes' => array('item' => 'menu-accordion'),
			'attributes' => array("data-kt-menu-trigger" => "click"),
			'sub' => array(
				'class' => 'menu-sub-accordion',
				'items' => array(
					array(
						'title' => 'Overview',
						'path' => 'documentation/general/datatables/overview',
						'bullet' => '<span class="bullet bullet-dot"></span>',
					),
				),
			),
		),

		// Remove demos
		array(
			'title' => 'Remove Demos',
			'path' => 'documentation/general/remove-demos',
		),


		// Separator
		array(
			'custom' => '<div class="h-30px"></div>',
		),

		// HTML Theme
		array(
			'heading' => 'HTML Theme',
		),

		array(
			'title' => 'Components',
			'path' => '//preview.keenthemes.com/metronic8/demo1/documentation/base/utilities.html',
		),

		array(
			'title' => 'Documentation',
			'path' => '//preview.keenthemes.com/metronic8/demo1/documentation/getting-started.html',
		),
	),

	// Main menu
	'main' => array(
		'Home' => array(
			"title" => "Home",
			"icon" => '<i class="bi bi-house-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/dashboard",
		),
		'Contacts' => array(
			"title" => "Contacts",
			"icon" => '<i class="bi bi-people-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/contacts/list",
		),
		'Communications' => array(
			"title" => "Communications",
			"icon" => '<i class="bi bi-inbox-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/communications/list",
		),
		'Tasks' => array(
			"title" => "Tasks",
			"icon" => '<i class="bi bi-list-task fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/tasks/list",
		),
		'Appointments' => array(
			"title" => "Appointments",
			"icon" => '<i class="bi bi-calendar-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/appointments/calendar",
		),
		'Scripts' => array(
			"title" => "Scripts",
			"icon" => '<i class="bi bi-file-earmark-text-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/scripts/list",
		),
		'Real-Time' => array(
			"title" => "RealTime",
			"icon" => '<i class="bi bi-hourglass-split fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/realtime/list",
		),
		'Reporting' => array(
			"title" => "Reporting",
			"icon" => '<i class="bi bi-bar-chart-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/reporting/list",
		),
		'Organization' => array(
			"title" => "Organization",
			"icon" => '<i class="bi bi-gear-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/organization/countries",
		)
	),

	// Horizontal menu
	'horizontal' => array(
		// Dashboard
		array(
			'title' => 'Dashboard',
			'path' => '',
			'classes' => array('item' => 'me-lg-1'),
		),

		// Resources
		array(
			'title' => 'Quick create',
			'classes' => array('item' => 'menu-lg-down-accordion me-lg-1', 'arrow' => 'd-lg-none'),
			'attributes' => array(
				'data-kt-menu-trigger' => "hover",
				'data-kt-menu-placement' => "bottom-start",
			),
			'sub' => array(
				'class' => 'menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px',
				'items' => array(
					array(
						'title' => "Contact",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/communication/com013.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
					array(
						'title' => "Contact group",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/communication/com014.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
					array(
						'title' => "Task",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/arrows/arr086.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
					array(
						'title' => "Appointment",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/general/gen014.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
					array(
						'title' => "User",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/communication/com006.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
				),
			),
		),
	),
);
