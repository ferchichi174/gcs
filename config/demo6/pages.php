<?php
return array(
	'' => array(
		'title' => 'Dashboard',
		'description' => '#XRS-45670',
		'view' => 'index',
		'layout' => array(
			'page-title' => array(
				'description' => true,
				'breadcrumb' => false
			)
		),
		'assets' => array(
			'vendors' => array(
				'css' => array(
					'plugins/custom/fullcalendar/fullcalendar.bundle.css'
				),
				'js' => array(
					'plugins/custom/fullcalendar/fullcalendar.bundle.js'
				)
			)
		)
	),

	'dashboards' => array(
		'compact' => array(
			'title' => 'Compact',
			'view' => 'index',
			'layout' => array(
				'header' => array(
					'left' => 'page-title',
				),
				'page-title' => array(
					'description' => false,
					'breadcrumb' => true
				),
				'toolbar' => array(
					'display' => true
				)
			),
			'assets' => array(
				'vendors' => array(
					'css' => array(
						'plugins/custom/fullcalendar/fullcalendar.bundle.css'
					),
					'js' => array(
						'plugins/custom/fullcalendar/fullcalendar.bundle.js'
					)
				)
			)
		),
		'minimal' => array(
			'title' => 'Minimal',
			'view' => 'index',
			'layout' => array(
				'header' => array(
					'left' => 'page-title',
				),
				'toolbar' => array(
					'display' => false,
				),
				'page-title' => array(
					'description' => false,
					'breadcrumb' => true
				)
			),
			'assets' => array(
				'vendors' => array(
					'css' => array(
						'plugins/custom/fullcalendar/fullcalendar.bundle.css'
					),
					'js' => array(
						'plugins/custom/fullcalendar/fullcalendar.bundle.js'
					)
				)
			)
		),
		'header' => null
	),

	'organization' => array(
		'title' => 'Organization',
		'country-scopes' => array(
			'title' => 'Country Scopes',
			'layout' => array(
				'page-title' => array(
					'breadcrumb' => true, // hide breadcrumb
					'breadcrumb-items' => [
						[
							'title' => 'Home',
						],
						[
							'title' => 'Organization'
						],
						[
							'title' => 'Country Scopes',
							'active' => true,
							'path' => 'organization.country-scopes.index'
						]
					]
				)
			)
		)
	),
	'contacts' => array(
		'title' => 'Organization',
		'layout' => array(
			'page-title' => array(
				'breadcrumb' => true, // hide breadcrumb
				'breadcrumb-items' => [
					[
						'title' => 'Home',
					],
					[
						'title' => 'Contacts'
					],
					[
						'title' => 'Contacts list',
						'active' => true,
						'path' => 'contacts.index'
					]
				]
			)
		),
		'country-scopes' => array(
			'title' => 'Country Scopes',
			'layout' => array(
				'page-title' => array(
					'breadcrumb' => true, // hide breadcrumb
					'breadcrumb-items' => [
						[
							'title' => 'Home',
						],
						[
							'title' => 'Organization'
						],
						[
							'title' => 'Country Scopes',
							'active' => true,
							'path' => 'organization.country-scopes.index'
						]
					]
				)
			)
		)
	)
);
