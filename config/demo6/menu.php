<?php

use App\Core\Adapters\Theme;

return array(
	'horizontal' => array(
		// Dashboard
		array(
			'title' => 'Dashboard',
			'path' => '',
			'classes' => array('item' => 'me-lg-1'),
		),

		// Resources
		array(
			'title' => 'Quick create',
			'classes' => array('item' => 'menu-lg-down-accordion me-lg-1', 'arrow' => 'd-lg-none'),
			'attributes' => array(
				'data-kt-menu-trigger' => "hover",
				'data-kt-menu-placement' => "bottom-start",
			),
			'sub' => array(
				'class' => 'menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px',
				'items' => array(
					array(
						'title' => "Contact",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/communication/com013.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
					array(
						'title' => "Contact group",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/communication/com014.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
					array(
						'title' => "Task",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/arrows/arr086.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
					array(
						'title' => "Appointment",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/general/gen014.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
					array(
						'title' => "User",
						'icon' => theme()->getSvgIcon("demo6/media/icons/duotune/communication/com006.svg", "svg-icon-2"),
						'path' => 'documentation/getting-started/overview',
					),
				),
			),
		),
	),
	'aside' => array(
		'Home' => array(
			"title" => "Home",
			"icon" => '<i class="bi bi-house-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/dashboard",
		),
		'Contacts' => array(
			"title" => "Contacts",
			"icon" => '<i class="bi bi-people-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/contacts/list",
		),
		'Communications' => array(
			"title" => "Communications",
			"icon" => '<i class="bi bi-inbox-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/communications/list",
		),
		'Tasks' => array(
			"title" => "Tasks",
			"icon" => '<i class="bi bi-list-task fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/tasks/list",
		),
		'Appointments' => array(
			"title" => "Appointments",
			"icon" => '<i class="bi bi-calendar-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/appointments/calendar",
		),
		'Scripts' => array(
			"title" => "Scripts",
			"icon" => '<i class="bi bi-file-earmark-text-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/scripts/list",
		),
		'Real-Time' => array(
			"title" => "RealTime",
			"icon" => '<i class="bi bi-hourglass-split fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/realtime/list",
		),
		'Reporting' => array(
			"title" => "Reporting",
			"icon" => '<i class="bi bi-bar-chart-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "/reporting/list",
		),
		'Organization' => array(
			"title" => "Organization",
			"icon" => '<i class="bi bi-gear-fill fs-2"></i>',
			"attributes" => array(
				'link' => array(
					"data-bs-trigger" => "hover",
					"data-bs-dismiss" => "click",
					"data-bs-placement" => "right",
				),
			),
			"classes" => array(
				"item" => "py-2",
				"link" => "menu-center",
				"icon" => "me-0",
			),
			"path" => "organization/country-scopes",
		)
	),
);
