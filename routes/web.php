<?php

use App\Http\Controllers\Contacts\ContactsController;
use App\Http\Controllers\Pages\AppointmentsCalendarController;
use App\Http\Controllers\Pages\DashboardController;

use App\Http\Controllers\Account\SettingsController;
use App\Http\Controllers\Auth\SocialiteLoginController;
use App\Http\Controllers\Logs\AuditLogsController;
use App\Http\Controllers\Logs\SystemLogsController;
use App\Http\Controllers\Pages\OrganizationExtensionsController;
use App\Http\Controllers\Pages\OrganizationIntegrationsController;
use App\Http\Controllers\Pages\OrganizationReportsController;
use App\Http\Controllers\Pages\OrganizationRolesController;
use App\Http\Controllers\Pages\OrganizationTeamsController;
use App\Http\Controllers\Pages\OrganizationUsersAddController;
use App\Http\Controllers\Pages\OrganizationUsersController;
use App\Http\Controllers\Pages\OrganizationUsersEditController;
use App\Http\Controllers\Pages\RealtimeController;
use App\Http\Controllers\Pages\ReportingController;
use App\Http\Controllers\Pages\ScriptsController;
use App\Http\Controllers\Pages\TasksController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect('index');
// });

$menu = theme()->getMenu();
array_walk($menu, function ($val) {
	if (isset($val['path'])) {
		$route = Route::get($val['path'], [PagesController::class, 'index']);

		// Exclude documentation from auth middleware
		//if (!Str::contains($val['path'], 'documentation')) {
		//    $route->middleware('auth');
		//}
	}
});


Route::middleware('auth')->group(function () {
	// Account pages
	Route::prefix('account')->group(function () {
		Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
		Route::put('settings', [SettingsController::class, 'update'])->name('settings.update');
		Route::put('settings/email', [SettingsController::class, 'changeEmail'])->name('settings.changeEmail');
		Route::put('settings/password', [SettingsController::class, 'changePassword'])->name('settings.changePassword');
	});

	// Logs pages
	Route::prefix('log')->name('log.')->group(function () {
		Route::resource('system', SystemLogsController::class)->only(['index', 'destroy']);
		Route::resource('audit', AuditLogsController::class)->only(['index', 'destroy']);
	});
});

Route::resource('users', UsersController::class);

/**
 * Socialite login using Google service
 * https://laravel.com/docs/8.x/socialite
 */
Route::get('/auth/redirect/{provider}', [SocialiteLoginController::class, 'redirect']);

require __DIR__ . '/auth.php';


/* ROUTES LIST */

Route::middleware(['auth:web'])->group(function () {
	Route::get('', [DashboardController::class, 'index'])->name('dashboard.index');

	Route::prefix('contacts')->name('contacts.')->group(function () {
		Route::post('/filtered', [\App\Http\Controllers\Contacts\ContactsController::class, 'filteredIndex'])->name('filtered-index');
		Route::resource('', \App\Http\Controllers\Contacts\ContactsController::class)->parameter('', 'contact');
		Route::get('list', [ContactsController::class, 'index'])->name('contacts.index');
		Route::get('details', [ContactController::class, 'index'])->name('contact.index');
		Route::get('search', [ContactsSearchController::class, 'index'])->name('contacts.index');
		Route::get('create', [ContactsController::class, 'index'])->name('contacts.index');
		Route::get('import', [ContactsImportController::class, 'index'])->name('contacts.index1');


		Route::prefix('groups')->group(function () {
			Route::get('list', [ContactsGroupsController::class, 'index'])->name('contacts.groups.index');
		});
	});
	Route::prefix('communications')->group(function () {
		Route::get('list', [CommunicationsController::class, 'index'])->name('communications.index');
		Route::get('search', [CommunicationsSearchController::class, 'index'])->name('communications.search');
	});
	Route::prefix('tasks')->group(function () {
		Route::get('list', [TasksController::class, 'index'])->name('tasks.index');
		Route::get('create', [TasksController::class, 'add'])->name('tasks.add');
		Route::get('details', [TasksDetailsController::class, 'index'])->name('tasks.detail');
		Route::get('search', [TasksSearchController::class, 'index'])->name('tasks.search');
	});
	Route::prefix('appointments')->group(function () {
		Route::get('calendar', [AppointmentsCalendarController::class, 'index'])->name('appointments.calendar');
		Route::get('list', [AppointmentsListController::class, 'index'])->name('appointments.list');
		Route::get('search', [AppointmentsSearchController::class, 'index'])->name('appointments.search');
	});
	Route::prefix('scripts')->group(function () {
		Route::get('list', [ScriptsController::class, 'index'])->name('scripts.index');
	});
	Route::prefix('realtime')->group(function () {
		Route::get('list', [RealtimeController::class, 'index'])->name('realtime.index');
	});
	Route::prefix('reporting')->group(function () {
		Route::get('list', [ReportingController::class, 'index'])->name('reporting.index');
	});
	Route::prefix('organization')->name('organization.')->group(function () {
		Route::prefix('country-scopes')->name('country-scopes.')->group(function () {
			Route::get('/enable/{countryScopeId}', [\App\Http\Controllers\Organization\CountryScopesController::class, 'enable'])->name('enable');
			Route::get('/disable/{countryScope}', [\App\Http\Controllers\Organization\CountryScopesController::class, 'disable'])->name('disable');
			Route::post('/filtered', [\App\Http\Controllers\Organization\CountryScopesController::class, 'filteredIndex'])->name('filtered-index');
			Route::resource('', \App\Http\Controllers\Organization\CountryScopesController::class)->parameter('', 'countryScope');
		});
		Route::prefix('communication-types')->name('communication-types.')->group(function () {
			Route::post('/filtered', [\App\Http\Controllers\Organization\AccountCommunicationTypesController::class, 'filteredIndex'])->name('filtered-index');
			Route::resource('', \App\Http\Controllers\Organization\AccountCommunicationTypesController::class)->parameter('', 'accountCommunicationType');
		});
		Route::prefix('landing-pages')->name('landing-pages.')->group(function () {
			Route::post('/filtered', [\App\Http\Controllers\Organization\LandingPagesController::class, 'filteredIndex'])->name('filtered-index');
			Route::resource('', \App\Http\Controllers\Organization\LandingPagesController::class)->parameter('', 'landingPage');
		});
		Route::prefix('users')->group(function () {
			Route::get('/', [OrganizationUsersController::class, 'index'])->name('users.index');
			Route::get('/add', [OrganizationUsersAddController::class, 'index'])->name('users.index');
			Route::get('/edit', [OrganizationUsersEditController::class, 'index'])->name('users.index');
		});
		Route::get('reports', [OrganizationReportsController::class, 'index'])->name('reports.index');
		Route::get('integrations', [OrganizationIntegrationsController::class, 'index'])->name('integrations.index');
		Route::get('extensions', [OrganizationExtensionsController::class, 'index'])->name('extensions.index');
		Route::get('roles', [OrganizationRolesController::class, 'index'])->name('roles.index');
		Route::get('teams', [OrganizationTeamsController::class, 'index'])->name('teams.index');
	});

	Route::get('test', function () {
		return view('pages.test');
	});
});
